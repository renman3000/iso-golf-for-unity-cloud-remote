﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CustomUnitType
{
    [System.Serializable]
    public struct rowData
    {
        public int[] row;
    }

    public rowData[] rows = new rowData[10];
}