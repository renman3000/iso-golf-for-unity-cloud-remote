﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Alerts_Mng : MonoBehaviour {


	public static Alerts_Mng Instance;

	public Transform tf_airball;

	void Awake(){
		Instance = this;
		tf_airball.localScale = Vector3.zero;
	}
		
	public void run_airball(){
		Invoke("end_airball", 1);
		tf_airball.DOScale(Vector3.one, 0.25f).SetEase(Ease.OutBounce);
	}
	void end_airball(){
		tf_airball.localScale = Vector3.zero;
	}







}
