﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;


public class ERS_Mng : MonoBehaviour {


	public static ERS_Mng Instance;

	public Player_Mng playerMng;
		






	int levelCount;




	public TextMesh tm_worldName;
	public TextMesh tm_levelName;
	public TextMesh tm_par;
	public TextMesh tm_shotCount;







	///AT DEAD BALL (AtDeath)
	public Transform tf_deadBall;



	///SECTION
	//TL_FG = time limit free gift:::: time limit for free gift...
	[HideInInspector]
	float TL_FG_limit =  0;
	bool TL_FG_isReached;
	float TL_FG_crnt;
	float TL_FG_start;
	float TL_FG_end;
	float TL_FG_delta;


	float TL_FG_m;
	float TL_FG_s;
	public TextMesh tm_TL_FG_m;
	public TextMesh tm_TL_FG_s;


	//////////////////////////////////////////////////
	/////////////////////////////////////////////////
	////////////////////////////////////////////////
	///////////////////////////////////////////////
	/// ERB = EndRoundBtn.... BTNS..... a class of btn. each represents a differnet condition depending on what conitions are met atDeath()... we assign various ones to to the ERS, run()
	public Transform ERB_O_TL_FG;//"_O_" = Offer. @ TL_FG_isReached
	public Transform ERB_O_TL_FG_clock;
	public TextMesh tm_ERB_O_TL_FG_clock;
	public Transform ERB_O_winPrize;//@ coins >= 100
	public Transform ERB_A_;//"_A_" = Acheivment. loaded if condition met. see achievment class
	public Transform ERB_O_coinsWA;//offer coins if WA = Watch Ad @ death @ coins < 100
	public Transform ERB_O_characterTemp;//offer character temp
	public Transform ERB_empty;



	/// ERB_slots: Transform, of [4] used to tween in any of the ERB_, into scene...
	public Transform[] ERB_slots;
	public ERB_Unit[] ERB_Units;
	bool wereCoinsDoubled;

	public Transform tf_staticBtns;///settings, photo share, play, game center
	public GameObject gm_staticUIBtns;


	public Transform tf_strokeCount_HUD;

	bool isAtDeath;

	[HideInInspector]
	public bool isPrizeCollected;




	Vector3 resetPos_staticBtns;
	Vector3[] resetPos_erbs;



	public Transform tf_rt_ERS;


	public Transform tf_ERBs;
	public Transform tf_sharePic;



	float last_startTime;
	DateTime crnt_dateTime;
	DateTime last_dateTime;




	void Awake(){

		Instance = this;

		hideHUD();


		set_ERS();


	




		print("??? ERB STUFF AT AWAKE");

		resetPos_staticBtns = tf_staticBtns.localPosition;
		resetPos_erbs = new Vector3[ERB_slots.Length];

		for(int i = 0; i < ERB_slots.Length; i++){
			resetPos_erbs[i] = ERB_slots[i].localPosition;
		}

		tf_rt_ERS.localScale = Vector3.zero;
	}


	public void set_clock(float min, float sec){
		print("SET CLOCK " + min + sec);
		TL_FG_isReached = true;
		TL_FG_m = min;
		TL_FG_s = sec;
		run_TL_FG();
	}



	void set_ERS(){
		gm_staticUIBtns.SetActive(false);
	}



	public void ballSunk(){
		

		//we wait for flag to be raised and number delegate mng to run transfer of shots to bank.

		Invoke("openERS_btn", 1);

	}

	void openERS_btn(){
		tf_rt_ERS.localScale = Vector3.one;
	}

	void transfer_shotsLeft(){
		//here at ers open, shot sunk, we must pass all shots left (from par), to our bank.



		int shotsLeft = Home_Mng.Instance.levelMng.holeMng.par;


		//now we must animate the passing of each shot left into our bank.
		Number_Mng.Instance.pass_shotsLeft();





	}


	public void openERS(){
		///called via this rtf btn
		print("openERS () " + Time.time);
		tf_rt_ERS.localScale = Vector3.zero;

		setERBS();

	}
	/*
	public void openERS_open(){
		//called when an alternate scene from game has been used (free gift, prize etc...), the ERB's will not be tweened in, but set to 0 x.
		///// NOTE . as of wednesday night.. need to sort how to organize the ERBS given conditions and from what point they are called. 
		for(int i = 0; i < ERB_Units.Length; i++){
			ERB_Units[i].reset();
		}

		///used later..........
		int int_ERBs_slotCount = 0;//used to know when to tween in static btns...


		///is dead? y = watch ad, n = shots won
		if(!isAtDeath){
			ERB_Units[0].setUnit(0, 0);
		}
		else if(isAtDeath){
			ERB_Units[0].setUnit(0, 1);
		}

		///is dead? if n = double coins, watch ad, if y = promo
		if(!isAtDeath && !wereCoinsDoubled){//double coins won
			ERB_Units[1].setUnit(1, 0);
		}
		if(isAtDeath){//promo
			ERB_Units[1].setUnit(1, 1);
		}


		//is coins < 100 || y = show coins to go, no = show prize available 
		int coins = Coin_Mng.coins;
		if(coins < 100){
			//shots to go
			ERB_Units[2].setUnit(2, 0);
		}
		else if (coins >= 100){
			//prize available
			ERB_Units[2].setUnit(2, 1);
		}


		//is time up on clock
		if(TL_FG_m > 0 || TL_FG_s > 0){
			///show running clock
			ERB_Units[3].setUnit(3, 0);
		}
		else if(TL_FG_m == 0 && TL_FG_s == 0){
			//show free gift availble
			ERB_Units[3].setUnit(3, 1);
		}



		//TEMP
		for(int i = 0; i < ERB_slots.Length; i++){
			float delay = 0.25f + ((float)i * 0.25f);
			setERBs_slot(ERB_slots[i], delay); 
		}
		run_StaticBtns_run();
	
		return;


	}
	*/


	void setERBs_slot_set(Transform tf_parent, Transform tf_child, float erbs_delay){
		tf_child.parent = tf_parent;
		tf_child.localScale = Vector3.one;
		tf_child.localPosition = Vector3.zero;
		//run into scene
		tf_parent.localPosition = Vector3.zero;

	}
	void run_StaticBtns_run(){
		tf_staticBtns.localPosition = Vector3.zero;
	}


	public void bp_playNext(){
		reset();
		//Home_Mng.Instance.turnOn_loadingScene();
		Coin_Mng.Instance.reset();
		Home_Mng.Instance.set_newLevel();
		Number_Mng.Instance.hide_shots();
		Number_Mng.Instance.hide_bank();
		wereCoinsDoubled = false;
	}

	void reset(){
		print("reset()");
		isAtDeath = false;



		tf_deadBall.localPosition = new Vector3(-300, 0, 0);

		tf_staticBtns.localPosition = resetPos_staticBtns;
		gm_staticUIBtns.SetActive(false);

		reset_erbs();
		resetERBs_slots();
	}


	void reset_erbs(){
		//resets the erb unit (btns etc)
		for(int i = 0; i < ERB_Units.Length; i++){
			ERB_Units[i].reset();
		}
	}





	void hideHUD(){
		

	}



	public void set_levelInfo(Level_Mng levelMng_del){
		print("??????");
		//tm_worldName.text = "World: " + levelMng_del.name_level;
		//tm_levelName.text = "Level: " + levelMng_del.name_level;
		//tm_par.text = "Par: " + levelMng_del.shotLimit.ToString();
		//tm_shotCount.text = "0";
	}










	/////////////////////////
	/// ////for watching time passed to offer free gift...(coins(rdmamount))
	public void run_TL_FG(){
		if(!TL_FG_isReached){
			return;
		}

		TL_FG_isReached = false;
		TL_FG_s = 30;
		InvokeRepeating("click_TL_FG", 0, 1);
		print("??? RUN CLOCK");
	}	
	void click_TL_FG(){
		//print("!");
		//if seconds = 0;
		if(TL_FG_s == 0){
			//if minutes > 0
			if(TL_FG_m > 0){
				TL_FG_m--;
				TL_FG_s = 59;
			}
			//if minutes = 0;
			else if(TL_FG_m == 0){
				print("times up!!! run free gift!");


				//this ERB slot is ERB[2], as such, fire into it. 
				TL_FG_isReached = true;
				ERB_Units[3].freeGiftReady();
				CancelInvoke("click_TL_FG");
				print("!!! CANCEL CLOCK CLICK");
				return;
			}
		}
		//if s > 0
		else if (TL_FG_s > 0){
			//TL_FG_m = TL_FG_m;
			TL_FG_s--;
		}



		//set text meshe of clock
		if(TL_FG_s >= 10){
			tm_TL_FG_m.text = TL_FG_m.ToString();
			tm_TL_FG_s.text = TL_FG_s.ToString();
		}
		else if(TL_FG_s < 10){
			tm_TL_FG_m.text = TL_FG_m.ToString();
			tm_TL_FG_s.text = "0" + TL_FG_s.ToString();
		}
	


		/*
		tm_ERB_O_TL_FG_clock.text = "FREE GIFT IN 00:" + TL_FG_crnt.ToString();
		if(TL_FG_crnt <= 0){
			CancelInvoke("click_TL_FG");
			TL_FG_crnt = 20;
			tm_ERB_O_TL_FG_clock.text = "";
			TL_FG_isReached = true;
			if(isAtDeath){
				///the ERBS should be open and active, swap the clock erb for the free gift ERB
				ERB_swap_clock_toGift();
			}
		}*/
	}







	public void atDeath(){
		////now that we are at death. we must check a few conditions to determine, what options are presented to the player.......
		print("atDeath");
		////conditions are....
		///1. if timeLimitReached: offer.gift_coins(rdm amount)
		///2. if coins > 100: offer.gift_character(rdm character)
		///3. if coins < 100: achievement.gift : (reach x of this ex. strokes taken, holes made)
		///4. watch ads for coin
		///5. offer charcter to try for free. at death with charcater, offer charcater for $ (+ 250 coins)

		isAtDeath = true;
		tf_rt_ERS.localScale = Vector3.one;

		return;




	}

	public void setERBS(){
		///// NOTE . as of wednesday night.. need to sort how to organize the ERBS given conditions and from what point they are called. 
		for(int i = 0; i < ERB_Units.Length; i++){
			ERB_Units[i].reset();
		}

		///used later..........
		int int_ERBs_slotCount = 0;//used to know when to tween in static btns...


		///is dead? y = watch ad, n = shots won
		if(!isAtDeath){
			ERB_Units[0].setUnit(0, 0);
		}
		else if(isAtDeath){
			ERB_Units[0].setUnit(0, 1);
		}

		///is dead? if n = double coins, watch ad, if y = promo
		if(!isAtDeath  && !wereCoinsDoubled){//double coins won
			ERB_Units[1].setUnit(1, 0);
		}
		if(isAtDeath ||!isAtDeath && wereCoinsDoubled){//promo
			ERB_Units[1].setUnit(1, 1);
		}



		//is coins < 100 || y = show coins to go, no = show prize available 
		int coins = Coin_Mng.coins;
		if(coins < 100){
			//shots to go
			ERB_Units[2].setUnit(2, 0);
		}
		else if (coins >= 100){
			//prize available
			ERB_Units[2].setUnit(2, 1);
		}


		//is time up on clock
		if(TL_FG_m > 0 || TL_FG_s > 0){
			///show running clock
			ERB_Units[3].setUnit(3, 0);
		}
		else if(TL_FG_m == 0 && TL_FG_s == 0){
			//show free gift availble
			ERB_Units[3].setUnit(3, 1);
		}


		///invoke the running o



		for(int i = 0; i < ERB_slots.Length; i++){
			float delay = 0.25f + ((float)i * 0.25f);
			setERBs_slot(ERB_slots[i], delay); 
		}
		run_StaticBtns(int_ERBs_slotCount);

		//tween out the par/strokecount, as the info was passed to the bank.
		tf_strokeCount_HUD.DOLocalMoveX(15, 0.25f, false);
		return;



	

		

	}

	void setERBs_slot(Transform tf_parent, float erbs_delay){
		//run into scene
		runERBs_slot(tf_parent, erbs_delay);
		//must also remove the par, values (strokeCount) from hud. 

	}
	void runERBs_slot(Transform tf_, float erbs_delay){
		//tf_.localPosition = new Vector3(-225, tf_.localPosition.y, tf_.localPosition.z);
		tf_.DOLocalMove(Vector3.zero, 0.5f, false).SetEase(Ease.OutBounce).SetDelay(erbs_delay);
	}
	void resetERBs_slots(){
		//reset the pos of each erb
		for(int i = 0; i < ERB_Units.Length; i++){
			ERB_slots[i].localPosition = resetPos_erbs[i];
		}
	}

	public void ERB_swap_clock_toGift(){

		Transform ERB_Slot = null;

		for(int i = 0; i < ERB_slots.Length; i++){

			if(ERB_O_TL_FG_clock.parent == ERB_slots[i]){
				ERB_Slot = ERB_slots[i];
			}

		}

		if(ERB_Slot != null){
			///called when ERBs open (isAtDeath), and the clock is running, now, a free gift has become available. 
			///... as such, swap the clock erb for the free gift erb
			ERB_O_TL_FG_clock.localPosition = new Vector3(-1000, ERB_O_TL_FG_clock.localPosition.y, ERB_O_TL_FG_clock.localPosition.z);
			ERB_O_TL_FG_clock.parent = transform;
			ERB_O_TL_FG.parent = ERB_Slot;
			ERB_O_TL_FG.localPosition = Vector3.zero;
		}

	}



	public void run_StaticBtns(int int_ERBs_slotCount){
		//float delay = ((float)int_ERBs_slotCount * 0.5f) + 0.25f;
		float delay = 1;
		tf_staticBtns.DOLocalMove(Vector3.zero, 0.5f, false).SetDelay(delay).SetEase(Ease.OutBounce).OnComplete(set_staticBtns_UI);
	}
	void set_staticBtns_UI(){
		gm_staticUIBtns.SetActive(true);
	}




	public void bp_sharePic(){
		//@ ers btn, share, pressed....

		///hide ers btns...
		tf_ERBs.DOLocalMoveX(-15, 0.25f, false);
		//bring in the pic share scene
		tf_sharePic.DOLocalMoveX(0, 0.25f, false);

		Home_Mng.Instance.levelMng.playerMng.stopInput();

	}
	public void bp_remove_sharePic(){
		//@ share pic scene btn, close scene, pressed....

		///hide ers btns...
		tf_ERBs.DOLocalMoveX(0, 0.25f, false);
		//bring in the pic share scene
		tf_sharePic.DOLocalMoveX(15, 0.25f, false);		
	}



	/// <summary>
	/// ADS
	/// </summary>
	public void bp_watchAd_doubleCoins(){
		UNITY_ADS_MNG.instance.watchAd_doubleCoins();
	}
	//results for watch ad, double coins
	public void adFinished_doubleCoins(){
		print("watched ad, double coins won in round, add to coins total.");
		//we must animate the coins won in round to double the current size. 
		ERB_Units[0].setCase_doubleCoinsWon();
		ERB_Units[1].setUnit(1, 1);
		wereCoinsDoubled = true;
	}
	public void adFailed_doubleCoins(){
		print("ad failed, make no adjustments to coins total, show alert");
	}
	public void adSkipped_doubleCoins(){
		print("ad skipped, show alert");
	}


	public void bp_watchAd_extraLife(){
		UNITY_ADS_MNG.instance.watchAd_extraLife();
	}
	//results for watch ad, double coins
	public void adFinished_extraLife(){
		print("watched ad, extra life won in round, add to coins total.");
	}
	public void adFailed_extraLife(){
		print("ad failed, make no adjustments to coins total, show alert");
	}
	public void adSkipped_extraLife(){
		print("ad skipped, show alert");
	}





	public void bp_ERB_TL_FG_run_freeGift(){
		Gift_Mng.Instance.run_freeGift();	
		Home_Mng.Instance.turnOn_giftScene();
		tf_deadBall.localPosition = new Vector3(-300, 0, 0);

		///hide ERB_slots...
		resetERBs_slots();//pos
		reset_erbs();//innards of unit
		hide_staticBtns();
		Coin_Mng.Instance.turnOn_HUD();
		Coin_Mng.Instance.tm_strokeParHud.text = "";

		Home_Mng.Instance.levelMng.playerMng.stopInput();
	}





	public void bp_ERB_run_winPrize(){
		Prize_Mng.Instance.reset();
		Home_Mng.Instance.turnOn_prizeScene();
		Prize_Mng.Instance.run();
		resetERBs_slots();
		hide_staticBtns();
		Coin_Mng.Instance.subtract_coinsToCoins(100);
		Home_Mng.Instance.levelMng.playerMng.stopInput();
	}





	void hide_staticBtns(){
		tf_staticBtns.localPosition = new Vector3(0,0,-10);
	}

	public void bp_ERB_run_earnCoins(){

	}




	public void OnApplicationQuit(){
		print("AT QUIT ERS");
		//store timeLimt_freegift value.....
		PlayerPrefs.SetFloat("TL_FG_m", TL_FG_m);
		PlayerPrefs.SetFloat("TL_FG_s", TL_FG_s);
		PlayerPrefs.Save();
	}

				
}
