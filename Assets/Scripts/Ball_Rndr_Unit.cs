﻿using UnityEngine;
using System.Collections;

public class Ball_Rndr_Unit : MonoBehaviour {

	///this is a behaviour of each ball renderer, when idle, moving, landing.....

	///will trigger various animations, particle effects etc. 


	public ParticleSystem ps_landed;
	public int int_landedParticles;

	public bool hasLandedAnim;
	public bool hasIdleAnim;
	public bool hasHitAnim;
	public bool hasFallAnim;

	public float f_idleDelay;

	public AudioSource ad_source;
	public AudioClip ac_landed;
	public AudioClip ac_hit;

	public Renderer[] rndr_landedSplat;



	public void landed(){


		if(ps_landed){
			ps_landed.Emit(int_landedParticles);
		}

		if(hasLandedAnim){
			//must animate idle...
			///ususally a tween, moving up and down, or rotating side to side, or scaling up and down.

		}


		if(ac_landed){
			//run audio clip of landed
			ad_source.clip = ac_landed;
			ad_source.Play();
		}


		if(rndr_landedSplat.Length > 0){
			//in case of hamburger, splatting mustard and ketchup and relish.
			
		}


		Invoke("runIdleAnim", f_idleDelay);

	}

	public void hit(){
		//cancel any idle anim and reset
		cancelIdleAnim();

		if(hasIdleAnim){
			//must cancel the idle anim
		}
		if(hasHitAnim){
			//must play hit anim.
		}
		if(ac_hit){
			//play hit audio
			ad_source.clip = ac_hit;
			ad_source.Play();
		}
	}

	void runIdleAnim(){
		//must run the idle animation

		//usually a tween of some sort (y(up, down - hoever) (scale, inout - breathing), wobble (back and forth rotate).






	}
	public void endIdleAnim(){
		//upon running the anim, run it again, lest there is no hit 
		Invoke("runIdleAnim", f_idleDelay);
	}
	void cancelIdleAnim(){
		/////
		CancelInvoke("runIdleAnim");
		//cancel and reset transform invloved in anim.

	}




	

}
