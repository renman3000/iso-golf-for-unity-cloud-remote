﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Enemy_Mouse : MonoBehaviour {

	
	[HideInInspector]
	public List<Grounds_Unit> grounds_units;
	int dir;


	// Use this for initialization
	void Start () {
		Home_Mng.Instance.enemies.Add(gameObject);
		Invoke("run", 1);
	}
	
	public void run(){
		//set grounds units for reference
		grounds_units = Home_Mng.Instance.levelMng.groundsMng.units;	

		//now begin cycle of the mouse scurry....
		get_direction();


		//check_nextTarget();
	}

	public void reset(){

	}

	void get_direction(){
		dir = Random.Range(1,5);
		check_nextTarget(dir);
	}


	void check_nextTarget(int dir){

		Vector3 newTarget = transform.position;

		if(dir == 1){
			newTarget = new Vector3(newTarget.x, newTarget.y, newTarget.z);
			transform.localEulerAngles = new Vector3(0,0,0);
		}
		if(dir == 2){
			newTarget = new Vector3(newTarget.x + 1, newTarget.y, newTarget.z);
			transform.localEulerAngles = new Vector3(0,270,0);
		}
		if(dir == 3){
			newTarget = new Vector3(newTarget.x, newTarget.y, newTarget.z - 1);
			transform.localEulerAngles = new Vector3(0,180,0);
		}
		if(dir == 4){
			newTarget = new Vector3(newTarget.x - 1, newTarget.y, newTarget.z);
			transform.localEulerAngles = new Vector3(0,90,0);
		}


		//check target pos in grounds units
		for(int i = 0; i < grounds_units.Count; i++){
			if(newTarget == grounds_units[i].nodeTarget.position){
				moveTo_newTarget(newTarget);
				return;
			}
		}

		///if here. no target was found, restart process.
		get_direction();
	}


	void moveTo_newTarget(Vector3 targetPos){
		transform.DOMove(targetPos, 0.75f, false).SetEase(Ease.InCubic).OnComplete(get_direction);
	}



	public void Kill(){
		print("KILL");
		transform.DOKill();
	}









}
