﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class Gift_Mng : MonoBehaviour {


	public static Gift_Mng Instance;
	public Transform tf_giftBox;
	public RectTransform rt_giftBox;
	Vector2 v2Size_giftBoxBtn;

	public Transform tf_masterCoin;//inside giftbox

	int coins_added;
	int coins_running;/// used to add up to coins added in animation to hud
	public RectTransform rt_giftContents;

	public Transform tf_conffetti;
	public ParticleSystem[] ps_conffetti;


	[HideInInspector]
	public List<Gift_Coin_Unit> coin_units;

	public TextMesh tm_coinsAdded;
	public Transform tf_coinsAdded;


	public Transform tf_hudTrgt;
	Vector3 hudTrgt_pos;


	Vector3 hudPos;
	float frq;


	void Awake(){
		Instance = this;
		v2Size_giftBoxBtn = rt_giftBox.sizeDelta;
		rt_giftBox.sizeDelta = Vector2.zero;
		rt_giftContents.sizeDelta = Vector2.zero;
		tf_masterCoin.localScale = Vector3.zero;
		tm_coinsAdded.text = "";
		hudTrgt_pos = tf_hudTrgt.position;
		tf_coinsAdded.localScale = Vector3.zero;
	}



	///////////////////
	/// /GIFT PROCEDURE
	//////////////////
	/////////////////
	////////////////
	///////////////----------
	public void run_freeGift(){
		///from ERS at ERB_free gift or ERB_freeCoinsWA
		Home_Mng.Instance.turnOn_giftScene();

		///animate gift box.....
		animate_giftBox_intoScene();
		
	}
	///
	void animate_giftBox_intoScene(){
		
		tf_giftBox.DOScale(Vector3.one, 0.25f).OnComplete(give_giftBox_bounce);
		rt_giftBox.sizeDelta = v2Size_giftBoxBtn;

		Vector3 rtn = new Vector3(0, 180, 0);
		tf_giftBox.DOLocalRotate(rtn, 20, 0).SetLoops(-1);



	}
	//
	public void bp_releaseGiftContents(){

		animate_giftBox_opening();



		//open the gift contents btn
		rt_giftContents.sizeDelta = v2Size_giftBoxBtn;
		//hide the gift btn
		rt_giftBox.sizeDelta = Vector3.zero;




	
		Invoke("animate_giftContents", 2);

	
	}
	//
	void animate_giftBox_opening(){
		tf_giftBox.DOKill();
		tf_giftBox.DOScaleX(0.75f, 0.25f).SetEase(Ease.OutBounce).SetLoops(-1, LoopType.Yoyo);
		tf_giftBox.DOScaleY(0.75f, 0.25f).SetEase(Ease.OutBounce).SetLoops(-1, LoopType.Yoyo);
		tf_giftBox.DOScaleZ(0.75f, 0.25f).SetEase(Ease.OutBounce).SetLoops(-1, LoopType.Yoyo);
	}
	void animate_giftContents(){

		

		//tf_conffetti.localScale = Vector3.one;

		for (int j = 0; j < ps_conffetti.Length; j++){
			//ps_conffetti[j].Play();
			//ps_conffetti[j].transform.localScale = Vector3.one;
		}
		print("????? gift confetti??");


		tf_giftBox.DOKill();
		tf_giftBox.DOScale(Vector3.zero, 0.25f);


		//tf_masterCoin.localEulerAngles = Coin_Mng.Instance.tf_coinIcon_hud.localEulerAngles;
		tf_masterCoin.localScale = Vector3.zero;
		tf_masterCoin.DOScale(Vector3.one, 0.25f);
		//Vector3 rtn = new Vector3(0, 180, 0);
		//tf_masterCoin.DOLocalRotate(rtn, 5, 0).SetLoops(-1).SetEase(Ease.Linear);

	
		coins_added = Random.Range(5, 20);
		coins_running = coins_added;

		tm_coinsAdded.text = coins_added.ToString();
		hudPos = Coin_Mng.Instance.tf_tm_coinsHUD.position;

		frq = 2f/(float)coins_added;
		InvokeRepeating("animate_coinToHud", frq, frq);
		tf_coinsAdded.localScale = Vector3.one;
	
	}



	void animate_coinToHud(){

		//coin_units[0].run(hudTrgt_pos, frq);	
		coins_running--;
		tm_coinsAdded.text = coins_running.ToString();

		if(coins_running == 0){
			CancelInvoke("animate_coinToHud");
			Invoke("reset", 1);
			return;
		}


		Invoke("coinAdded", frq);

	}

	public void coinAdded(){

		Transform tf_coinHud = Coin_Mng.Instance.tf_tm_coinsHUD;
		float scale =  tf_masterCoin.localScale.x - (1/coins_added);
		Vector3 newScale = new Vector3(scale, scale, scale);
		tf_masterCoin.DOScale(newScale, 0.25f).SetEase(Ease.InBounce);



		float scaleBump = tf_coinHud.localScale.x + (4/(float)coins_added);
		tf_coinHud.localScale = new Vector3(scaleBump, scaleBump, scaleBump);

		Coin_Mng.coins++;
		Coin_Mng.Instance.tm_coinsCollectedHud.text = Coin_Mng.coins.ToString();


	}



	void reset_giftBox(){
		tf_giftBox.DOKill();
		tf_giftBox.localScale = Vector3.zero;
		tf_giftBox.localEulerAngles = Vector3.zero;
		rt_giftBox.sizeDelta = Vector3.zero;

	}
	//
	public void bp_giveGiftContents(){

		print("bp_giveGiftContents");
		///animate gift into coins hud....
		///add coins added to coins
		for(int i = 0; i < coin_units.Count; i++){
			//coin_units[i].move_toHud();
		}


		Coin_Mng.Instance.add_coinstoCoins(coins_added);
		Coin_Mng.Instance.update_coinsInHUD();

		///hide contents Btn
		rt_giftContents.sizeDelta = Vector2.zero;

		for (int j = 0; j < ps_conffetti.Length; j++){
			ps_conffetti[j].Stop();
			ps_conffetti[j].transform.localScale = Vector3.zero;
		}

		Invoke("resetERS", 0.25f);
	}

	void give_giftBox_bounce(){
		tf_giftBox.DOScaleX(0.9f, 0.25f).SetLoops(-1, LoopType.Yoyo);
		tf_giftBox.DOScaleY(0.9f, 0.25f).SetDelay(0.125f).SetLoops(-1, LoopType.Yoyo); 
	}

	//
	void resetERS(){
	print("GIFT: resetERS");
		ERS_Mng.Instance.run_TL_FG();///restart clock for nect free gift...
		ERS_Mng.Instance.setERBS();///reset/update ERBS
		Home_Mng.Instance.turnOn_gameScene();
		tf_masterCoin.localScale = Vector3.one;
		tf_giftBox.localScale = Vector3.one;
	}


	void reset(){
		//reopen ers/erbs
		print("GIFT: reset");
		Transform tf_coinHud = Coin_Mng.Instance.tf_tm_coinsHUD;
		tf_coinHud.DOScale(Vector3.one, 0.25f).SetEase(Ease.OutBounce);
		ERS_Mng.Instance.run_TL_FG();
		ERS_Mng.Instance.openERS();
		tf_masterCoin.DOScale(Vector3.zero, 0.25f).SetEase(Ease.InBounce);
		tm_coinsAdded.text = "";
		tf_coinsAdded.localScale = Vector3.zero;
		coins_running = 0;
		coins_added = 0;
		Coin_Mng.Instance.Save();
		return;
	}
}
