﻿using UnityEngine;
using System.Collections;

public class LevelIntro_Mng : MonoBehaviour {


	public Player_Mng playerMng;

	string world;
	string hole;
	int par;
	int reward;

	public Camera cam;
	public RectTransform rtf_btn;


	void Start () {
		GameObject UI_Canvas = GameObject.Find("Root/UI/Canvas");
		transform.parent = UI_Canvas.transform;
		rtf_btn.sizeDelta = Vector2.zero;
		cam.gameObject.SetActive(false);
	}


	public void openScene(){
		rtf_btn.sizeDelta = new Vector2(375, 555);
		cam.gameObject.SetActive(true);
	}
	public void closeScene(){
		print("levelIntroMng::closeScene() ");
		rtf_btn.sizeDelta = Vector2.zero;
		cam.gameObject.SetActive(false);

	}





}
