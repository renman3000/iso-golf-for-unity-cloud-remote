﻿using UnityEngine;
using System.Collections;

public class ERB_AchievmentUnit : MonoBehaviour {


	public bool is_make3Holes;
	public bool is_take3Shots;

	int runningInt;
	int limitInt;
	
	// Use this for initialization
	void Start () {
		ERB_AchievmentMng.Instance.units.Add(this);
	}
	///
	public void run(){
		runningInt = 0;
		set_ERBText();
	}
	///
	void set_ERBText(){
		if(is_make3Holes){
			ERB_AchievmentMng.Instance.tm_ERB_Achievemnt.text = "MAKE 3 HOLES " + runningInt.ToString() + "/3";
			limitInt = 3;
		}
		if(is_take3Shots){
			ERB_AchievmentMng.Instance.tm_ERB_Achievemnt.text = "TAKE 3 SHOTS " + runningInt.ToString() + "/3";
			limitInt = 3;
		}
	}

	public void achievment_increment(){
		runningInt++;
		if(runningInt >= limitInt){
			ERB_AchievmentMng.Instance.achievement_made();
		}
	}


}
