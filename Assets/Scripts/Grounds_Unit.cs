﻿using UnityEngine;
using System.Collections;

public class Grounds_Unit : MonoBehaviour {

	public Grounds_Mng mng;

	[HideInInspector]
	public Level_Mng levelMng;


	//ESENTIALS
	[HideInInspector]
	public bool isHole;
	[HideInInspector]
	public bool isStart;
	[HideInInspector]
	public bool isExtent;
	//DETAILS
	[HideInInspector]
	public bool isWater;
	[HideInInspector]
	public bool isDirt;
	[HideInInspector]
	public bool isRoseBed;
	[HideInInspector]
	public bool isSand;


	[HideInInspector]
	public int unitType; ///0 = grass, 1 = sand, 2 = water, 3 = power up ...... 
	[HideInInspector]
	public Transform nodeTarget;

	public MeshRenderer rndr;

	public Collider cldr;


	void Awake(){








	}


	public void init_unit(int unitType){

		transform.tag = "GROUNDS_UNIT";

		nodeTarget = new GameObject().transform;
		nodeTarget.parent = transform;
		Vector3 nodePos = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
		nodeTarget.position = nodePos;
		//print("nodePos = " + nodePos + ", thisPos " + transform.position);

		nodeTarget.name = "NT" + nodeTarget.position.ToString();

		transform.name =  "GU " + transform.position.ToString();

		//if(rndr && !isWater)
		//rndr.transform.localScale = Vector3.one;

		//set mng
		if(Home_Mng.Instance){
			mng = Home_Mng.Instance.levelMng.groundsMng;
			mng.units.Add(this);
		}

		if(isHole){
			rndr.enabled = false;
			mng.levelMng.holeMng.setHole(transform);
		}

		if(isStart){
			Vector3 pos = nodeTarget.position;
			mng.levelMng.ballMng.set_ballAtStart(pos);
		}

		if(unitType == 3){
			isWater = true;
		}
		if(unitType == 5){
			isSand = true;
		}
		if(unitType == 9){
			isDirt = true;
		}
		if(unitType == 6){
			isRoseBed = true;
		}

		levelMng = Home_Mng.Instance.levelMng;


	}






	public void ping_groundUnit(Ball_Mng ballMng){
		//ball has landed on this... 
		ballMng.return_groundUnit(this);
	}




	public void reset(){
		transform.localScale = Vector3.one;
		isHole = false;
		if(rndr)
		rndr.enabled = true;
	
	}



	void OnTriggerEnter(Collider col){
		/*
		if(col.CompareTag("CLDR_BALL_VERT")){
			print("!! BALL CLDR VERT in reaction:, unit in reaction: " + transform.name);
			print("!! tf ball pos " + col.transform.position);
			levelMng.ballMng.ballHit_unforeseenCollider_VERT(transform, col.transform);
		}
		if(col.CompareTag("CLDR_BALL_HORZ")){
			print("!! BALL CLDR HORZ in reaction:, unit in reaction: " + transform.name);
			print("!! tf ball pos " + col.transform.position);
			levelMng.ballMng.ballHit_unforeseenCollider_HORZ(transform);
		}*/
	}



	public void runSplash(){
		mng.runSplash(this);
	}

}
