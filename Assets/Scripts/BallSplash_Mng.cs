﻿using UnityEngine;
using System.Collections;

public class BallSplash_Mng : MonoBehaviour {


	public BallSplash_Unit unit_water;

	public Material mtl_water;

	public Color water;
	public Color dirt;
	public Color sand;
	public Color roseBed;

	// Use this for initialization
	void Start () {
	
	}

	public void run(int type, Vector3 pos){
		if(type == 1){
			//splash water 
			//print("splash water");
			unit_water.run(pos);

		}

	}

}
