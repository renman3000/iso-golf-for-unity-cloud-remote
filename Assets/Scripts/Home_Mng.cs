﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;
using System;


public class Home_Mng : MonoBehaviour {

	/// <summary>
	/// the home mng is never destroyed, it will handle all scene transitions...

	///.... loading new level... set loading screen (never destroyed)
	///.... level data scene... never destoyed.. indo obtained from new loaded level
	///.... at level cleared, load new level, show loadfing scene... load level (random)

	////----- at quit, or die (die is caused by not sinking ball with inshot limit.....
	////----- close, destroy hole scene... show home scene.......
	////----- show if gift available... show if prize avqailabel.....
	////----- show random player for purchase.... 

	////.... levels will be prefabs that are slecgted at random on load new hole.....
	////.... levels prefabd include..... grounds mng, hole mng, level info mng, 
	///..... level info  mng will find home mng (static), pass it various data, mngs....
	///..... home mng will set player to start pos...... 
	///..... home mng will set level data (shot limit), 
	///..... home mng will set wqorld type.... (owned) occasional random


	/// </summary>




	public static Home_Mng Instance;
	public Transform tf_Canvas;
	public Transform tf_homeScene;
	public RectTransform rt_homeBtns;
	public GameObject gm_gameScene_StartBtns;
	public GameObject gm_playerBtns;
	public Text tm_tapToPlay;


	public Transform tf_CamClouds;//set into game scene cam as parent, and remove....level to level.

	public Transform tf_shotMeter;//passed to levelMng.playerMng.tf_hudShifter. Marks player swing power. 


	public LevelInfo_Mng levelInfoMng;

	public Cam_Mng camMng;

	public Store_Mng storeMng;


	public GameObject[] levels;
	int lastLevel;
	int crntLevel;

	[HideInInspector]
	public Level_Mng levelMng;


	[HideInInspector]
	public List<GameObject> enemies;

	GameObject gm_crntLevel;


	public Transform tf_hud;
	public Transform tf_levelNames_hud;
	public TextMesh tm_worldName;
	public TextMesh tm_holeName;

	DateTime currentDate;
    DateTime oldDate;



    [HideInInspector]
	public Transform tf_defaultBall_rndr;
	public Transform tf_homeScene_ball;
	public Transform tf_homeScene_ball_rndrAtStart;





	void Awake(){

		Instance = this;
		crntLevel = PlayerPrefs.GetInt("crntLevel");
		tf_levelNames_hud.localScale = Vector3.zero;
	}
	void Start(){
		lastLevel = -1;
		Invoke("turnOn_homeScene", 5);
		turnOn_loadingScene();
	

		// get current date
		DateTime date = DateTime.Now;
		 
		// convert to binary
		long date_bin = date.ToBinary();
		 		 
		// get the binary back from the save data
		long old_date_bin = long.Parse(PlayerPrefs.GetString("date"));
		 
		// convert from binary to datetime.
		DateTime oldDate = DateTime.FromBinary(old_date_bin);
		 
		// get the time difference
		TimeSpan ts = date - oldDate;
		 
		// get the difference in days
		float differenceInDate = ts.Minutes;

		float timeLimit = 60 * 5;//5 hours in minutes
		if(differenceInDate >= timeLimit || differenceInDate == 0){
			print("FRESH CONDENSED CLOCK");
			//set FG_TL to 1:30
			ERS_Mng.Instance.set_clock(1, 30);
		}
		else if(differenceInDate < timeLimit){
			print("NORMAL REFRESHED CLOCK");
			//set FG_TL to 59:59
			ERS_Mng.Instance.set_clock(59, 59);
		}

			
		

	}

	public void turnOn_homeScene(){
		camMng.turnOn_homeScene();
		rt_homeBtns.gameObject.SetActive(true);
		tf_homeScene.localScale = Vector3.one;
		gm_gameScene_StartBtns.SetActive(false);
		gm_playerBtns.SetActive(false);
		tm_tapToPlay.text = "TAP TO PLAY";
		Coin_Mng.Instance.turnOn_HUD();
		Coin_Mng.Instance.tm_strokeParHud.text = "";
		tf_levelNames_hud.localScale = Vector3.zero;
	}
	public void turnOn_loadingScene(){
		camMng.turnOn_loadingScene();
		rt_homeBtns.gameObject.SetActive(false);
		gm_gameScene_StartBtns.SetActive(false);
		gm_playerBtns.SetActive(false);
		tm_tapToPlay.text = "";
		tf_levelNames_hud.localScale = Vector3.zero;
	}
	public void turnOn_levelInfo(){
		camMng.turnOn_levelInfo();
		levelInfoMng.allowInput();
		rt_homeBtns.gameObject.SetActive(false);
		//gm_gameScene_StartBtns.SetActive(false);
		tm_tapToPlay.text = "";
		levelMng.camCntrl.frame_ball();
		/////must get data for level, adjust texts... 

	}
	public void turnOn_gameScene(){
		//sent from level info mng, 
		//camMng.turnOn_levelInfo();///only for bug fix since we are skipping turnOnLevelInfo()
		camMng.turnOn_gameScene();
		rt_homeBtns.gameObject.SetActive(false);
		tf_homeScene.localScale = Vector3.zero;
		levelMng.turnOn_gameScene();
		levelInfoMng.haltInput();
		gm_playerBtns.SetActive(true);
		//gm_gameScene_StartBtns.SetActive(true);
		tm_tapToPlay.text = "";
		//tf_CamClouds.parent = levelMng.camCntrl.tf_camFollow;
		//levelMng.holeMng.show_shots();
		tf_levelNames_hud.localScale = Vector3.one;
		tf_hud.localScale = Vector3.one;
	}

	public void turnOn_storeScene(){
		camMng.turnOn_storeScene();
		rt_homeBtns.gameObject.SetActive(false);
		storeMng.openStore();
		gm_playerBtns.SetActive(false);
		//gm_gameScene_StartBtns.SetActive(false);
		tm_tapToPlay.text = "";
		Coin_Mng.Instance.turnOff_HUD();
		tf_levelNames_hud.localScale = Vector3.zero;
	}


	public void turnOn_giftScene(){
		camMng.turnOn_giftScene();
		tm_tapToPlay.text = "";
		tf_levelNames_hud.localScale = Vector3.zero;
	}


	public void turnOn_prizeScene(){
		camMng.turnOn_prizeScene();
		tf_levelNames_hud.localScale = Vector3.zero;
	}


	public void bp_play(){
		//called from home scene
		print("bp play");
		///must get random prefabed level...
		loadLevel();

	}


	public void set_newLevel(){
		///from ERS at play next... clear current level mng, instanitate a new....

		print("1 set new level() ");


		if(gm_crntLevel != null){
			//tf_CamClouds.parent = transform;
			destroy_crntLevel();
		}



		loadLevel();


	}

	void destroy_crntLevel(){
		//first handle all artifacts of level
		if(tf_defaultBall_rndr != null){
			tf_defaultBall_rndr.parent = transform;
			tf_defaultBall_rndr.localScale = Vector3.zero;
		}
		levelMng.playerMng.rt_btn.parent = levelMng.playerMng.transform;
		
		Destroy(gm_crntLevel);
	}



	void loadLevel(){


		////must show loading scene(native)
		turnOn_loadingScene();

		/*
		//generate random prefabed level. 
		int rdmInt = Random.Range(0, levels.Length);
		if(lastLevel == rdmInt){
			///may retry, load level..leave for now...
			//loadLevel();
			//return;
			print("??? prevent doubles same level loaded");
		}*/

		gm_crntLevel = GameObject.Instantiate(levels[crntLevel]) as GameObject;
		crntLevel++;
		if(crntLevel >= levels.Length){
			crntLevel = 0;
		}

		//-->must wiat for ping from loaded level mng (newLevel_awake())

	}



	public void newLevel_awake(Level_Mng levelMng_del){
		print("new level awake ");
		///now we have the new level 

		//pass as level mng
		levelMng = levelMng_del;

		///we need to synch global assets to the new level and vice versa....
		levelInfoMng.set_levelInfo(levelMng);

		ERS_Mng.Instance.set_levelInfo(levelMng);

		levelMng.playerMng.tf_hudShifter = tf_shotMeter;

		Weather_Mng.Instance.setMood(levelMng);

		Invoke("turnOn_gameScene", 2f);//cam adjustments (not data - handled by above.)

		tf_levelNames_hud.localScale = Vector3.zero;
		tm_worldName.text = levelMng.name_world;
		tm_holeName.text = levelMng.name_level;


		if(tf_defaultBall_rndr != null){
			Transform tf_ballRndr = levelMng.ballMng.tf_rndr;
			//clear any old rndrs from tfballrndr
			foreach(Transform child in tf_ballRndr){
				if(child != tf_defaultBall_rndr){
					Destroy(child.gameObject);
				}
			}
			//add this default rndr to tf rndr.
			tf_defaultBall_rndr.parent = levelMng.ballMng.tf_rndr;
			tf_defaultBall_rndr.localScale = new Vector3(0.2f, 0.2f, 0.2f);
			tf_defaultBall_rndr.localPosition = Vector3.zero;
			tf_defaultBall_rndr.localEulerAngles = Vector3.zero;
			//set layers
			foreach(Transform r_child in tf_defaultBall_rndr){
				r_child.gameObject.layer = LayerMask.NameToLayer("Default");
			}
		}											
	}








	public void bp_home(){
		//this function can be pressed via store scene (while level active or not) and/or game scene. 


		print("bp_home ");



		//home btn pressed from store, while no game active
		if(gm_crntLevel == null){
			//prssed from store, while no level active
			turnOn_homeScene();
			rt_homeBtns.localScale = Vector3.one;
			tf_hud.localScale = Vector3.one;
			print("a");
			return;
		}
		if(gm_crntLevel != null){
			//pressed from store, while game scene active
			if(camMng.cam_store.gameObject.active){
				//then the store scene is open, and we are in it. 
				//... as such turn off store scene
				turnOn_gameScene();
				tf_hud.localScale = Vector3.one;
				print("b");
				return;
			}
			//pressed from game scene...
			if(!camMng.cam_store.gameObject.active){
				//in game scene, simply turn on home, destroy level.
				//tf_CamClouds.parent = Cloud_Mng.Instance.transform;
				turnOn_homeScene();
				destroy_crntLevel();
				rt_homeBtns.localScale = Vector3.one;
				print("c");
				return;
			}		  
		}
	}











	public void bp_store(){
		//prssed from home or game
		tf_hud.localScale = Vector3.zero;

		//pressed from home
		if(gm_crntLevel == null){
			///open store from home scene
			turnOn_storeScene();
			return;
		}


		//pressed from game
		if(gm_crntLevel != null){
			//open store, from game scene
			turnOn_storeScene();
			levelMng.playerMng.stopInput();
			return;
		}

		Coin_Mng.Instance.tm_strokeParHud.text = "";




	}







	public void bp_playGame(){
		//from game scene.
		levelMng.playerMng.startInput();
		levelMng.camCntrl.flyTo_Ball(levelMng.ballMng.tf_ball.position);
		gm_gameScene_StartBtns.SetActive(false);
	
	}



	public void bp_seeGrounds(){
		print("bp see grounds");
		levelMng.camCntrl.show_BallVsHole();
	}





	public void holeSunk(){
		print("HomeMng, holeSunk()");
		ScreenShot_Mng.Instance.takePicture();
		levelMng.holeMng.holeSunk();
		levelMng.playerMng.stopInput();
		levelMng.camCntrl.ballSunk();
		Coin_Mng.Instance.holeSunk();
		gm_playerBtns.SetActive(false);
		ERS_Mng.Instance.ballSunk();
		tf_levelNames_hud.localScale = Vector3.zero;
	}





	public void atDeath(){
		///ball has reached, passed stroke limit of level.....
		Coin_Mng.Instance.turnOff_HUD();
		levelMng.atDeath();
		//ERS_Mng.Instance.atDeath();
		gm_playerBtns.SetActive(false);
		tf_levelNames_hud.localScale = Vector3.zero;
	}





	public void set_defaultBall(GameObject newDefaultBall){
		//called at 1) prize won, 2) ball purchased.. we recive a transfrom which has the new ball renderer in it. 



		print("SET DEFAULT BALL " + newDefaultBall.name);
		//store the ball here. 
		tf_defaultBall_rndr = newDefaultBall.transform;
		tf_defaultBall_rndr.parent = tf_homeScene_ball;
		tf_defaultBall_rndr.localScale = new Vector3(0.8669436f, 0.8669436f, 0.8669436f);
		tf_homeScene_ball_rndrAtStart.localScale = Vector3.zero;
		tf_defaultBall_rndr.localPosition = Vector3.zero;

		//in case a round is active, pass the ball to the ball mng, 
		if(levelMng != null){
			levelMng.ballMng.check_ifCustomBall();
		}
	

	}





	public void OnApplicationQuit(){
		///called at quit game...
		PlayerPrefs.SetInt("crntLevel", crntLevel);



		//Savee the current system time as a string in the player prefs class
        PlayerPrefs.SetString("date", DateTime.Now.ToBinary().ToString());
		PlayerPrefs.Save();
        print("Saving this date to prefs: " + DateTime.Now);
		print("√ SAVE - crnt level at quit");
	}



	public void kill_allEnemies(){
		for(int i = 0; i < enemies.Count; i++){
			enemies[i].SendMessage("Kill");
		}
	}




}
