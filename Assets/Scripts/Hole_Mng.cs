﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class Hole_Mng : MonoBehaviour {

	[HideInInspector]
	public Level_Mng levelMng;


	[HideInInspector]
	public int strokeCount;
	int currentHole;
	public int par;



	public Transform tf_flag;
	public Transform tf_win;
	public Transform tf_conffetti;
	public Transform tf_holeLight;

	Vector3 flag_startPos;

	public Transform tf_Lose;
	public Transform tf_Lose_sheet;
	public SpriteRenderer rndr_loseSheet;


	public ParticleSystem[] ps_conffetti;


	public TextMesh tm_shotsTaken;



	public TextMesh tm_flagPar;

	void Awake(){
		strokeCount = par;
		reset_tf_lose();
	}

	void reset_tf_lose(){
		Vector4 clr = new Vector4(rndr_loseSheet.color.r, rndr_loseSheet.color.g, rndr_loseSheet.color.b, 0);
		rndr_loseSheet.color = clr;
		tf_Lose.localScale = Vector3.zero;
	}


	// Use this for initialization
	void Start () {
		//tf_flag.DOLocalRotate(new Vector3(0,-24,0), 4, 0).SetLoops(-1, LoopType.Yoyo); 
		tf_win.localScale = Vector3.zero;
	

		for (int j = 0; j < ps_conffetti.Length; j++){
			ps_conffetti[j].Play();
			ps_conffetti[j].transform.localScale = Vector3.zero;
		}


		tf_Lose.localScale = Vector3.zero;


		hide_shots();
	}


	public void setHole(Transform tf_hole){
		print("setHole");
		tf_flag.position = tf_hole.position;
		tf_holeLight.position = tf_hole.position;
		flag_startPos = tf_hole.position;
		tf_conffetti.position = tf_hole.position;
		tf_win.position = new Vector3(-1000, -1000, -1000);
		par = levelMng.shotLimit;
		strokeCount = par;
		tm_flagPar.text = par.ToString();


	}


	public void setPar(Vector3 ballPos){
		/*
		float distanceToHole = Vector3.Distance(ballPos, tf_flag.position);
		distanceToHole = Mathf.RoundToInt(distanceToHole);
		par = (int)distanceToHole;
		print("PAR: " + par);
		*/
		print("???? needed ? setPar() =  " + par);

	}





	public void holeSunk(){
		tf_win.localScale = Vector3.zero;///allow the ScreenShotMng, to reset scale after default pic taken.
		tf_win.position = tf_flag.position;
		float newYPos = tf_flag.position.y + 5;
		tf_flag.DOLocalMoveY(newYPos, 1, false).SetEase(Ease.OutBounce);
		tf_win.DOLocalMoveY(newYPos, 1,false).SetEase(Ease.OutBounce);
		tf_conffetti.DOLocalMoveY(newYPos, 1,false).SetEase(Ease.OutBounce);
		tf_win.localScale= Vector3.one;

		for (int j = 0; j < ps_conffetti.Length; j++){
			ps_conffetti[j].transform.localScale = Vector3.one;
		}





	}


	public void atDeath(){
		tf_Lose.position = levelMng.ballMng.tf_ball.position;
		tf_Lose.localScale = Vector3.one;
		float yPos = tf_Lose.position.y + 5;
		tf_Lose.DOLocalMoveY(yPos, 1, false).SetEase(Ease.OutBounce);
		rndr_loseSheet.DOFade(0.5f, 0.5f);
		tf_flag.localScale = Vector3.zero;

	}




	public void hide_shots(){
		print("????");
		if(tm_shotsTaken)
		tm_shotsTaken.text = "";
		Coin_Mng.Instance.tm_strokeParHud.text = "";
	}	
	public void show_shots(){
		Number_Mng.Instance.show_shots(strokeCount);
		tm_flagPar.text = strokeCount.ToString();

		Coin_Mng.Instance.tm_strokeParHud.text = strokeCount.ToString() + "/" + par.ToString(); 

		//print(Time.time + " shotLimit = " + levelMng.shotLimit);
		//if(strokeCount == 0){
			
		//}


		//tm_shotsTaken.text = strokeCount.ToString() + "/" + shotLimit.ToString();
	}

	public void strokeTaken(){
		strokeCount--;
		//par--;
		show_shots();
	}


	public void removeShots_HUD(){
		//@all strokes passed to bank, must tween out par transform.



	}





	public void resetScene(){
		tf_flag.position = flag_startPos;
		tf_conffetti.position = flag_startPos;
		tf_win.position = new Vector3(-1000, -1000, -1000);
		tf_win.localScale = Vector3.zero;


		for (int j = 0; j < ps_conffetti.Length; j++){
			ps_conffetti[j].transform.localScale = Vector3.zero;
		}
	}




	public void flyTo_hole(){
		Home_Mng.Instance.levelMng.camCntrl.flyTo_Hole();
	}
	public void flyTo_ball(){
		Home_Mng.Instance.levelMng.camCntrl.flyTo_Ball(levelMng.ballMng.tf_ball.position);
	}



}
