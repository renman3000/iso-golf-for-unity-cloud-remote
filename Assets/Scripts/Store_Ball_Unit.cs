﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


//tire 7.25
//sushi 2.5
//red squirrel 1.55
//golf 1.5
//bird 3.5
//t rex 4.5
//cloud 0.8
//polar 1.87
//turkey 3.5
//ufo 1

public class Store_Ball_Unit : MonoBehaviour {

	public Store_Mng storeMng;
	[HideInInspector]
	public bool isOwned;
	Vector3 v3_draggerPos;
	Vector3 v3_draggerScale = new Vector3(0.5f, 0.5f, 0.5f);
	public bool isPrize;//some balls can be won via prize, some can not, this controls that. 
	public string name;
	public int insertPoint;//for position in mngs list. 

	public GameObject prefabbed_rep;

	public float intensity_spotlight;//passed to manager, used when this ball on display in store


	// Use this for initialization
	void Start () {
		if(prefabbed_rep == null){
			transform.localScale = Vector3.zero;
			return;
		}

		storeMng.balls.Add(this);
		if(isPrize){
			//Prize_Mng.Instance.storeBallUnits.Insert(insertPoint, this);
			Prize_Mng.Instance.storeBallUnits.Add(this);
		}


		int int_isOwned = PlayerPrefs.GetInt(name + "isOwned");
		if(int_isOwned == 0){
			isOwned = false;
		}
		if(int_isOwned == 1){
			isOwned = true;
		}

	}

	public void setBall(int i){
		///////////----------------iii
		transform.parent = storeMng.tf_ballsDragger;
		transform.localScale = v3_draggerScale;
		float xPos = (float) i * 5;
		if(i == 0){
			xPos = 17 * 5;
		}
		transform.localPosition = new Vector3(xPos, 0, 0);
		v3_draggerPos = transform.localPosition;
		if(name == "GOLF BALL"){
			print("is GOLF BALL, i = " + i);
			isOwned = true;
			set_toDisplay();
			storeMng.spotLight.intensity = intensity_spotlight;
			storeMng.activeUnit = this;
			storeMng.store_activeUnit_ball();
			v3_draggerPos = Vector3.zero;
		}
	}

	public void set_toDisplay(){
		print(transform.name + "set_toDisplay()" );
		//pop out of dragger
		transform.parent = storeMng.tf_ballsDisplay;	


		//scale up
		transform.DOScale(Vector3.one, 0.25f);

		///tween to zero pos....
		transform.DOLocalMove(Vector3.zero, 0.25f, false);


		storeMng.tm_ball.text = name;
						
	}

	public void set_toDragger(){
		print(transform.name + "set_toDragger()" );
		//pop into dragger 
		transform.parent = storeMng.tf_ballsDragger;

		///scale down to dragger size
		transform.DOScale(v3_draggerScale, 0.25f);

		///move to default pos in dragger
		transform.DOLocalMove(v3_draggerPos, 0.25f, false);


	}







	public void unlock(){
		///pass ball renderer into the home managers default ball, and the (if ball mng, tf ball.
		isOwned = true;
		PlayerPrefs.SetInt(name + "isOwned", 1);
		PlayerPrefs.Save();
		print("√SAVE- purchased ball " + name);
	}


}
