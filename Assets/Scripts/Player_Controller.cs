﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Player_Controller : MonoBehaviour {
/*
	public Ball_Unit ball;

	public Grounds_Mng groundsMng;
	public Camera_Controller camCntrl;
	public Hole_Mng holeMng;

	Vector2 startPos_finger;
	Vector2 endPos_finger;
	Vector2 crntPos_finger;
	Vector2 deltaPos_finger;



	Vector3[] tn_nodes = new Vector3[3];
	public Transform tf_ball;	
	public Transform tf_ballHolder;//this will act as a parent to the ball at start swing... doing so, insures the local position of tf ball will always be 0
	Vector3 ball_startPos;
	public Collider cldr_ball;

	Vector3 endPos_ball;//use local space to tf ball holder... used for tween ball
	Vector3 midPos_ball;
	Vector3 lastPos_ball;


	float dist_ball;
	float dist_ball_f;/// <summary>
	/// used to show incremental (not rounded to int.. for percsion judgement, not placement...)
	/// </summary>
	int int_trajectory;//1 = fwd, 2 = right, 3, back, 4 = left.

	float hang_time;
	float height_full;

	public TrailRenderer tr_ball;



	public Transform tf_club;
	public Transform tf_clubHolder;
	float swingTime;
	float clubRtn_peak;

	bool ballActive;
	bool isAirBall;
	bool isBallSunk;


	// Use this for initialization
	void Start () {


		DOTween.Init();
		Invoke("initScene", 1);



	}


	void initScene(){
		///set start pos of ball.....
		for(int i = 0; i < groundsMng.units.Count; i++){
			if(groundsMng.units[i].isStart){
				tf_ball.position = groundsMng.units[i].nodeTarget.position;
				ball_startPos = tf_ball.position;
				lastPos_ball = ball_startPos;
			}
		}
		//reset camera
		camCntrl.reset(tf_ball);
		//restart input
		restart_monitorInput();
		//reset club
		resetClub_pos();
		///get the distance between ball pos and new hole to set par.....
		holeMng.setPar(tf_ball.position);
	}


	IEnumerator monitorInput(){
		bool monitoring = true;
		while(monitoring){
			if(Input.GetMouseButtonDown(0)){
				markStartPoint_swing(Input.mousePosition);
			}
			if(Input.GetMouseButton(0)){
				monitorSwing(Input.mousePosition);
			}
			if(Input.GetMouseButtonUp(0)){
				release(Input.mousePosition);
			}
			yield return null;

		}
	}

	public void halt_monitorInput(){
		StopAllCoroutines();
	}



	void markStartPoint_swing(Vector2 mousePos){

		if(ballActive){
			return;
		}

		//insure a zero local position.....
		tf_ballHolder.position = tf_ball.position;
		//tf_ball.parent = tf_ballHolder;


		startPos_finger = Camera.main.ScreenToWorldPoint(mousePos);
	
		
		tf_club.localEulerAngles = Vector3.zero;


	}


	/// FOR THE ANIMATION OF THE CLUB....
	void monitorSwing(Vector2 mousePos){

		if(ballActive){
			return;
		}
		

		///WE MUST KNOW THE SWING AIM SO WE CAN CREATE THE RIGHT SWING ANIM

		tf_club.localScale = Vector3.one;
		crntPos_finger = Camera.main.ScreenToWorldPoint(mousePos);

		int int_localTrajectory = 0;

		if(startPos_finger.x > crntPos_finger.x){
			if(startPos_finger.y > crntPos_finger.y){
				///from top right to bottm left..... swinging to move forward...

				//set trajectory of ball (maybe referenced by Grounds Mng)
				int_localTrajectory = 1;
	
			}
			if(startPos_finger.y < crntPos_finger.y){
				///form bottom right to top left... swinging to move right....

				//set trajectory of ball (maybe referenced by Grounds Mng)
				int_localTrajectory = 2;

			}
		}
		if(startPos_finger.x < crntPos_finger.x){
			if(startPos_finger.y > crntPos_finger.y){
				///from top left to to bottom right... swingning to move left....
				//set trajectory of ball (maybe referenced by Grounds Mng)
				int_localTrajectory = 4;
			}
			if(startPos_finger.y < crntPos_finger.y){
				////from bottom left to top right... swinging to move back
				//set trajectory of ball (maybe referenced by Grounds Mng)
				int_localTrajectory = 3;
			}
		}


		//set rotation of club holder based on trajectory....
		float rtnFctr = 0;
		if(int_localTrajectory == 1){
			tf_clubHolder.localEulerAngles = Vector3.zero;
			rtnFctr = (crntPos_finger.x - startPos_finger.x) * 10;
			tf_club.localEulerAngles = new Vector3(-rtnFctr,0,0);
		}
		if(int_localTrajectory == 2){
			tf_clubHolder.localEulerAngles = new Vector3(0,90,0);
			rtnFctr = (-1) * (crntPos_finger.x - startPos_finger.x) * 10;
			tf_club.localEulerAngles = new Vector3(rtnFctr,0,0);
		}
		if(int_localTrajectory == 3){
			tf_clubHolder.localEulerAngles = new Vector3(0,0,0);
			rtnFctr = ((-1) * (crntPos_finger.x - startPos_finger.x) * 10) + 360;
			tf_club.localEulerAngles = new Vector3(rtnFctr,0,0);
		}
		if(int_localTrajectory == 4){
			tf_clubHolder.localEulerAngles = new Vector3(0,90,0);
			rtnFctr = ((-1) * (crntPos_finger.x - startPos_finger.x) * 10) + 360;
			tf_club.localEulerAngles = new Vector3(rtnFctr,0,0);
		}



		///for the swing hud.....
		if(rtnFctr > 180){
			rtnFctr = Mathf.Abs(rtnFctr - 360);
		}
		set_SwingHud(rtnFctr);
	}





	public Transform tf_powerBar;
	public Transform tf_powerMarker;

	void set_SwingHud(float swingFactr){

		
		swingFactr = (Mathf.Abs(swingFactr/12));
		if(swingFactr > 9.5f){
			swingFactr = 9.5f;
		}
		tf_powerMarker.localPosition = new Vector3(-swingFactr, 0, 0);

	}


	void reset_powerBar(){
		swingTime = Mathf.Abs(tf_powerMarker.localPosition.x/12);
		tf_powerMarker.DOLocalMoveX(0, swingTime, false).OnComplete(run_ball_tn); 
		swingClub_0();
	}









	void release(Vector2 mousePos){
		

		if(ballActive){
			return;
		}

		print("---------- RELEASE -------");
		StopAllCoroutines();


		tf_ballHolder.position = tf_ball.position;
		tf_ball.parent = tf_ballHolder;
		tn_nodes = new Vector3[0];
		endPos_finger = Camera.main.ScreenToWorldPoint(mousePos);
		determine_trajectory();



	}


	void determine_trajectory(){
		print("-------DETERMINE TRAJECTORY------ ");
		if(ballActive)
		return;
		ballActive = true;

		///get distance or hypotenuses of swing 
		float dist = Vector2.Distance(startPos_finger, endPos_finger);
		dist = dist * 2;
		if(dist < 1){
			print("SWING < 1 ");
			StopAllCoroutines();
			resetBall();
			return;
		}



		///round distance to a int...
		dist_ball = Mathf.RoundToInt(dist);
		dist_ball_f = dist;

		///GET TRAJECTORY OF SWING.....
		///caluculate direction of swing.... and set startPos, endPos and mid pos for nodes in ball tn
		if(startPos_finger.x > endPos_finger.x){
			if(startPos_finger.y > endPos_finger.y){
				///from top right to bottm left..... swinging to move forward...

				//set trajectory of ball (maybe referenced by Grounds Mng)
				int_trajectory = 1;

				///set the end ball pos...
				float zPos = dist_ball;
				endPos_ball = new Vector3(0,0,zPos);
			}
			if(startPos_finger.y < endPos_finger.y){
				///form bottom right to top left... swinging to move right....

				//set trajectory of ball (maybe referenced by Grounds Mng)
				int_trajectory = 2;

				///set the end ball pos...
				float xPos = dist_ball;
				endPos_ball = new Vector3(xPos,0,0);
			}
		}
		if(startPos_finger.x < endPos_finger.x){
			if(startPos_finger.y > endPos_finger.y){
				///from top left to to bottom right... swingning to move left....
				//set trajectory of ball (maybe referenced by Grounds Mng)
				int_trajectory = 4;

				///set the end ball pos...
				float xPos = -dist_ball;
				endPos_ball = new Vector3(xPos,0,0);
			}
			if(startPos_finger.y < endPos_finger.y){
				////from bottom left to top right... swinging to move back
				//set trajectory of ball (maybe referenced by Grounds Mng)
				int_trajectory = 3;

				///set the end ball pos...
				float zPos = -dist_ball;
				endPos_ball = new Vector3(0,0,zPos);
			}
		}


		//turn on trail renderer
		tr_ball.enabled = true;
		print("DIST BALL = " + dist_ball);

		reset_powerBar();///ob complete will fire swing.

		if(dist_ball == 0){
			resetBall();
			return;
		}




		///before we set the tween, we must see if the ball will land on a playable area... 
		/// please consider, heights of playable areas, hazards, no playable area......
		/// this will return a targt grounds unit, if not.. we know an air ball is fired. 
		///... once returned, we can set the tn for ball hit ...
		//groundsMng.check_ballDestination_forUnit(int_trajectory, dist_ball, tf_ball, dist_ball_f);




	}


	Grounds_Unit trgt_ground_unit;
	public void found_destinedUnit(Grounds_Unit trgt_ground_unit_del){

		trgt_ground_unit = trgt_ground_unit_del;
		print("-- FOUND DESTINE UNIT: " + trgt_ground_unit.transform.name);
		endPos_ball = trgt_ground_unit.nodeTarget.position;
		print(" END BALL POS = " + endPos_ball);
		set_ball_tn();

	}

	public void swingMissed_anyUnit(){
		print("SWING MISSED ANY UNIT " + Time.time);
		isAirBall = true;
		tn_nodes = new Vector3[2];
		hang_time = dist_ball/4;
		tn_nodes[0] = new Vector3(tf_ball.position.x, tf_ball.position.y + dist_ball, tf_ball.position.z + dist_ball);
		tn_nodes[1] = new Vector3(tf_ball.position.x, tf_ball.position.y - dist_ball, tf_ball.position.z + dist_ball/4);
		tf_ball.DOPath(tn_nodes, hang_time, PathType.CatmullRom, PathMode.Full3D, 10, null).OnComplete(resetBall);
		camCntrl.followBall(tn_nodes[1], hang_time);
	}






	void set_ball_tn(){
		print("------- SET BALL TN, as " + int_trajectory + " ---------");
		///use the start, mid and end pos to set the positions of the ball's tween

		if(int_trajectory == 1){
			set_trajectory1();	
		}
		if(int_trajectory == 2){
			set_trajectory2();
		}
		if(int_trajectory == 3){
			set_trajectory3();
		}
		if(int_trajectory == 4){
			set_trajectory4();
		}
	
	}




	void set_trajectory1(){
		///ball hit fwd...

		//account for varying hieghts..
		float y_offset = Mathf.Abs(endPos_ball.y - tf_ball.position.y);
		height_full = Mathf.Abs((dist_ball) + y_offset);
		float zDiff = (endPos_ball.z - tf_ball.position.z);




		Vector3 nodePos = Vector3.zero;///init var here... used thruout.

		tn_nodes = new Vector3[2];

	
		//BALL LAUNCHED


		//BALL AT FIRT PEAK 
		//first mid air pos
		nodePos.x = tf_ball.position.x;
		nodePos.y = height_full;
		nodePos.z = (zDiff/2) + tf_ball.position.z;
		tn_nodes[0] = nodePos;



		///BALL HIT GROUND			
		//first ground pos
		nodePos.x = tf_ball.position.x;
		nodePos.y = endPos_ball.y;
		nodePos.z = endPos_ball.z;

		///set last node as nodePos
		tn_nodes[1] = nodePos;



	}


	void set_trajectory2(){
		///ball hit right...

		//account for varying hieghts..
		float y_offset = Mathf.Abs(endPos_ball.y - tf_ball.position.y);
		height_full = Mathf.Abs((dist_ball) + y_offset);
		float xDiff = (endPos_ball.x - tf_ball.position.x);




		Vector3 nodePos = Vector3.zero;///init var here... used thruout.

		tn_nodes = new Vector3[2];

	
		//BALL LAUNCHED


		//BALL AT FIRT PEAK 
		//first mid air pos
		nodePos.x = (xDiff/2) + tf_ball.position.x;
		nodePos.y = height_full;
		nodePos.z = tf_ball.position.z;
		tn_nodes[0] = nodePos;



		///BALL HIT GROUND			
		//first ground pos
		nodePos.x = endPos_ball.x;
		nodePos.y = endPos_ball.y;
		nodePos.z = tf_ball.position.z;

		///set last node as nodePos
		tn_nodes[1] = nodePos;
	


	}
	void set_trajectory3(){


		//account for varying hieghts..
		float y_offset = Mathf.Abs(endPos_ball.y - tf_ball.position.y);
		height_full = Mathf.Abs((dist_ball) + y_offset);
		float zDiff = -Mathf.Abs(endPos_ball.z - tf_ball.position.z);




		Vector3 nodePos = Vector3.zero;///init var here... used thruout.

		tn_nodes = new Vector3[2];

	
		//BALL LAUNCHED


		//BALL AT FIRT PEAK 
		//first mid air pos
		nodePos.x = tf_ball.position.x;
		nodePos.y = height_full;
		nodePos.z = (zDiff/2) + tf_ball.position.z;
		tn_nodes[0] = nodePos;



		///BALL HIT GROUND			
		//first ground pos
		nodePos.x = tf_ball.position.x;
		nodePos.y = endPos_ball.y;
		nodePos.z = endPos_ball.z;

		///set last node as nodePos
		tn_nodes[1] = nodePos;


	}



	void set_trajectory4(){
		///ball hit left...



		//account for varying hieghts..
		float y_offset = Mathf.Abs(endPos_ball.y - tf_ball.position.y);
		height_full = Mathf.Abs((dist_ball/2) + y_offset);
		float xDiff = -Mathf.Abs(endPos_ball.x - tf_ball.position.x);




		Vector3 nodePos = Vector3.zero;///init var here... used thruout.

		tn_nodes = new Vector3[2];

	
		//BALL LAUNCHED


		//BALL AT FIRT PEAK 
		//first mid air pos
		nodePos.x = (xDiff/2) + tf_ball.position.x;
		nodePos.y = height_full;
		nodePos.z = tf_ball.position.z;
		tn_nodes[0] = nodePos;



		///BALL HIT GROUND			
		//first ground pos
		nodePos.x = endPos_ball.x;
		nodePos.y = endPos_ball.y;
		nodePos.z = tf_ball.position.z;

		///set last node as nodePos
		tn_nodes[1] = nodePos;


	}




	void run_ball_tn(){

		if(trgt_ground_unit == null)
		return;

		//store this ball pos @ currnet as last pos, in case of air ball....
		lastPos_ball = tf_ball.position;

		print("ball strt pos = " + tf_ball.position);
		for(int i = 0; i < tn_nodes.Length; i ++){
			print(" tn_nodes[" + i + "] = " + tn_nodes[i]);
		}

		//set time... account for height differnece 
		hang_time = Mathf.Abs(dist_ball)/8;
		///now set the pathtween....
		tf_ball.DOPath(tn_nodes, hang_time, PathType.CatmullRom, PathMode.Full3D, 10, null).SetEase(Ease.InOutSine).OnUpdate(updateTween).OnComplete(resetBall);
		camCntrl.followBall(endPos_ball, hang_time);


	}




	public void ballHitWall(Transform nodeTarget_del){
		print("BALL HIT WALL ");
		print("nodePos = " + nodeTarget_del.position + " vs endPos" + endPos_ball);
		if(nodeTarget_del.position == endPos_ball){
			//do nothing, allow the tween to complete.


		}

		else if(nodeTarget_del.position != endPos_ball){
			//killBall();
		}
	}





	void killBall(){
		print("BALL EXPLODE!!!");
		isAirBall = true;
		Invoke("resetBall", 1);
		tf_ball.DOKill();
		camCntrl.cancelFollow();
	}





	void resetBall(){
		//in case it was an air ball...
		if(isAirBall){
			print("reset AirBall()");
			isAirBall = false;
			tf_ball.position = lastPos_ball;
			camCntrl.reset_forAirBall(tf_ball);
		}
		print("---- RESET BALL --- pos @ " + tf_ball.position);
		tf_ball.parent = transform;
		ballActive = false;
		tr_ball.enabled = false;
		hideClub();
		holeMng.strokeTaken();
		cldr_ball.enabled = true;


		if(trgt_ground_unit != null && trgt_ground_unit.isHole){
			print("BALL SUNK !!!!");
			ballSunk();
		}
		else{
			Invoke("restart_monitorInput", 0.1f);
		}

		trgt_ground_unit = null;


	}



	public ERS_Mng ersMng;



	void ballSunk(){
		///ball has been hit intot he hole.....
		//as such we need to animate the win.....
		isBallSunk = true;
		holeMng.holeSunk();
		tf_ball.DOLocalMoveY(-2, 1, false);
		Invoke("resetScene", 5);
		ersMng.ballSunk();
	}








	float lastPos_x;
	float lastPos_y;
	float lastPos_z;

	float crntPos_x;
	float crntPos_y;
	float crntPos_z;

	float deltaPos_x;
	float deltaPos_y;
	float deltaPos_z;

	Vector3 delta_vector;


	void reset_tweenUpdate(){
		lastPos_x = 0;
		lastPos_y = 0;
		lastPos_z = 0;
	}

	void updateTween(){

		crntPos_x = tf_ball.position.x;
		crntPos_y = tf_ball.position.y;
		crntPos_z = tf_ball.position.z;

		deltaPos_x = crntPos_x - lastPos_x;
		deltaPos_y = crntPos_y - lastPos_y;
		deltaPos_z = crntPos_z - lastPos_z;


		lastPos_x = crntPos_x;
		lastPos_y = crntPos_y;
		lastPos_z = crntPos_z;

		delta_vector = new Vector3(deltaPos_x, deltaPos_y, deltaPos_z);


	}
















	void resetScene(){
		//reset main input breaker...
		isBallSunk = false;
		///trgt grounf unit
		trgt_ground_unit = null;
		//reset ball pos
		tf_ball.position = ball_startPos;
		//reset camera
		camCntrl.reset(tf_ball);
		//restart input
		restart_monitorInput();
		//reset club
		resetClub_pos();
		///get the distance between ball pos and new hole to set par.....
		holeMng.resetScene();
	}



	public void restart_monitorInput(){	
		if(isBallSunk)
		return;

		StartCoroutine(monitorInput());
	}






	void resetClub_pos(){
		tf_clubHolder.position = new Vector3(tf_ball.position.x, tf_ball.position.y - 1, tf_ball.position.z);
	}

	void swingClub_0(){		
		clubRtn_peak = tf_club.localEulerAngles.x;
		tf_club.DOLocalRotate(Vector3.zero, swingTime, 0).OnComplete(swingClub_1); 
	}

	void swingClub_1(){
		/// club is at 0 rtn... just made "contact" with ball...
		/// tween thru to mirror rtn
		tf_club.DOLocalRotate(new Vector3(-clubRtn_peak, 0,0), swingTime, 0);
	}


	void hideClub(){
		tf_clubHolder.position = new Vector3(tf_ball.position.x, tf_ball.position.y - 1, tf_ball.position.z);
		tf_club.localScale = Vector3.zero;
		swingTime = 0;
		clubRtn_peak = 0;
	}

	*/

}
