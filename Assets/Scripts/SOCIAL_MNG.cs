﻿using UnityEngine;
using System.Collections;
using DG.Tweening;



public class SOCIAL_MNG : MonoBehaviour {
	

	public Transform tf_pu;
	public Transform tf_homeBtn;

	string string1;
	string string2;
	string string3;
	public TextMesh tm_line1;
	public TextMesh tm_line2;
	public TextMesh tm_line3;


	public static SOCIAL_MNG instance;


		

	void Awake(){
		DontDestroyOnLoad(transform.gameObject);
		//print ("SOCIAL MNG ");
		//events use example

		IOSSocialManager.OnFacebookPostResult += HandleOnFacebookPostResult;
		IOSSocialManager.OnTwitterPostResult += HandleOnTwitterPostResult;
		IOSSocialManager.OnInstagramPostResult += HandleOnInstagramPostResult;


		instance = this;

	}
	// Use this for initialization
	void Start () {
		tf_pu.localPosition = new Vector3(-15, 0, 0);
		tf_homeBtn.localScale = Vector3.zero;
	}


	public void post_pic_FB(Texture2D pic){
		runPopUp(10);
		IOSSocialManager.Instance.FacebookPost("", "", pic);
	}
	public void post_pic_TW(Texture2D pic){
		IOSSocialManager.Instance.TwitterPost("", "", pic);	
	}
	public void post_pic_IG(Texture2D pic){
		IOSSocialManager.Instance.InstagramPost(pic);
	}
	public void save_pic_localDevice(){

	}


	void runPopUp(int caseType){

		//facebook
		if(caseType == 10){
			//facebook post attempt
			string1 = "PLASE WAIT";
			string2 = "WHILE WE";
			string3 = "CONNECT";
		}
		if(caseType == 11){
			//facebook post success
			string1 = "HOORAY!";
			string2 = "A SUCCESS";
			string3 = "ZUCKERBERG OUT";
		}
		if(caseType == 12){
			//facebook post fail'
			string1 = "SORRY";
			string2 = "CANT POST";
			string3 = "RIGHT NOW";
		}

		//twitter
		if(caseType == 20){
			//twitter post attempt
			string1 = "PLASE WAIT";
			string2 = "WHILE WE";
			string3 = "CONNECT";
		}
		if(caseType == 21){
			//twitter post success
			string1 = "HOORAY!";
			string2 = "A SUCCESS";
			string3 = "- TWITTERVERSE";
		}
		if(caseType == 22){
			//twitter post fail
			string1 = "SORRY";
			string2 = "CANT POST";
			string3 = "RIGHT NOW";
		}

		//instagram
		if(caseType == 30){
			//instagram post attempt
			string1 = "PLASE WAIT";
			string2 = "WHILE WE";
			string3 = "CONNECT";
		}
		if(caseType == 31){
			//instagram post success
			string1 = "HOORAY!";
			string2 = "A SUCCESS";
			string3 = "- THE COOLEST";
		}
		if(caseType == 32){
			//instagram post fail
			string1 = "SORRY";
			string2 = "CANT POST";
			string3 = "RIGHT NOW";
		}


		tm_line1.text = string1;
		tm_line2.text = string2;
		tm_line3.text = string3;

		if(tf_pu.localPosition.x != 0){
			tf_pu.localPosition = Vector3.zero;
			Vector3 hidePos = new Vector3(-15, 0, 0);
			Transform tf_pic = ERS_Mng.Instance.tf_sharePic;
			tf_pic.localPosition = hidePos;
			Invoke("open_homeBtn", 2);
		}

	}


	private void HandleOnFacebookPostResult(ISN_Result res){
		if(res.IsSucceeded){
			//success
			runPopUp(11);
		}
		else if (res.IsFailed){
			//fail
			runPopUp(12);
		}

	}
	private void HandleOnTwitterPostResult(ISN_Result res){
		if(res.IsSucceeded){
			//success
			runPopUp(21);
		}
		else if (res.IsFailed){
			//fail
			runPopUp(22);
		}

	}
	private void HandleOnInstagramPostResult(ISN_Result res){
		if(res.IsSucceeded){
			//success
			runPopUp(31);
		}
		else if (res.IsFailed){
			//fail
			runPopUp(32);
		}

	}


	void open_homeBtn(){
		tf_homeBtn.localScale = Vector3.one;
	}

	public void bp_home(){
		tf_homeBtn.localScale = Vector3.zero;
		tf_pu.localPosition = new Vector3(-15, 0, 0);
		//ERS_Mng.Instance.setERBS();
		ERS_Mng.Instance.tf_ERBs.localPosition = Vector3.zero;
	}





}
