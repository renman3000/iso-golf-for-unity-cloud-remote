﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ERB_Unit_FreeGiftSegment : MonoBehaviour {

	int count;
	public TextMesh[] tm_panel1;
	public Color[] c_panel;
	public Transform tf_gift;


	// Use this for initialization
	void Start () {
	
	}
	
	public void run(){
		print("FGS run()");
		InvokeRepeating("_run", 0, 1);
		tf_gift.DOScaleX(0.9f, 0.25f).SetLoops(-1, LoopType.Yoyo);
		tf_gift.DOScaleY(0.9f, 0.25f).SetDelay(0.125f).SetLoops(-1, LoopType.Yoyo); 
	}
	void _run(){
		if(count == 0){
			tm_panel1[0].color = c_panel[0];
			tm_panel1[1].color = c_panel[1];
			tm_panel1[2].color = c_panel[2];
		}
		if(count == 1){
			tm_panel1[0].color = c_panel[2];
			tm_panel1[1].color = c_panel[1];
			tm_panel1[2].color = c_panel[0];
		}
		if(count == 2){
			tm_panel1[0].color = c_panel[1];
			tm_panel1[1].color = c_panel[2];
			tm_panel1[2].color = c_panel[0];
		}

		count++;
		if(count == 3){
			count = 0;
		}
	}

	public void reset(){
		tf_gift.DOKill();
		tf_gift.localScale = Vector3.one;
		for(int i = 0; i < tm_panel1.Length; i++){
			tm_panel1[i].DOKill();
		}
	}
}
