﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;


public class Store_Mng : MonoBehaviour {

	public GameObject gm_unit;

	public List<Store_Ball_Unit> balls;
	public Transform tf_ballsDragger;
	public Transform tf_ballsDisplay;
	public TextMesh tm_ball;
	public TextMesh tm_ownedStatus;
	public Transform pu_confirmPurchase;

	public Transform tf_uiBtns;


	Vector2 v2_startPos;
	Vector2 v2_crntPos;
	Vector2 v2_lastPos;
	Vector2 v2_offset;

	float xLimit_ballsDragger; /// <summary>
	/// used to set dragger x limit...
	/// </summary>

	public ParticleSystem[] ps_purchase;


	public Transform tf_purchaseBtn;
	[HideInInspector]
	public Store_Ball_Unit activeUnit;
	GameObject gm_activeUnit_Ball;


	public Image sr_ownershipBtn;
	public Color c_owned;
	public Color c_purchase;

	public Light spotLight;



	// Use this for initialization
	void Start () {
		stopInput();
		Invoke("setBalls", 1);
		for(int i = 0; i < ps_purchase.Length; i++){
			ps_purchase[i].Emit(0);
		}
		c_owned = new Vector4(c_owned.r, c_owned.g, c_owned.b, 255);
		c_purchase = new Vector4(c_purchase.r, c_purchase.g, c_purchase.b, 255);
	}

	void setBalls(){
		for(int i = 0; i < balls.Count; i++){
			balls[i].setBall(i);
		}

		xLimit_ballsDragger = ((float)balls.Count - 1) * (-5);
		//checkForSnap();
		gm_unit.SetActive(false);
	}


	public void openStore(){
		gm_unit.SetActive(true);
		startInput();
	}

	void startInput(){
		tf_uiBtns.gameObject.SetActive(true);
	}
	void stopInput(){
		tf_uiBtns.gameObject.SetActive(false);
	}
	public void bp_pressed_ballsDragger(){
		//mark start pos
		v2_startPos = Input.mousePosition;
		v2_lastPos = Input.mousePosition;
		v2_crntPos = Input.mousePosition;
		v2_offset = new Vector2(v2_startPos.x, 0);

		print("PRESSD TO DRAG");
		//reset_allBalls();

	}


	public void bp_drag_ballsDragger(){

		//tf balls dragger being dragged.




		float dragFactor = 3;

		///
		v2_crntPos = Input.mousePosition;

		float delta_x = (v2_crntPos.x - v2_lastPos.x) * Time.deltaTime * dragFactor;

		float new_x = tf_ballsDragger.localPosition.x + delta_x;

		///LIMIT BREAKER
		if(new_x < xLimit_ballsDragger || new_x > 0){
			///drag at last balls unit....
			return;
		}



		tf_ballsDragger.localPosition = new Vector3(new_x, tf_ballsDragger.localPosition.y, tf_ballsDragger.localPosition.z);

		v2_lastPos = v2_crntPos;


		checkForSnap();

	}

	void checkForSnap(){
		///here we must get the store ball unit, which has been dragged into the zero x point.......
		for(int i = 0; i < balls.Count; i++){
			if(balls[i].transform.parent == tf_ballsDragger){
				if(balls[i].transform.position.x > -3f && balls[i].transform.position.x < 3f){
					if(balls[i].transform.localScale.x == 0.5f){
						balls_resetDisplay(balls[i]);
					}
				}
			}
		}
	}

	void balls_resetDisplay(Store_Ball_Unit unit){
		print("Found Ball To Snap!!! " + unit.transform.name);

		for(int i = 0; i < balls.Count; i++){
			if(balls[i].transform.parent == tf_ballsDisplay){
				//this unit is on display..remove... reset to dragger
				balls[i].set_toDragger();
				print("found ball to reset");
			}
		}
		///set new unit (last colected to display)
		unit.set_toDisplay();
		spotLight.DOIntensity(unit.intensity_spotlight, 0.25f);
		activeUnit = unit;
		set_purchaseBtn();
		if(activeUnit.isOwned)
		store_activeUnit_ball();
		else{
			sr_ownershipBtn.color = c_purchase;
		}
	}



	public void set_purchaseBtn(){
		if(activeUnit.isOwned){
			tm_ownedStatus.text = "OWNED";
		}
		else{
			tm_ownedStatus.text = "$0.99";
		}
	}
	public void bp_purchaseBall(){
		if(activeUnit.isOwned){
			print("∆∆ SHAKE CAM, BALL is already owned");
			store_activeUnit_ball();
			return;
		}
		else{
			print("bp purchase ball, run stans!!!!!! - for now assume success");	
			Invoke("purchaseSucceeded_ball", 1);
		}
	}

	public void purchaseSucceeded_ball(){
		print("BALL PURCHASE SUCEEDED");
		activeUnit.unlock();
		tm_ownedStatus.text = "OWNED";
		for(int i = 0; i < ps_purchase.Length; i++){
			ps_purchase[i].Emit(25);
		}
		store_activeUnit_ball();

	}
	public void store_activeUnit_ball(){
		gm_activeUnit_Ball = activeUnit.prefabbed_rep;
		sr_ownershipBtn.color = c_owned;
		print("1. STORE ACTIVE UNIT PREFAB " + activeUnit.prefabbed_rep.name);
	}
	void apply_newBall_toHomeMng(){
		GameObject newBall = Instantiate(gm_activeUnit_Ball, Vector3.zero, transform.rotation)as GameObject;
		Home_Mng.Instance.set_defaultBall(newBall);
	}

	void purchaseFailed(){
		print("Purchase failed");

	}





	public void bp_released_ballsDragger(){
		///ball dragger is released... we must get snap
		print("dragger released, snap to pos");
		float trgtPos = Mathf.Round(tf_ballsDragger.localPosition.x/5)*5;
		//trgtPos must be a unit of 5... so 0, 5, 10, 15.....


		tf_ballsDragger.DOLocalMoveX(trgtPos, 0.25f, false).SetEase(Ease.OutQuint);
	}




	public void bp_home(){
		print("STORE: bpHome");
		apply_newBall_toHomeMng();
		Home_Mng.Instance.bp_home();
		stopInput();
		gm_unit.SetActive(false);
	}


	void reset_allBalls(){
		for(int i = 0; i < balls.Count; i++){
			balls[i].set_toDragger();
		}
	}



}
