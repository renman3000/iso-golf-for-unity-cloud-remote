﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Number_Mng : MonoBehaviour {


	public static Number_Mng Instance;
	public GameObject val_0;
	public GameObject val_1;
	public GameObject val_2;
	public GameObject val_3;
	public GameObject val_4;
	public GameObject val_5;
	public GameObject val_6;
	public GameObject val_7;
	public GameObject val_8;
	public GameObject val_9;


	public Number_Delegate nd_strokeCount;
	public Number_Delegate nd_shotLimit;



	int shotsLeft;
	public Transform tf_bank;
	public Transform tf_bankHolder;

	void Awake(){
		Instance = this;
		//tf_bankHolder.localPosition = new Vector3(10, 0,0);
	}



	public void show_shots(int par){
		print("Ç˛Ç˛Ç˛showShots()");
		//nd_strokeCount.set_number(par);
		//nd_shotLimit.set_number(shotLimit);
	}


	public void holeSunk(){
		////nd_strokeCount.holeSunk();
	}




	public void pass_shotsLeft(){

		shotsLeft = Home_Mng.Instance.levelMng.holeMng.par;
		//for each shot left, we must animate the passing of 1 into the bank and lower the shotsLeft value.


		//open bank sybmol
		tf_bankHolder.DOLocalMove(Vector3.zero, 0.25f, false).SetEase(Ease.OutBounce).OnComplete(run_shotToBank);


	}

	void run_shotToBank(){
		//hide current nd strokeCount value, 
		if(shotsLeft > 0){
			move_toBank();
		}
		else{
			ERS_Mng.Instance.setERBS();
		}
	}
	void move_toBank(){
		float frq = 1/shotsLeft;
		Invoke("complete_shotToBank", frq);
	}

	void complete_shotToBank(){
		//animate the bank recieving the number.
		//Vector3 scale = new Vector3(0.75f, 0.75f, 1f);
		//tf_bank.DOScale(scale, 0.125f).SetEase(Ease.OutBounce).SetLoops(2, LoopType.Yoyo);

		shotsLeft--;

		Bank_Mng.Instance.add_shotToBank();

		if(shotsLeft == 0){
			//nd_strokeCount.set_number(0);
			Bank_Mng.Instance.shake();
			ERS_Mng.Instance.setERBS();
			return;
		}
		else{
			//we must run another shot to bank..... first, we update shots left via the nd_shotCount
			//nd_strokeCount.set_number(shotsLeft);
			////nd_strokeCount.shake();
			//invoke the next number to pass.
			Invoke("run_shotToBank", 0.125f);

		}
	}

	public void hide_bank(){
		print("??? hide bank");
		//tf_bankHolder.localPosition = new Vector3(10f, 0,0);
	}

	public void hide_shots(){
		//nd_strokeCount.hide_shots();
	}
	

}
