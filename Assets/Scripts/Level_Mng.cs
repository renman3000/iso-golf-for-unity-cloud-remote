﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Level_Mng : MonoBehaviour {


	public string name_world;
	public string name_level;
	public int shotLimit;


	public Player_Mng playerMng;
	public Ball_Mng ballMng;
	public Hole_Mng holeMng;
	public Grounds_Mng groundsMng;
	public Camera_Controller camCntrl;


	public TextMesh tm_shotLimit;
	public TextMesh tm_alert;
	public Transform tf_alert;
	int shotsTaken = 0;



	void Start () {
		print("LEVEL NAME " + transform.name);
		print("BALL MNG NAME " + ballMng.transform.name);
		ballMng.levelMng = this;
		groundsMng.levelMng = this;
		playerMng.levelMng = this;
		holeMng.levelMng = this;
		Home_Mng.Instance.newLevel_awake(this);
		transform.position = Vector3.zero;

		Coin_Mng.Instance.setCoins(groundsMng);


		//set_HUD();
	}




	public void turnOn_gameScene(){
		camCntrl.turnOn_gameScene();
		playerMng.startInput();
		holeMng.levelMng = this;
		holeMng.par = shotLimit;
		holeMng.show_shots();
		playerMng.initScene();
		Invoke("hide_alert", 5);
	}






	public void atDeath(){
		print("atDeath!");
		ballMng.atDeath();///anim
		playerMng.atDeath();//input
		ERS_Mng.Instance.atDeath();//sets bool
		holeMng.hide_shots();//
		holeMng.atDeath();//raises LOSE graphic
		//tm_alert.text = "YOU LOSE!!!";
		//tf_alert.DOScale(Vector3.one, 1);
	}




	public void gracePar(){
		///a coin was obtained on the grounds, as such, increase par...
		print("GRACE PAR");
		holeMng.par++;
		print("par = " + holeMng.par);
		holeMng.show_shots();
	}

}
