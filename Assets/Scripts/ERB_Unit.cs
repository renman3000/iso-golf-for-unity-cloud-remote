﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class ERB_Unit : MonoBehaviour {


	public GameObject[] cases;



	public TextMesh tm_shotCount;

	public Transform tf_auxText;//allow for floating position, due to number count 1, 11, 111, 1111, length


	public Transform tf_icon;


	public ERB_Coin_Mng erbCoinMng;


	public ERB_Unit_FreeGiftSegment fgs;

	public ParticleSystem ps_celebration;

	public SpriteRenderer rndr_bkg;
	public Color c_case0;
	public Color c_case1;



	//used in case 0-0
	int par; 
	int running_shotWon;
	int coinsWon;
	int coinsTo100;

	// Use this for initialization
	void Start () {

		//turn off cases
		for(int i = 0; i < cases.Length; i++){
			cases[i].SetActive(false);
		}


		c_case0 = new Vector4(c_case0.r, c_case0.g, c_case0.b, 255);
		c_case1 = new Vector4(c_case1.r, c_case1.g, c_case1.b, 255);
	}

	public void setUnit(int unit, int case_){
		//insure fresh texts (all == "";)


		if(unit == 0){
			if(case_ == 0){
				//show shots won
				setCase_00();
			}
			if(case_ == 1){
				setCase_01();
			}
		}

		if(unit == 1){
			if(case_ == 0){
				setCase_10();
			}
			if(case_ == 1){
				setCase_11();
			}
		}

		if(unit == 2){
			if(case_ == 0){
				setCase_20();
			}
			if(case_ == 1){
				setCase_21();
			}
		}

		if(unit == 3){
			if(case_ == 0){
				setCase_30();
			}
			if(case_ == 1){
				setCase_31();
			}
		}



	}





	/// <summary>
	/// shots won/gain 3 shots watch ad
	/// </summary>
	void setCase_00(){
		//shots won.....
		cases[0].SetActive(true);
		int strokeCount = Home_Mng.Instance.levelMng.holeMng.strokeCount;
		tm_shotCount.text = strokeCount.ToString();
		rndr_bkg.color = c_case0;
	}


	void setCase_01(){
		//watch ad to gain 3 free shots
		cases[1].SetActive(true);
		rndr_bkg.color = c_case1;
	}


	/// <summary>
	/// double coins watch ad/promo
	/// </summary>
	void setCase_10(){
		///double coins won, watch ad
		cases[0].SetActive(true);
		rndr_bkg.color = c_case0;
		//tf_icon.DOScaleX(0.75f, 0.25f).SetDelay(0.125f).SetLoops(-1, LoopType.Yoyo);
		//tf_icon.DOScaleY(0.75f, 0.25f).SetLoops(-1, LoopType.Yoyo);
	}
	void setCase_11(){
		//promo
		cases[1].SetActive(true);
		cases[0].SetActive(false);
		rndr_bkg.color = c_case1;
		///sub cases are website, random player, more game, rate us......
		print("!!!!??? HANDLE PROMO varient");

	}



	/// <summary>
	/// shots to go/prize available
	/// </summary>
	void setCase_20(){
		//shots to go...
		cases[0].SetActive(true);
		cases[1].SetActive(false);
		int shotsToGo = 100 - Coin_Mng.coins;
		tm_shotCount.text = shotsToGo.ToString();	
		int shots_length = shotsToGo.ToString().Length - 1;
		float xPos = (float)shots_length * 0.84f;
		tf_auxText.localPosition = new Vector3(xPos, 0, 0);
		rndr_bkg.color = c_case0;
	}
	void setCase_21(){
		cases[1].SetActive(true);
		cases[0].SetActive(false);
		rndr_bkg.color = c_case1;
	}



	/// <summary>
	/// running time until gift/gift available
	/// </summary>
	void setCase_30(){
		cases[0].SetActive(true);
		int m_length = tm_shotCount.text.Length - 1;
		float xPos = (float)m_length * 0.9f;
		tf_auxText.localPosition = new Vector3(xPos, 0, 0);
		rndr_bkg.color = c_case0;
	}
	void setCase_31(){
		cases[1].SetActive(true);
		rndr_bkg.color = c_case1;
	}










	//returns from collections
	public void setCase_doubleCoinsWon(){
		///double coins won....

		coinsWon = (Home_Mng.Instance.levelMng.holeMng.strokeCount);
		running_shotWon = coinsWon;
		//set frq
		float frq = 1.5f/(float)coinsWon;
		//double coins won
		coinsWon = coinsWon + coinsWon;

		//set invoke
		InvokeRepeating("anim_coinsWonDouble", frq, frq);



	}
	void anim_coinsWonDouble(){

		running_shotWon++;
		tm_shotCount.text = running_shotWon.ToString();
		Coin_Mng.coins++;
		Coin_Mng.Instance.tm_coinsCollectedHud.text = Coin_Mng.coins.ToString();

		if(Coin_Mng.coins < 100){
			int shotsToGo = 100 - Coin_Mng.coins;
			ERS_Mng.Instance.ERB_Units[2].tm_shotCount.text = shotsToGo.ToString();
		}


		if(Coin_Mng.coins == 100){
			ERS_Mng.Instance.ERB_Units[2].setCase_21();
		}




		if(running_shotWon == coinsWon){
			CancelInvoke("anim_coinsWonDouble");
		}







	}







	public void freeGiftReady(){
		cases[0].SetActive(false);
		cases[1].SetActive(true);
		fgs.run();
	}




	public void reset(){
		if(fgs){
			fgs.reset();
		}

		if(tf_icon){
			tf_icon.DOKill();
			tf_icon.localScale = Vector3.one;
		}

		for(int i = 0; i < cases.Length; i++){
			cases[i].SetActive(false);
		}
	}

}
