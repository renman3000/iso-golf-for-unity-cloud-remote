﻿using UnityEngine;
using System.Collections;
using DG.Tweening;




public class Number_Delegate : MonoBehaviour {

	public string type;
	Number_Mng numberMng;
	[HideInInspector]
	public GameObject gm_val;

	public bool isTweened;
	Vector3 resetPos;



	void Start(){
		numberMng = Number_Mng.Instance;

	}



	public void set_number(int value){

		if(gm_val){
			Destroy(gm_val);
		}


		print("set Number " + value);


		if(value == 0){
			gm_val = Instantiate(numberMng.val_0, Vector3.zero, Quaternion.identity) as GameObject;
		}
		if(value == 1){
			gm_val = Instantiate(numberMng.val_1, Vector3.zero, Quaternion.identity) as GameObject;
		}
		if(value == 2){
			gm_val = Instantiate(numberMng.val_2, Vector3.zero, Quaternion.identity) as GameObject;
		}
		if(value == 3){
			gm_val = Instantiate(numberMng.val_3, Vector3.zero, Quaternion.identity) as GameObject;
		}
		if(value == 4){
			gm_val = Instantiate(numberMng.val_4, Vector3.zero, Quaternion.identity) as GameObject;
		}
		if(value == 5){
			gm_val = Instantiate(numberMng.val_5, Vector3.zero, Quaternion.identity) as GameObject;
		}
		if(value == 6){
			gm_val = Instantiate(numberMng.val_6, Vector3.zero, Quaternion.identity) as GameObject;
		}
		if(value == 7){
			gm_val = Instantiate(numberMng.val_7, Vector3.zero, Quaternion.identity) as GameObject;
		}
		if(value == 8){
			gm_val = Instantiate(numberMng.val_8, Vector3.zero, Quaternion.identity) as GameObject;
		}
		if(value == 9){
			gm_val = Instantiate(numberMng.val_9, Vector3.zero, Quaternion.identity) as GameObject;
		}



		gm_val.transform.parent = transform;
		gm_val.transform.localPosition = Vector3.zero;
		gm_val.transform.rotation = transform.rotation;
		gm_val.transform.localScale = Vector3.one;

		if(isTweened){
			
		}

		//print("!!" + gm_val.transform.name);
	}



	public void hide_shots(){
		if(gm_val){
			Destroy(gm_val);
		}
	}

	public void holeSunk(){
		if(gm_val){
			print("HOLE SUNK  " );
			Vector3 scale_curnt = gm_val.transform.localScale;
			Vector3 scale_trgt = scale_curnt * 0.75f;

			transform.DOScale(scale_trgt, 0.125f).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutBounce);
		}
	}


	public void shake(){
		if(gm_val == null)
		return;

		Vector3 scale_curnt = gm_val.transform.localScale;
		Vector3 scale_trgt = scale_curnt * 0.75f;
		transform.DOScale(scale_trgt, 0.125f).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutBounce);
	}





}
