﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ERB_AchievmentMng : MonoBehaviour {


	public static ERB_AchievmentMng Instance;



	[HideInInspector]
	public List<ERB_AchievmentUnit> units;
	ERB_AchievmentUnit activeUnit;
	bool isActive;
	[HideInInspector]
	public bool is_achievmentMade;

	public TextMesh tm_ERB_Achievemnt;


	void Awake(){
		Instance = this;
	}



	public void run(){
		///here we have been told by the ers mng to run...
		if(isActive){
			return;		
		}
		///
		else if (!isActive){
			///find a random unit for a new acheivement.....
			int rdmInt = Random.Range(0, units.Count);
			units[rdmInt].run();		
			return;
		}
	}

	public void holeSunk(){
		//one hole sunk... 
		print("!!!!!! ERB AheivcementMng @ holeSunk");
		if(activeUnit == null)
		return;

		if(activeUnit.is_make3Holes){
			activeUnit.achievment_increment();
		}
	}

	public void achievement_made(){
		//here, acheivement was made.... as such, set bool 
		is_achievmentMade = true;
		//this bool will be chacked by the ers at setERBS....
	}






}
