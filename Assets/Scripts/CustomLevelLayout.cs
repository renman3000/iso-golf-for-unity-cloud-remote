﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CustomLevelLayout
{
    [System.Serializable]
    public struct rowData
    {
        public bool[] row;
    }

    public rowData[] rows = new rowData[20];
}