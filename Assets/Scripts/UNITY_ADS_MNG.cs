﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;


public class UNITY_ADS_MNG : MonoBehaviour {


	bool resultBool;
	public static UNITY_ADS_MNG instance;

	void Awake(){
		instance = this;
		DontDestroyOnLoad(transform.gameObject);
	}

	// Use this for initialization
	void Start () {
		Advertisement.Initialize ("66368");
	}

	public void watchAd_doubleCoins(){
		print ("watch ad double coins");
		if(Advertisement.isReady()){ 
			Advertisement.Show(null, new ShowOptions{
				resultCallback = result => {
					Debug.Log(result.ToString());
					switch(result)
					{
					case (ShowResult.Finished):
						print ("AD FINSISHED");
						ERS_Mng.Instance.adFinished_doubleCoins();
						break;
					case (ShowResult.Failed):
						print ("AD FAILED");
						ERS_Mng.Instance.adFailed_doubleCoins();
						break;
					case(ShowResult.Skipped):
						print ("AD SKIPPED");
						ERS_Mng.Instance.adSkipped_doubleCoins();
						break;
					}

				}
			});

		}
	}
	public void watchAd_extraLife(){
		print ("watch ad extraLife");
		if(Advertisement.isReady()){ 
			Advertisement.Show(null, new ShowOptions{
				resultCallback = result => {
					Debug.Log(result.ToString());
					switch(result)
					{
					case (ShowResult.Finished):
						print ("AD FINSISHED");
						ERS_Mng.Instance.adFinished_extraLife();
						break;
					case (ShowResult.Failed):
						print ("AD FAILED");
						ERS_Mng.Instance.adFailed_extraLife();
						break;
					case(ShowResult.Skipped):
						print ("AD SKIPPED");
						ERS_Mng.Instance.adSkipped_extraLife();
						break;
					}

				}
			});

		}
	}




	/*
	public void runAd_forLife(){
		print ("run ad for life");
		if(Advertisement.isReady()){ 
			Advertisement.Show(null, new ShowOptions{
				resultCallback = result => {
					Debug.Log(result.ToString());
					
					switch(result)
					{
					case (ShowResult.Finished):
						print ("AD FINSISHED");
						resultBool = true;
						ersMng.giveOneLife_watchVideo_cleared();
						break;
					case (ShowResult.Failed):
						print ("AD FAILED");
						resultBool = false;
						break;
					case(ShowResult.Skipped):
						print ("AD SKIPPED");
						resultBool = false;
						break;
					}
					
				}
			});
			
		}
	}*/

}
