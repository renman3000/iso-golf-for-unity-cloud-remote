﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class ERB_Coin_Unit : MonoBehaviour {

	public ERB_Coin_Mng thisMng;
	
	// Use this for initialization
	void Start () {
		reset();
	}


	public void run(){
		//called at erb, in scene, this gives an animation effect for each shot won.

		thisMng.units.Remove(this);

		transform.localPosition = Vector3.zero;

		transform.localScale = Vector3.one;

		Vector3 trgtPos = Coin_Mng.Instance.tm_coinsCollectedHud.transform.position;

		transform.DOMove(trgtPos, 0.35f, false).OnComplete(reset);
	}

	void reset(){
		transform.localScale = Vector3.zero;
		thisMng.units.Add(this);
	}

}
