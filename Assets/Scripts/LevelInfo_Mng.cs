﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelInfo_Mng : MonoBehaviour {


	Level_Mng levelMng;

	public TextMesh tm_worldName;
	public TextMesh tm_levelName;
	public TextMesh tm_shotLimit;

	int int_shotLimit;


	public RectTransform rt_btn;


	// Use this for initialization
	void Start () {
		rt_btn.sizeDelta = Vector2.zero;
	}


	public void set_levelInfo(Level_Mng levelMng_del){
		levelMng = levelMng_del;

		tm_worldName.text = levelMng.name_world;
		tm_levelName.text = levelMng.name_level;
		tm_shotLimit.text = levelMng.shotLimit.ToString();
		int_shotLimit = levelMng.shotLimit;

		allowInput();
	}


	public void allowInput(){
		print("allowInput()");
		rt_btn.sizeDelta = new Vector2(300, 300);
	}
	public void haltInput(){
		///at tap of the rt_btn
		rt_btn.sizeDelta = Vector2.zero;
	}


}
