﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Enemy_BigHopper : MonoBehaviour {


	

	[HideInInspector]
	public List<Grounds_Unit> grounds_units;
	int dir;





	public Transform tf_pointsHolder;
	public Transform[] tf_points;


	// Use this for initialization
	void Start () {
		Home_Mng.Instance.enemies.Add(gameObject);
		Invoke("run", 1);
	}
	
	public void run(){
		//set grounds units for reference
		grounds_units = Home_Mng.Instance.levelMng.groundsMng.units;	

		//now begin cycle of the mouse scurry....
		get_direction();


		//check_nextTarget(
	}

	public void reset(){

	}

	void get_direction(){
		dir = Random.Range(1,5);
		check_nextTarget(dir);
	}


	void check_nextTarget(int dir){

		Vector3 newTarget = transform.position;

		if(dir == 1){
			newTarget = new Vector3(newTarget.x, newTarget.y, newTarget.z + 1);
			transform.localEulerAngles = new Vector3(0,0,0);
		}
		if(dir == 2){
			newTarget = new Vector3(newTarget.x + 1, newTarget.y, newTarget.z);
			transform.localEulerAngles = new Vector3(0,270,0);
		}
		if(dir == 3){
			newTarget = new Vector3(newTarget.x, newTarget.y, newTarget.z - 1);
			transform.localEulerAngles = new Vector3(0,180,0);
		}
		if(dir == 4){
			newTarget = new Vector3(newTarget.x - 1, newTarget.y, newTarget.z);
			transform.localEulerAngles = new Vector3(0,90,0);
		}



		check_points(newTarget);
	

	}


	void check_points(Vector3 newTarget){

		tf_pointsHolder.position = newTarget;
		int valid_points = 0;
		for(int i = 0; i < tf_points.Length; i++){
			Ray ray = new Ray(tf_points[i].position, Vector3.down);
			RaycastHit hit; 
			if(Physics.Raycast(ray, out hit, 1)){ 
				if(hit.collider.CompareTag("GROUNDS_UNIT")){
					//has hit ground
					valid_points++;
				}
			}
		}

		tf_pointsHolder.localPosition = Vector3.zero;

		if(valid_points >= 3){
			moveTo_newTarget(newTarget);
		}
		else{
			get_direction();
		}
	}


	void moveTo_newTarget(Vector3 targetPos){
		///make path....
		Vector3[] path = new Vector3[2];
		if(dir == 1){
			path[0] = new Vector3(targetPos.x, targetPos.y + 1, targetPos.z - 0.5f);
		}
		if(dir == 2){
			path[0] = new Vector3(targetPos.x - 0.5f, targetPos.y + 1, targetPos.z);
		}
		if(dir == 3){
			path[0] = new Vector3(targetPos.x, targetPos.y + 1, targetPos.z + 0.5f);
		}
		if(dir == 4){
			path[0] = new Vector3(targetPos.x + 0.5f, targetPos.y + 1, targetPos.z);
		}
		path[1] = targetPos;



	

		transform.DOPath(path, 1f, PathType.CatmullRom, PathMode.Full3D, 10, null).SetEase(Ease.InOutQuint).OnComplete(shake).SetDelay(0.25f);

	}

	void shake(){
		//Home_Mng.Instance.levelMng.camCntrl.tf_camHolder
		//transform.DOShakePosition(0.1f, 0.1f, 0, 0, false).SetLoops(4, LoopType.Yoyo).OnComplete(get_direction);
		//tf_camFollow.DOShakePosition(0.1f, 0.05f, 0, 0, false).SetLoops(2, LoopType.Yoyo);
		transform.DOShakePosition(0.1f, new Vector3(1, 0, 1), 0, 0, false);
		get_direction();
	}






	public void Kill(){
		print("KILL");
		transform.DOKill();
	}






}
