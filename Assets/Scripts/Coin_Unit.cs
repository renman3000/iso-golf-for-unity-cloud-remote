﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Coin_Unit : MonoBehaviour {

	public Transform tf_P1;
	public Collider cldr;

	void Awake(){
		cldr.enabled = false;
	}

	// Use this for initialization
	void Start () {
		reset();
	}



	public void set(Vector3 pos){
		Coin_Mng.Instance.units.Remove(this);
		Coin_Mng.Instance.units_active.Add(this);
		transform.position = pos;
		transform.localScale = Vector3.one;
		transform.DORotate(new Vector3(0,180,0), 0.75f, RotateMode.Fast).SetLoops(-1).SetEase(Ease.Linear);
		cldr.enabled = true;
	}
	public void coin_pickedUp(){
		run_anim();
		cldr.enabled = false;
		Coin_Mng.Instance.Coin_pickedUp();
	}
	void run_anim(){
		transform.DOLocalMoveY(transform.position.y + 5, 0.5f, false).SetEase(Ease.OutSine);
		transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InElastic).OnComplete(reset).SetDelay(0.25f);
	}

	public void reset(){
		transform.localScale = Vector3.zero;
		//tf_P1.localPosition = Vector3.zero;
		transform.DOKill();
		transform.localEulerAngles = Vector3.zero;
		Coin_Mng.Instance.units.Add(this);
		Coin_Mng.Instance.units_active.Remove(this);
	}


}
