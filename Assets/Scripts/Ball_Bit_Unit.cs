﻿	using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Ball_Bit_Unit : MonoBehaviour {

	public Ball_Mng ballMng;
	public Vector3[] tn_nodes;

	// Use this for initialization
	void Start () {
		transform.localPosition = Vector3.zero;
		ballMng.ballBit_units.Add(this);
	}





	public void run(){
		///ball hit trap.. exploded...
		transform.DOLocalPath(tn_nodes, 0.25f, PathType.CatmullRom, PathMode.Full3D, 10, null).SetEase(Ease.OutBounce).OnComplete(sink);

	}

	public void sink(){
		float yPos = transform.localPosition.y - 12;
		transform.DOLocalMoveY(yPos, 2, false).SetEase(Ease.InQuint);
	}






	public void reset(){
		transform.localPosition = Vector3.zero;
	}



}
