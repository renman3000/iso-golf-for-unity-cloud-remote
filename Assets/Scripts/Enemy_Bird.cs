﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;



public class Enemy_Bird : MonoBehaviour {


	int dir;
	List<Grounds_Unit> groundUnits;


	// Use this for initialization
	void Start () {
		Home_Mng.Instance.enemies.Add(gameObject);
		Invoke("get_groundsUnits", 0.1f);
	}

	void get_groundsUnits(){
		groundUnits = Home_Mng.Instance.levelMng.groundsMng.units;
		Invoke("run", 1);
	}


	void run(){
		get_direction();
	}

	void get_direction(){
		////print("get direction");
		dir = Random.Range(1,5);
		////print("DIR = " + dir);
		get_newTarget();
	}


	void get_newTarget(){

		////print("get newTarget()");

		Vector3 newTarget = transform.position;

		if(dir == 1){
			newTarget = new Vector3(newTarget.x, newTarget.y, newTarget.z + 1);
			transform.localEulerAngles = new Vector3(0,0,0);
		}
		if(dir == 2){
			newTarget = new Vector3(newTarget.x + 1, newTarget.y, newTarget.z);
			transform.localEulerAngles = new Vector3(0,270,0);
		}
		if(dir == 3){
			newTarget = new Vector3(newTarget.x, newTarget.y, newTarget.z - 1);
			transform.localEulerAngles = new Vector3(0,180,0);
		}
		if(dir == 4){
			newTarget = new Vector3(newTarget.x - 1, newTarget.y, newTarget.z);
			transform.localEulerAngles = new Vector3(0,90,0);
		}


		check_newTarget(newTarget);

	}





	void check_newTarget(Vector3 newTarget){
		////print("check new target()");

		Vector2 targetMarker = new Vector2(newTarget.x, newTarget.z);

		for(int i = 0; i < groundUnits.Count; i++){
			Vector2 pos = new Vector2(groundUnits[i].transform.position.x, groundUnits[i].transform.position.z);
			if(pos == targetMarker){
				///then this position is safe, it is on the game board, proceed....
				moveTo_newTarget(newTarget);
				return;
			}
		}						

		get_direction();

										
	}


	void moveTo_newTarget(Vector3 newTarget){
		////print("move to new target");
		transform.DOMove(newTarget, 0.5f, false).OnComplete(check_swoop).SetEase(Ease.Linear);
	}

	void check_swoop(){


		//check pos, make sure not directly over head....
		Vector2 thisPos = new Vector2(transform.position.x, transform.position.z);
		Transform that = Home_Mng.Instance.levelMng.ballMng.tf_ball;
		Vector2 thatPos = new Vector2(that.position.x, that.position.z);
		if(thisPos == thatPos){
			get_direction();
			return;
		}


		//proceed....

		//
		int rdmInt = Random.Range(0,8);
		if(rdmInt == 0){
			swoop();
		}
		else{
			get_newTarget();
		}
		
	}


	void swoop(){
		//print("!!!!!!SWOOP!!!!! ");
		transform.DOKill();
		Vector3 thisPos = transform.position;
		Vector3 ballPos = Home_Mng.Instance.levelMng.ballMng.tf_ball.position;
		Vector3[] path = new Vector3[2];
		Vector3 targetPos = Vector3.zero;
		int rdmDir = Random.Range(1,3);
		float dist = 0;

		//print("thisPos " + thisPos);


		///
		if(rdmDir == 1){
			//swoop along x
			if(thisPos.x > ballPos.x){
				///
				dir = 4;
				float dist_x = Mathf.Abs(thisPos.x) - Mathf.Abs(ballPos.x);
				//since we have the absolute difference......
				///set end paths points...
				path[1] = new Vector3(transform.position.x - dist_x, thisPos.y, transform.position.z);
				path[0] = new Vector3(transform.position.x - (dist_x/2), ballPos.y, transform.position.z);
			}
			else if(thisPos.x < ballPos.x){
				///
				dir = 2;	
				float dist_x = Mathf.Abs(ballPos.x) - Mathf.Abs(transform.position.x);
				//since we have the absolute difference......
				///set end paths points...
				path[1] = new Vector3(transform.position.x + dist_x, thisPos.y, transform.position.z);
				path[0] = new Vector3(transform.position.x + (dist_x/2), ballPos.y, transform.position.z);
			}

		}
		else if (rdmDir == 2){
			//swoop along z
			if(thisPos.z > ballPos.z){
				///
				dir = 3;
				float dist_z = Mathf.Abs(thisPos.z) - Mathf.Abs(ballPos.z);
				//since we have the absolute difference......
				///set end paths points...
				path[1] = new Vector3(transform.position.x, thisPos.y, transform.position.z + dist_z);
				path[0] = new Vector3(transform.position.x, ballPos.y, transform.position.z + (dist_z/2));
			}
			else if(thisPos.z < ballPos.z){
				///
				dir = 1;	
				float dist_z = Mathf.Abs(ballPos.z) - Mathf.Abs(transform.position.z);
				//since we have the absolute difference......
				///set end paths points...
				path[1] = new Vector3(transform.position.x, thisPos.y, transform.position.z - dist_z);
				path[0] = new Vector3(transform.position.x, ballPos.y, transform.position.z - (dist_z/2));
			}

		}

		//print("!!dir = " + dir);


		if(path[1].y == 0){
			//print("!!!y at 0!!! " + path);
			path[1].y = thisPos.y;
		}
		///
		dist = Vector3.Distance(transform.position, path[1]);
		///////////
		set_swoop(path, dist);



	}


	void set_swoop(Vector3[] path, float dist){
		//print("_path[1] = " + path[1]);
		//print("_thisPos = " + transform.position);
		//print("distance = " + dist);
		float swoop_time = dist * 0.5f;
		transform.DOPath(path, swoop_time, PathType.CatmullRom, PathMode.Full3D, 10, null)
						.SetEase(Ease.InOutCirc).OnComplete(insure_onBoard);
	}


	void insure_onBoard(){
		//after swoop, may be off board, return to board. 
		for(int i = 0; i < groundUnits.Count; i++){
			Vector2 thisPos = new Vector2(transform.position.x, transform.position.z);
			Vector2 thatPos = new Vector2(groundUnits[i].transform.position.x, groundUnits[i].transform.position.z);

			if(thisPos == thatPos){
				//then we are on board... 
				get_direction();
				return;
			}
		}

		///since not on board, return to board.
		return_toBoard();

	}
	void return_toBoard(){
		//off board, after swoop
		//print("!!!!return to board!!!");
		Invoke("swoop", 5);


	}



	public void Kill(){
		print("KILL");
		transform.DOKill();
	}










}
