﻿using UnityEngine;
using System.Collections;

public class Cam_Mng : MonoBehaviour {


	public Camera cam_homeScene;
	public Camera cam_loadingScene;
	public Camera cam_levelInfo;
	public Camera cam_UI;
	public Camera cam_HUD;
	public Camera cam_store;
	public Camera cam_giftScene;
	public Camera cam_prizeScene;
	public Camera cam_title;

	// Use this for initialization
	void Start () {
	
	}


	public void turnOn_homeScene(){
		cam_homeScene.gameObject.SetActive(true);
		cam_UI.gameObject.SetActive(true);
		cam_loadingScene.gameObject.SetActive(false);
		cam_levelInfo.gameObject.SetActive(false);
		cam_store.gameObject.SetActive(false);
		cam_giftScene.gameObject.SetActive(false);
		cam_prizeScene.gameObject.SetActive(false);
		cam_HUD.gameObject.SetActive(false);
		cam_title.gameObject.SetActive(true);
		Title_Mng.instance.openHome();
	}
	public void turnOn_loadingScene(){
		cam_loadingScene.gameObject.SetActive(true);
		cam_UI.gameObject.SetActive(false);
		cam_homeScene.gameObject.SetActive(false);
		cam_levelInfo.gameObject.SetActive(false);
		cam_store.gameObject.SetActive(false);
		cam_giftScene.gameObject.SetActive(false);
		cam_prizeScene.gameObject.SetActive(false);
		cam_HUD.gameObject.SetActive(false);
		cam_title.gameObject.SetActive(false);
	}
	public void turnOn_levelInfo(){
		cam_levelInfo.gameObject.SetActive(true);
		cam_homeScene.gameObject.SetActive(false);
		cam_loadingScene.gameObject.SetActive(false);
		cam_UI.gameObject.SetActive(true);
		cam_store.gameObject.SetActive(false);
		cam_giftScene.gameObject.SetActive(false);
		cam_HUD.gameObject.SetActive(false);
		/////must get data for level, adjust texts... 
	}
	public void turnOn_gameScene(){
		//sent from level info mng, 
		cam_loadingScene.gameObject.SetActive(false);
		cam_homeScene.gameObject.SetActive(false);
		cam_levelInfo.gameObject.SetActive(false);
		cam_UI.gameObject.SetActive(true);
		cam_store.gameObject.SetActive(false);
		cam_giftScene.gameObject.SetActive(false);
		cam_prizeScene.gameObject.SetActive(false);
		cam_HUD.gameObject.SetActive(true);
		cam_title.gameObject.SetActive(false);
		Title_Mng.instance.openGame();//shuts off all (for now)
	}

	public void turnOn_storeScene(){
		cam_store.gameObject.SetActive(true);
		cam_homeScene.gameObject.SetActive(false);
		cam_UI.gameObject.SetActive(true);
		cam_loadingScene.gameObject.SetActive(false);
		cam_levelInfo.gameObject.SetActive(false);
		cam_giftScene.gameObject.SetActive(false);
		cam_HUD.gameObject.SetActive(false);
		cam_title.gameObject.SetActive(true);
		Title_Mng.instance.openStore();
	}
	public void turnOn_giftScene(){
		cam_giftScene.gameObject.SetActive(true);
		cam_homeScene.gameObject.SetActive(false);
		cam_UI.gameObject.SetActive(true);
		cam_loadingScene.gameObject.SetActive(false);
		cam_levelInfo.gameObject.SetActive(false);
		cam_store.gameObject.SetActive(false);
		cam_HUD.gameObject.SetActive(false);
		cam_title.gameObject.SetActive(true);
		Title_Mng.instance.openGift();
	}
	public void turnOn_prizeScene(){
		cam_prizeScene.gameObject.SetActive(true);
		cam_giftScene.gameObject.SetActive(false);
		cam_title.gameObject.SetActive(true);
		Title_Mng.instance.openPrize();
	}

	//used at screen grab.
	public void turnOn_titleCam(){
		cam_title.gameObject.SetActive(true);
	}
	public void turnOff_titleCam(){
		cam_title.gameObject.SetActive(false);
	}



}
