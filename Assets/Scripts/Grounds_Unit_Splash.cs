﻿using UnityEngine;
using System.Collections;

public class Grounds_Unit_Splash : MonoBehaviour {

	public ParticleSystem ps_;
	
	// Use this for initialization
	void Start () {
		ps_.Stop();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Jump")){
			ps_.Emit(12);
		}
		if(Input.GetButtonUp("Jump")){
			ps_.Stop();	
		}
	}
}
