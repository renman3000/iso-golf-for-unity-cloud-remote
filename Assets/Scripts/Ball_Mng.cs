﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;


public class Ball_Mng : MonoBehaviour {

	[HideInInspector]
	public Level_Mng levelMng;

	[HideInInspector]
	public int trajectory;


	public Transform tf_ball;
	public Transform tf_rndr;

	public Renderer rndr;

	public Transform tf_ballRtn;
	float rtn_time;
	float rtn_amount;

	public GameObject[] tf_cldrBalls;/// use array to reset (set all local scale to 0;
	public GameObject gm_cldrBalls_11;
	public GameObject gm_cldrBalls_12;
	public GameObject gm_cldrBalls_21;
	public GameObject gm_cldrBalls_22;
	public GameObject gm_cldrBalls_31;
	public GameObject gm_cldrBalls_32;
	public GameObject gm_cldrBalls_41;
	public GameObject gm_cldrBalls_42;



	Vector3 endPos;
	float height_full;
	Vector3 lastPos;
	Vector3 startPos;
	int yDirection;//1 = up, 2 = down

	bool isAirBall;


	Grounds_Unit trgtUnit;




	Vector3 v3_crntBallPos;
	Vector3 v3_lastBallPos;
	Vector3 v3_deltaBallPos;





	Vector3 v3_newGridPos_1;
	Vector3 v3_newGridPos_2;





	public List<Ball_Trailer_Unit> ballTrailer_units; 



	int int_bounceDirection; // allows for a bounce up, bounce down condition. 



	Transform tf_hitUnit_highlight;



	[HideInInspector]
	public List<Ball_Bit_Unit> ballBit_units;






	public BallSplash_Mng ballSplashMng;









	void Awake(){
	}



	// Use this for initialization
	void Start () {
		reset_cldrBalls();
		////print("RESET CLLDR BALLS");
		check_ifCustomBall();
	}

	public void check_ifCustomBall(){
		if(Home_Mng.Instance.tf_defaultBall_rndr != null){

			if(Home_Mng.Instance.tf_defaultBall_rndr == tf_rndr){
				return;
			}

			//then we must replace the currnt ball rndr with the home mng, default rdnr
			Destroy(tf_rndr.gameObject);

			tf_rndr = Home_Mng.Instance.tf_defaultBall_rndr;
			tf_rndr.parent = tf_ball;
			tf_rndr.localPosition = Vector3.zero;
			tf_rndr.localScale = Vector3.one;
			tf_rndr.parent = tf_ballRtn;
			tf_rndr.localEulerAngles = new Vector3(0,270,0);


			foreach(Transform child in tf_rndr){
				child.gameObject.layer = LayerMask.NameToLayer("Default");
				rndr = child.gameObject.GetComponent<Renderer>();
			}

		}
	}



	void reset_cldrBalls(){
		////print("- RESET CLDR BALLS - " + Time.time);
		for(int i = 0; i < tf_cldrBalls.Length; i++){
			tf_cldrBalls[i].SetActive(false);
		}
	}


	public void set_ballAtStart(Vector3 startPos_del){
		//print("set ball at start () " + startPos);
		startPos = startPos_del;
		lastPos = startPos;
		tf_ball.position = startPos;
		levelMng.playerMng.hideClub();//sets pos to ball
		levelMng.camCntrl.setStart(tf_ball);

	}





	public void hitBall(Vector3[] tn_nodes, float f_hangTime, bool isAirBall_del, Grounds_Unit trgtUnit_del, float distBallHit_del, int trj_del){
		//print("--------hitBall ------------" + Time.time);



		///
		trajectory = trj_del;


		///
		v3_deltaBallPos = Vector3.zero;


		///
		reset_cldrBalls();



		///store pos as last pos
		lastPos = tf_ball.position;
		//print("-- LAST POS : " + lastPos);


		//
		endPos = tn_nodes[tn_nodes.Length - 1];


		///all componenets have been gathered to hit ball......
		tf_ball.DOPath(tn_nodes, f_hangTime, PathType.CatmullRom, PathMode.Full3D, 10, null).OnComplete(init_shotComplete).SetEase(Ease.InQuart).OnUpdate(updateTween); 

		///
		apply_ballRotation(f_hangTime);

	
		levelMng.camCntrl.followBall(endPos, f_hangTime);
	


		////
		//InvokeRepeating("release_one_ballTrailer", 0, 0.15f);



		////////////////////////
		isAirBall = isAirBall_del;


		//////
		trgtUnit = trgtUnit_del;


		levelMng.holeMng.strokeTaken();


	}



	void apply_ballRotation(float f_hangTime){

		///now that ball hit, we must give it some life in the animation... 
		///.... tilting it slighty forward on ascent and back on decent (*all rtn local, to trajectory)

		rtn_time = f_hangTime/2;

		rtn_amount = rtn_time * 20;

		//print("rtn time " + rtn_time);
		//x axis + 15 (fwd)
		lean_fwd();
	
	
	}

	void lean_fwd(){
		if(trajectory == 1){
			//axis rtn is x, +15 degrees
			Vector3 vr_rtn = new Vector3(rtn_amount, 0, 0);
			tf_ballRtn.DOLocalRotate(vr_rtn, rtn_time, 0).SetEase(Ease.InSine).OnComplete(lean_back);
		}
		if(trajectory == 3){
			//axis rtn is x, +15 degrees
			Vector3 vr_rtn = new Vector3(-rtn_amount, 0, 0);
			tf_ballRtn.DOLocalRotate(vr_rtn, rtn_time, 0).SetEase(Ease.InSine).OnComplete(lean_back);
		}
		if(trajectory == 4){
			//axis rtn is z, +15 degrees
			Vector3 vr_rtn = new Vector3(0, 0, rtn_amount);
			tf_ballRtn.DOLocalRotate(vr_rtn, rtn_time, 0).SetEase(Ease.InSine).OnComplete(lean_back);
		}
		if(trajectory == 2){
			//axis rtn is z, +15 degrees
			Vector3 vr_rtn = new Vector3(0, 0, -rtn_amount);
			tf_ballRtn.DOLocalRotate(vr_rtn, rtn_time, 0).SetEase(Ease.InSine).OnComplete(lean_back);
		}
	}
	void lean_back(){
		if(trajectory == 1){
			//axis rtn is x, -15 degrees
			Vector3 vr_rtn = new Vector3(-rtn_amount, 0, 0);
			tf_ballRtn.DOLocalRotate(vr_rtn, rtn_time, 0).SetEase(Ease.OutSine);
		}
		if(trajectory == 3){
			//axis rtn is x, +15 degrees
			Vector3 vr_rtn = new Vector3(rtn_amount, 0, 0);
			tf_ballRtn.DOLocalRotate(vr_rtn, rtn_time, 0).SetEase(Ease.OutSine);
		}
		if(trajectory == 4){
			//axis rtn is x, +15 degrees
			Vector3 vr_rtn = new Vector3(0, 0, -rtn_amount);
			tf_ballRtn.DOLocalRotate(vr_rtn, rtn_time, 0).SetEase(Ease.OutSine);
		}
		if(trajectory == 2){
			//axis rtn is x, +15 degrees
			Vector3 vr_rtn = new Vector3(0, 0, rtn_amount);
			tf_ballRtn.DOLocalRotate(vr_rtn, rtn_time, 0).SetEase(Ease.OutSine);
		}
	}






	void init_shotComplete(){
		print("≈≈≈init shot complete");

		//orginal shot, complete. check if landed on unit, if yes, check unit type. if no, check if this can fall toi unit, or airball
		//get the status of land. are we safe, is there water, or nothing beneath us?
		for(int i = 0; i < levelMng.groundsMng.units.Count; i++){
			if(levelMng.groundsMng.units[i].nodeTarget.position == tf_ball.position){
				///we landed on an ground unit.......
				///we must check if unit isHole, isWater, .......
				print("have found unit target node pos that matches bal pos, ping unit");
				levelMng.groundsMng.units[i].gameObject.SendMessage("ping_groundUnit", this);
				return;
			}
		}

		//if here, no unit was landed on..... so we fall 
		calculate_fall();

	}


	void calculate_fall(){
		//ball has landed, but on no unit....
		///as such we must see if this will fall onto another unit, on sub level or airball.
		print("not unit found to match ball pos at end pos, calculate fall");
		reset_cldrBalls();
		Ray ray = new Ray(tf_ball.position, Vector3.down);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit, 5)){ 
			if(hit.collider.CompareTag("GROUNDS_UNIT")){
				fallTo_unit(hit.collider.gameObject);
				return;
			}
		}


		//since no unit was found, run airball..
		run_airBall();

	}
	void run_airBall(){

		//fall 5 units and scale to zero;

		float yPos = tf_ball.position.y - 12;
		tf_ball.DOMoveY(yPos, 0.75f, false).SetEase(Ease.OutSine).OnComplete(resetBall_toLastPos);



	}

	void fallTo_unit(GameObject gm_groundUnit){
		print("fal t0 unit");
		float yPos = gm_groundUnit.transform.position.y + 1;
		float yDiff = (Mathf.Abs(tf_ball.position.y) - Mathf.Abs(yPos))/20;
		tf_ball.DOMoveY(yPos, yDiff, false).SetEase(Ease.Linear).OnComplete(init_shotComplete);
	}




	void release_one_ballTrailer(){
		//ballTrailer_units[0].runUnit();
	}







	public void updateTween(){
		v3_crntBallPos = tf_ball.position;
		v3_deltaBallPos = new Vector3(v3_crntBallPos.x - v3_lastBallPos.x, v3_crntBallPos.y - v3_lastBallPos.y, v3_crntBallPos.z - v3_lastBallPos.z);
		v3_lastBallPos = v3_crntBallPos;
		set_ballColliders_pos();
	}







	int yDelta_last;
	int yDelta_crnt;

	void set_ballColliders_pos(){

		///use for change in y direction, change in cldr balls active (hit unit detection)
		if(v3_deltaBallPos.y < 0){
			yDelta_crnt = -1;
		}
		else{
			yDelta_crnt = 1;
		}


		 
		if(trajectory == 1){
			if(v3_deltaBallPos.y >= 0){
				//place collider at fwd up.
				gm_cldrBalls_11.SetActive(true);
			}
			if(v3_deltaBallPos.y < 0){
				//place collider at fwd down.
				gm_cldrBalls_12.SetActive(true);
			}
		}
		if(trajectory == 3){
			if(v3_deltaBallPos.y >= 0){
				//place collider at back up.
				gm_cldrBalls_31.SetActive(true);
			}
			if(v3_deltaBallPos.y < 0){
				//place collider at back down.
				gm_cldrBalls_32.SetActive(true);
			}
		}


		if(trajectory == 2){
			if(v3_deltaBallPos.y >= 0){
				//place collider at back up.
				gm_cldrBalls_21.SetActive(true);
			}
			if(v3_deltaBallPos.y < 0){
				//place collider at back down.
				gm_cldrBalls_22.SetActive(true);
			}
		}

		if(trajectory == 4){
			if(v3_deltaBallPos.y >= 0){
				//place collider at back up.
				gm_cldrBalls_41.SetActive(true);
			}
			if(v3_deltaBallPos.y < 0){
				//place collider at back down.
				gm_cldrBalls_42.SetActive(true);
			}
		}


		////check for change in y direction
		if(yDelta_crnt != yDelta_last){ 
			reset_cldrBalls();
		}
		else{
			///do nothing
		}
		yDelta_last = yDelta_crnt;



	}





	public void return_groundUnit(Grounds_Unit unit){



		///we have laneded on a unit.. check if unit is a hazard, hole or safe. 
		trgtUnit = unit;
		if(trgtUnit.isHole){
			ballSunk();
			trgtUnit = null;
			return;
		}



		if(trgtUnit.isWater){
			print("???? create splashwater()");
			runSplash(1);
			sinkBallAnim();
			//first add stroke
			levelMng.holeMng.strokeTaken();
			return;
	
		}
		if(trgtUnit.isSand){
			print("???? create splashsand()");
			runSplash(2);
			Player_Mng.powerFactor = 0.5f;
			resetBall();
			return;
		}
		if(trgtUnit.isDirt){
			print("???? create splashsand()");
			runSplash(3);
			Player_Mng.powerFactor = 0.75f;
			resetBall();
			return;
		}
		if(trgtUnit.isRoseBed){
			print("???? create splashsand()");
			runSplash(4);
			Player_Mng.powerFactor = 0.85f;
			resetBall();
			return;
		}
		else{
			Player_Mng.powerFactor = 1;
			resetBall();
			return;
		}






	}





	void killTweens(){
		tf_ball.DOKill();
		levelMng.camCntrl.cancelFollow();
	}


	public void ballHit_unforeseenCollider_VERT(Transform tf_hitUnit, Transform tf_ballCollider){
		///ball cldr has hit a unit, on the vertical plane
		//print("BALL HIT UNIT ON VERT");
		reset_cldrBalls();


		///
		if(v3_deltaBallPos.y < 0){
			///ball is moving down.....
			yDirection = -1;
		}
		else{
			///ball is moving up.....
			yDirection = 1;
		}


		///we must ask if hit unit node is pos of end... 
		Vector3 hitUnit_nodePos = new Vector3(tf_hitUnit.position.x, tf_hitUnit.position.y + 1, tf_hitUnit.position.z);

		///check if hit up or down.... if > 0, is up... if up, and in same pos as end... rn into the bottom of ddestined unit.. therefore invalid. 
		float f_ballsClldr_vs_ball_onY = tf_ballCollider.position.y - tf_ball.position.y; 

		///compare
		if(endPos == hitUnit_nodePos || lastPos == hitUnit_nodePos && f_ballsClldr_vs_ball_onY < 0){
			///we can assume the hit unit is safe to land on, with no change in tweens

			//as such, do nothing
			print(" -- LANDING SAFELY-- ");
			return;
		}
		else{
			//we have hit a unit, that is not the destined position.....

			///therefore we must calculate the rebound......
			print(" -- ADJUST LANDING-- ");

			//first, kill tweens
			killTweens();
			tf_hitUnit_highlight = tf_hitUnit;
			//hightlight_unforeseenHitUnit();


			if(trajectory == 1){
				if(yDirection > 0){
					//ball is moving up along postive z at hit....
					print("11");
					reboundBall_downFwd(tf_hitUnit.position);
				}
				if(yDirection < 0){
					//ball is moving down along postive z at hit....
					print("10");
					reboundBall_upFwd(tf_hitUnit.position);
				}
			}
			if(trajectory == 3){
				if(yDirection > 0){
					//ball is moving up along negative z at hit....
					print("31");
					reboundBall_downBack(tf_hitUnit.position);
				}
				if(yDirection < 0){
					//ball is moving down along negative z at hit....
					print("30");
					reboundBall_upBack(tf_hitUnit.position);
				}
			}
			if(trajectory == 2){
				if(yDirection > 0){
					//ball is moving up along postive x at hit....
					print("21");
					reboundBall_downLeft(tf_hitUnit.position);
				}
				if(yDirection < 0){
					//ball is moving down along positive x at hit....
					print("20");
					reboundBall_upRight(tf_hitUnit.position);
				}
			}
			if(trajectory == 4){
				if(yDirection > 0){
					//ball is moving up along negative x at hit....
					print("41");
					reboundBall_downRight(tf_hitUnit.position);
				}
				if(yDirection < 0){
					//ball is moving down along negative x at hit....
					print("40");
					reboundBall_upLeft(tf_hitUnit.position);
				}
			}



		}

	}
	public void ballHit_unforeseenCollider_HORZ(Transform tf_hitUnit){
		///ball cldr has hit a unit, on the vertical plane
		print("BALL HIT UNIT ON HORZ");
		reset_cldrBalls();

		///
		if(v3_deltaBallPos.y < 0){
			///ball is moving down.....
			yDirection = -1;
		}
		else{
			///ball is moving up.....
			yDirection = 1;
		}


		///we must ask if hit unit node is pos of end... 
		Vector3 hitUnit_nodePos = new Vector3(tf_hitUnit.position.x, tf_hitUnit.position.y + 1, tf_hitUnit.position.z);

		///compare
		if(endPos == hitUnit_nodePos){
			///we can assume the hit unit is safe to land on, with no change in tweens

			//as such, do nothing
			print("-- LANDING SAFELY--");
			return;
		}
		else{
			//we have hit a unit, that is not the destined position.....

			///therefore we must calculate the rebound......
			print(" -- ADJUST LANDING -- ");

			//first, kill tweens
			killTweens();
			tf_hitUnit_highlight = tf_hitUnit;
			//hightlight_unforeseenHitUnit();
			print("BALL POS AT HIT " + tf_ball.position);


			if(trajectory == 1){
				if(yDirection > 0){
					//ball is moving up along postive z at hit....
					reboundBall_downBack(tf_hitUnit.position);
				}
				if(yDirection < 0){
					//ball is moving down along postive z at hit....
					reboundBall_downBack(tf_hitUnit.position);
				}
			}
			if(trajectory == 3){
				if(yDirection > 0){
					//ball is moving up along negative z at hit....
					reboundBall_downFwd(tf_hitUnit.position);
				}
				if(yDirection < 0){
					//ball is moving down along negative z at hit....
					reboundBall_downFwd(tf_hitUnit.position);
				}
			}
		}
	}


	void hightlight_unforeseenHitUnit(){
		

		Vector3 moveToVector = Vector3.zero;
		print("????**** HIGHTLIGHT UNFORESEEN UNIT : deltaXYZ = " + v3_deltaBallPos);

		/*
		moveToVector.x = tf_hitUnit_highlight.position.x + (v3_deltaBallPos.x * 0.5f);
		moveToVector.y = tf_hitUnit_highlight.position.y + (v3_deltaBallPos.y * 0.5f);
		moveToVector.z = tf_hitUnit_highlight.position.z + (v3_deltaBallPos.z * 0.5f);
	


		tf_hitUnit_highlight.DOMove(moveToVector, 0.125f, false).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutBounce);
		*/
	}



	void reboundBall_downBack(Vector3 v3_hitPos){
		print("RBBI: dB ");


		/// ball needs to rebound down and fwd... 
		//we need to know the velocity of the ball and check if any units to fall onto...

		print("BALL POS " + tf_ball.position);
		print("HIT POS " + v3_hitPos);


		///get the first re-grid position.....
		v3_newGridPos_1.x = Mathf.RoundToInt(tf_ball.position.x);
		v3_newGridPos_1.y = Mathf.RoundToInt(tf_ball.position.y);
		v3_newGridPos_1.z = Mathf.RoundToInt(tf_ball.position.z);

		print(" GRID POS 1 " + v3_newGridPos_1);

		tf_ball.position = v3_newGridPos_1;
		return;



		//given the delta z we can gauge the velocity
		//print(" - delta z = " + v3_deltaBallPos.z);


		//now that we have the new grid position, we 






		//we need to get the new grid pos 2. (this pos is the locked position we will land on (if unit found (else, air ball, and reset.).....



		//print("CHECK FOR UNIT TO FALL TO: DB ");
		/// given the new locked pos (v3 new grid pos 1), we will scan one unit down. one unit fowrd.
		for(int i = 0; i < 100; i++){
			//scan down a ray cast from new locked pos z - i
			float zPos = (v3_newGridPos_1.z - ((float)i)/2);
			Vector3 pos = new Vector3(v3_newGridPos_1.x, v3_newGridPos_1.y, zPos);
			RaycastHit hit; 
			Ray ray = new Ray(pos, Vector3.down);
			float rayDist = 1 + (float)i;
			if(Physics.Raycast(ray, out hit, rayDist)){
				///if here, we have gathered a new unit to land on... that is in the falling trajectory..
				//print("FOUND FALLING UNIT");
				Grounds_Unit unit = hit.collider.gameObject.GetComponent<Grounds_Unit>();
				found_fallToUnit(unit);
				return;
			}
		}







		//if here, we have not found an object,,, thus, we can send ball on an air ball course
		//print("INIT AIRBALL");
		isAirBall = true;
		v3_newGridPos_2 = new Vector3(v3_newGridPos_1.x, v3_newGridPos_2.y - ((Mathf.Abs(v3_deltaBallPos.y) * 5)), v3_newGridPos_1.z - (Mathf.Abs(v3_deltaBallPos.z) * 5));

		//print(" GRID POS 2 " + v3_newGridPos_2);


		Vector3[] tn_nodes = new Vector3[2];
		tn_nodes[0] = tf_ball.position;
		tn_nodes[1] = v3_newGridPos_2;
		endPos = tn_nodes[1];
		//print("endPos " + endPos);




		float f_hangTime = (Vector2.Distance(tn_nodes[0], tn_nodes[1]) * 0.1f);
		//print("hang time " + f_hangTime);
		///all componenets have been gathered to hit ball......
		tf_ball.DOPath(tn_nodes, f_hangTime, PathType.CatmullRom, PathMode.Full3D, 10, null).OnComplete(resetBall).SetEase(Ease.OutQuart); 


	}
	void reboundBall_downFwd(Vector3 v3_hitPos){
		print("--------RBBI: dF ");


		/// ball needs to rebound down and fwd... 
		//we need to know the velocity of the ball and check if any units to fall onto...

		//print("BALL POS " + tf_ball.position);
		print("HIT POS " + v3_hitPos);


		///get the first re-grid position.....
		v3_newGridPos_1.x = Mathf.RoundToInt(tf_ball.position.x);
		v3_newGridPos_1.y = Mathf.RoundToInt(tf_ball.position.y);
		v3_newGridPos_1.z = Mathf.RoundToInt(tf_ball.position.z);

		print(" GRID POS 1 " + v3_newGridPos_1);

		//given the delta z we can gauge the velocity
		//print(" - delta z = " + v3_deltaBallPos.z);

		tf_ball.position = v3_newGridPos_1;
		return;


		//we need to get the new grid pos 2. (this pos is the locked position we will land on (if unit found (else, air ball, and reset.).....


	
		/// given the new locked pos (v3 new grid pos 1), we will scan one unit down. one unit fowrd.
		for(int i = 0; i < 100; i++){
			//scan down a ray cast from new locked pos z + i
			float zPos = (v3_newGridPos_1.z + ((float)i)/2);
			Vector3 pos = new Vector3(v3_newGridPos_1.x, v3_newGridPos_1.y, zPos);
			RaycastHit hit; 
			Ray ray = new Ray(pos, Vector3.down);
			float rayDist = 1 + (float)i;
			if(Physics.Raycast(ray, out hit, rayDist)){
				///if here, we have gathered a new unit to land on... that is in the falling trajectory..
				print("FOUND FALLING UNIT");
				Grounds_Unit unit = hit.collider.gameObject.GetComponent<Grounds_Unit>();
				found_fallToUnit(unit);
				return;
			}
		}







		//if here, we have not found an object,,, thus, we can send ball on an air ball course
		//print("INIT AIRBALL");
		isAirBall = true;
		v3_newGridPos_2 = new Vector3(v3_newGridPos_1.x, v3_newGridPos_2.y - ((Mathf.Abs(v3_deltaBallPos.y) * 5)), v3_newGridPos_1.z + (Mathf.Abs(v3_deltaBallPos.z) * 5));

		print(" GRID POS 2 " + v3_newGridPos_2);


		Vector3[] tn_nodes = new Vector3[2];
		tn_nodes[0] = tf_ball.position;
		tn_nodes[1] = v3_newGridPos_2;
		endPos = tn_nodes[1];
		//print("endPos " + endPos);




		float f_hangTime = (Vector2.Distance(tn_nodes[0], tn_nodes[1]) * 0.1f);
		//print("hang time " + f_hangTime);
		///all componenets have been gathered to hit ball......
		tf_ball.DOPath(tn_nodes, f_hangTime, PathType.CatmullRom, PathMode.Full3D, 10, null).OnComplete(resetBall).SetEase(Ease.OutQuart); 


	}


	void reboundBall_downLeft(Vector3 v3_hitPos){
		print("RBBI: dL ");


		/// ball needs to rebound down and fwd... 
		//we need to know the velocity of the ball and check if any units to fall onto...

		print("BALL POS " + tf_ball.position);
		print("HIT POS " + v3_hitPos);


		///get the first re-grid position.....
		v3_newGridPos_1.x = Mathf.RoundToInt(tf_ball.position.x);
		v3_newGridPos_1.y = Mathf.RoundToInt(tf_ball.position.y);
		v3_newGridPos_1.z = Mathf.RoundToInt(tf_ball.position.z);

		print(" GRID POS 1 " + v3_newGridPos_1);

		tf_ball.position = v3_newGridPos_1;
		return;



		//given the delta z we can gauge the velocity
		//print(" - delta z = " + v3_deltaBallPos.z);


		//now that we have the new grid position, we 






		//we need to get the new grid pos 2. (this pos is the locked position we will land on (if unit found (else, air ball, and reset.).....



		//print("CHECK FOR UNIT TO FALL TO: DB ");
		/// given the new locked pos (v3 new grid pos 1), we will scan one unit down. one unit fowrd.
		for(int i = 0; i < 100; i++){
			//scan down a ray cast from new locked pos z - i
			float zPos = (v3_newGridPos_1.z - ((float)i)/2);
			Vector3 pos = new Vector3(v3_newGridPos_1.x, v3_newGridPos_1.y, zPos);
			RaycastHit hit; 
			Ray ray = new Ray(pos, Vector3.down);
			float rayDist = 1 + (float)i;
			if(Physics.Raycast(ray, out hit, rayDist)){
				///if here, we have gathered a new unit to land on... that is in the falling trajectory..
				//print("FOUND FALLING UNIT");
				Grounds_Unit unit = hit.collider.gameObject.GetComponent<Grounds_Unit>();
				found_fallToUnit(unit);
				return;
			}
		}







		//if here, we have not found an object,,, thus, we can send ball on an air ball course
		//print("INIT AIRBALL");
		isAirBall = true;
		v3_newGridPos_2 = new Vector3(v3_newGridPos_1.x, v3_newGridPos_2.y - ((Mathf.Abs(v3_deltaBallPos.y) * 5)), v3_newGridPos_1.z - (Mathf.Abs(v3_deltaBallPos.z) * 5));

		//print(" GRID POS 2 " + v3_newGridPos_2);


		Vector3[] tn_nodes = new Vector3[2];
		tn_nodes[0] = tf_ball.position;
		tn_nodes[1] = v3_newGridPos_2;
		endPos = tn_nodes[1];
		//print("endPos " + endPos);




		float f_hangTime = (Vector2.Distance(tn_nodes[0], tn_nodes[1]) * 0.1f);
		//print("hang time " + f_hangTime);
		///all componenets have been gathered to hit ball......
		tf_ball.DOPath(tn_nodes, f_hangTime, PathType.CatmullRom, PathMode.Full3D, 10, null).OnComplete(resetBall).SetEase(Ease.OutQuart); 


	}
	void reboundBall_downRight(Vector3 v3_hitPos){
		print("RBBI: dR ");


		/// ball needs to rebound down and fwd... 
		//we need to know the velocity of the ball and check if any units to fall onto...

		print("BALL POS " + tf_ball.position);
		print("HIT POS " + v3_hitPos);


		///get the first re-grid position.....
		v3_newGridPos_1.x = Mathf.RoundToInt(tf_ball.position.x);
		v3_newGridPos_1.y = Mathf.RoundToInt(tf_ball.position.y);
		v3_newGridPos_1.z = Mathf.RoundToInt(tf_ball.position.z);

		print(" GRID POS 1 " + v3_newGridPos_1);

		tf_ball.position = v3_newGridPos_1;
		return;



		//given the delta z we can gauge the velocity
		//print(" - delta z = " + v3_deltaBallPos.z);


		//now that we have the new grid position, we 






		//we need to get the new grid pos 2. (this pos is the locked position we will land on (if unit found (else, air ball, and reset.).....



		//print("CHECK FOR UNIT TO FALL TO: DB ");
		/// given the new locked pos (v3 new grid pos 1), we will scan one unit down. one unit fowrd.
		for(int i = 0; i < 100; i++){
			//scan down a ray cast from new locked pos z - i
			float zPos = (v3_newGridPos_1.z - ((float)i)/2);
			Vector3 pos = new Vector3(v3_newGridPos_1.x, v3_newGridPos_1.y, zPos);
			RaycastHit hit; 
			Ray ray = new Ray(pos, Vector3.down);
			float rayDist = 1 + (float)i;
			if(Physics.Raycast(ray, out hit, rayDist)){
				///if here, we have gathered a new unit to land on... that is in the falling trajectory..
				//print("FOUND FALLING UNIT");
				Grounds_Unit unit = hit.collider.gameObject.GetComponent<Grounds_Unit>();
				found_fallToUnit(unit);
				return;
			}
		}







		//if here, we have not found an object,,, thus, we can send ball on an air ball course
		//print("INIT AIRBALL");
		isAirBall = true;
		v3_newGridPos_2 = new Vector3(v3_newGridPos_1.x, v3_newGridPos_2.y - ((Mathf.Abs(v3_deltaBallPos.y) * 5)), v3_newGridPos_1.z - (Mathf.Abs(v3_deltaBallPos.z) * 5));

		//print(" GRID POS 2 " + v3_newGridPos_2);


		Vector3[] tn_nodes = new Vector3[2];
		tn_nodes[0] = tf_ball.position;
		tn_nodes[1] = v3_newGridPos_2;
		endPos = tn_nodes[1];
		//print("endPos " + endPos);




		float f_hangTime = (Vector2.Distance(tn_nodes[0], tn_nodes[1]) * 0.1f);
		//print("hang time " + f_hangTime);
		///all componenets have been gathered to hit ball......
		tf_ball.DOPath(tn_nodes, f_hangTime, PathType.CatmullRom, PathMode.Full3D, 10, null).OnComplete(resetBall).SetEase(Ease.OutQuart); 


	}

	void reboundBall_upBack(Vector3 v3_hitPos){
		print("RBBI: uF ");


		//we have landed on top of an unforeseen unit... 
		//we must bounce into its node pos.


		//create the slots for new path tween
		Vector3[] tn_nodes = new Vector3[2];


		//adjust endPos to this hit pos, node pos...
		endPos = new Vector3(v3_hitPos.x, v3_hitPos.y + 1, v3_hitPos.z);
		//apply to the tn_nodes
		tn_nodes[1] = endPos;



		///get hieght xyz for bounce
		tn_nodes[0].x = v3_hitPos.x;
		tn_nodes[0].y = endPos.y + 2;
		tn_nodes[0].z = endPos.z - 0.25f;




		//negate airball, since we will land on new pos
		isAirBall = false;

		float f_hangTime = levelMng.playerMng.f_hangTime/2;

		///all componenets have been gathered to hit ball......
		tf_ball.DOPath(tn_nodes, f_hangTime, PathType.CatmullRom, PathMode.Full3D, 10, null).OnComplete(resetBall).SetEase(Ease.InQuart); 

		//
		levelMng.camCntrl.followBall(endPos, f_hangTime);

		

	}


	void reboundBall_upFwd(Vector3 v3_hitPos){
		print("RBBI: uF ");


		//we have landed on top of an unforeseen unit... 
		//we must bounce into its node pos.


		//create the slots for new path tween
		Vector3[] tn_nodes = new Vector3[2];


		//adjust endPos to this hit pos, node pos...
		endPos = new Vector3(v3_hitPos.x, v3_hitPos.y + 1, v3_hitPos.z);
		//apply to the tn_nodes
		tn_nodes[1] = endPos;



		///get hieght xyz for bounce
		tn_nodes[0].x = v3_hitPos.x;
		tn_nodes[0].y = endPos.y + 2;
		tn_nodes[0].z = endPos.z + 0.25f;




		//negate airball, since we will land on new pos
		isAirBall = false;

		float f_hangTime = levelMng.playerMng.f_hangTime/2;

		///all componenets have been gathered to hit ball......
		tf_ball.DOPath(tn_nodes, f_hangTime, PathType.CatmullRom, PathMode.Full3D, 10, null).OnComplete(resetBall).SetEase(Ease.InQuart); 

		//
		levelMng.camCntrl.followBall(endPos, f_hangTime);

		

	}


	void reboundBall_upRight(Vector3 v3_hitPos){
		print("RBBI: uR ");


		//we have landed on top of an unforeseen unit... 
		//we must bounce into its node pos.


		//create the slots for new path tween
		Vector3[] tn_nodes = new Vector3[2];


		//adjust endPos to this hit pos, node pos...
		endPos = new Vector3(v3_hitPos.x, v3_hitPos.y + 1, v3_hitPos.z);
		//apply to the tn_nodes
		tn_nodes[1] = endPos;



		///get hieght xyz for bounce
		tn_nodes[0].x = v3_hitPos.x + 0.25f;
		tn_nodes[0].y = endPos.y + 2;
		tn_nodes[0].z = endPos.z;




		//negate airball, since we will land on new pos
		isAirBall = false;

		float f_hangTime = levelMng.playerMng.f_hangTime/2;

		///all componenets have been gathered to hit ball......
		tf_ball.DOPath(tn_nodes, f_hangTime, PathType.CatmullRom, PathMode.Full3D, 10, null).OnComplete(resetBall).SetEase(Ease.InQuart); 

		//
		levelMng.camCntrl.followBall(endPos, f_hangTime);

		

	}

	void reboundBall_upLeft(Vector3 v3_hitPos){
		print("RBBI: uL ");


		//we have landed on top of an unforeseen unit... 
		//we must bounce into its node pos.


		//create the slots for new path tween
		Vector3[] tn_nodes = new Vector3[2];


		//adjust endPos to this hit pos, node pos...
		endPos = new Vector3(v3_hitPos.x, v3_hitPos.y + 1, v3_hitPos.z);
		//apply to the tn_nodes
		tn_nodes[1] = endPos;



		///get hieght xyz for bounce
		tn_nodes[0].x = v3_hitPos.x - 0.25f;
		tn_nodes[0].y = endPos.y + 2;
		tn_nodes[0].z = endPos.z;




		//negate airball, since we will land on new pos
		isAirBall = false;

		float f_hangTime = levelMng.playerMng.f_hangTime/2;

		///all componenets have been gathered to hit ball......
		tf_ball.DOPath(tn_nodes, f_hangTime, PathType.CatmullRom, PathMode.Full3D, 10, null).OnComplete(resetBall).SetEase(Ease.InQuart); 

		//
		levelMng.camCntrl.followBall(endPos, f_hangTime);

		

	}










	void found_fallToUnit(Grounds_Unit fallToUnit){
		//print(" ##### found _ fall to unit ");
		//print(" ----fall to pos = " + fallToUnit.nodeTarget.position);
		//print(" ----ball pos = " + tf_ball.position);
	
		//////
		tf_ball.DOKill();
		levelMng.camCntrl.cancelFollow();

		////
		v3_newGridPos_2 = fallToUnit.nodeTarget.position;



		////BUG BREAKER - 
		if(tf_ball.position == v3_newGridPos_2){
			//print("BUG BREAKER XA - ball pos = end pos: set tn_nodes1 to be == to end pos");
			v3_newGridPos_1 = tf_ball.position;
		}

		Vector3[] tn_nodes = new Vector3[2];
		tn_nodes[0] = v3_newGridPos_1;
		tn_nodes[1] = v3_newGridPos_2;
	
		//sort time
		float f_hangTime = Vector2.Distance(tn_nodes[0], tn_nodes[1])/10;

		////////
		tf_ball.DOPath(tn_nodes, f_hangTime, PathType.CatmullRom, PathMode.Full3D, 10, null).OnComplete(resetBall);


		//
		isAirBall = false;

	}




	void reset_gamePlay(){


		restart_monitorInput();
	
	}





	void resetBall(){

		//first add stroke
		levelMng.holeMng.strokeTaken();



		//CHECK IF AT DEATH!!!
		//print("CHECK IDF AT DEATH: " + Home_Mng.Instance.levelMng.holeMng.strokeCount + " " + Home_Mng.Instance.levelMng.shotLimit);
		if(Home_Mng.Instance.levelMng.holeMng.strokeCount <= 0){
			print("AT DEATH");
			Home_Mng.Instance.atDeath();
			return;
		}

		Invoke("restart_monitorInput", 0.1f);
		trgtUnit = null;


	
		//print("------ RESET BALL ");
		reset_cldrBalls();
		//print("---- RESET BALL --- pos @ " + tf_ball.position);
		tf_ball.parent = transform;
		levelMng.playerMng.hideClub();


		tf_ballRtn.DOKill();
		tf_ballRtn.localEulerAngles = Vector3.zero;

		levelMng.camCntrl.ballLanded();


	}




	void resetBallScale(){
		//in case of air ball.... 
		tf_ball.localScale = Vector3.one;
	}


	void restart_monitorInput(){
		levelMng.playerMng.startInput();
	}










	void ballSunk(){
		float yPos = tf_ball.localPosition.y - 1;
		tf_ball.DOLocalMoveY(yPos, 0.5f, false).OnComplete(hideBall);
		tf_ball.DOScale(Vector3.zero, 0.5f);
		//tr_ball.enabled = false;
		Home_Mng.Instance.holeSunk();
	}




	void hideBall(){
		tf_ball.position = new Vector3(1000, 1000, 1000);
	}

	public void resetScene(){
		levelMng.holeMng.resetScene();
		//tr_ball.enabled = false;
		tf_ball.position = startPos;
		tf_ball.localScale = Vector3.one;
		levelMng.playerMng.hideClub();
		levelMng.camCntrl.reset(tf_ball);
	}





	public void atDeath(){
		///at death, must play death anim....
		run_killAnim();
		//Home_Mng.Instance.kill_allEnemies();
	}








	public void hitTree(Vector3 hitPos){
		print("HIT TREE");
		//upon hitting tree, we must fall back one unit in opposite direction of trrajectory and fqall to first ground unit.

		tf_ball.DOKill();
		levelMng.camCntrl.cancelFollow();

		if(trajectory == 1){
			print("1a");
			hitPos = new Vector3(hitPos.x, hitPos.y + 1, hitPos.z);
			///get the first re-grid position.....
			v3_newGridPos_1.x = Mathf.RoundToInt(tf_ball.position.x);
			v3_newGridPos_1.y = Mathf.RoundToInt(tf_ball.position.y);
			if(v3_newGridPos_1.y > tf_ball.position.y){
				v3_newGridPos_1.y--;
			}
			if(v3_newGridPos_1.y < lastPos.y){
				v3_newGridPos_1.y++;
			}
			v3_newGridPos_1.z = Mathf.RoundToInt(hitPos.z - 1);
			tf_ball.position = v3_newGridPos_1;
		}
		if(trajectory == 2){
			print("2a");
			hitPos = new Vector3(hitPos.x, hitPos.y + 1, hitPos.z);
			///get the first re-grid position.....
			v3_newGridPos_1.x = Mathf.RoundToInt(tf_ball.position.x - 1);
			v3_newGridPos_1.y = Mathf.RoundToInt(tf_ball.position.y);
			if(v3_newGridPos_1.y > tf_ball.position.y){
				v3_newGridPos_1.y--;
			}
			if(v3_newGridPos_1.y < lastPos.y){
				v3_newGridPos_1.y++;
			}
			v3_newGridPos_1.z = Mathf.RoundToInt(hitPos.z);
			tf_ball.position = v3_newGridPos_1;
		}
		if(trajectory == 3){
			print("3a");
			hitPos = new Vector3(hitPos.x, hitPos.y + 1, hitPos.z);
			///get the first re-grid position.....
			v3_newGridPos_1.x = Mathf.RoundToInt(tf_ball.position.x);
			v3_newGridPos_1.y = Mathf.RoundToInt(tf_ball.position.y);
			if(v3_newGridPos_1.y > tf_ball.position.y){
				v3_newGridPos_1.y--;
			}
			if(v3_newGridPos_1.y < lastPos.y){
				v3_newGridPos_1.y++;
			}
			v3_newGridPos_1.z = Mathf.RoundToInt(hitPos.z + 1);
			tf_ball.position = v3_newGridPos_1;
		}
		if(trajectory == 4){
			print("4a");
			hitPos = new Vector3(hitPos.x, hitPos.y + 1, hitPos.z);
			///get the first re-grid position.....
			v3_newGridPos_1.x = Mathf.RoundToInt(tf_ball.position.x + 1);
			v3_newGridPos_1.y = Mathf.RoundToInt(tf_ball.position.y);
			if(v3_newGridPos_1.y > tf_ball.position.y){
				v3_newGridPos_1.y--;
			}
			if(v3_newGridPos_1.y < lastPos.y){
				v3_newGridPos_1.y++;
			}
			v3_newGridPos_1.z = Mathf.RoundToInt(hitPos.z);
			tf_ball.position = v3_newGridPos_1;
		}







		Vector3 endPos = Vector3.zero;

		//cast ray to see if any grounds units, exist with in 12 units....
		Ray ray = new Ray(tf_ball.position, Vector3.down);
		RaycastHit hit; 
		if(Physics.Raycast(ray, out hit, 12)){
			//if here, we have hit....
			if(hit.collider.CompareTag("GROUNDS_UNIT")){
				///we have found unit to fall to..
				print("Found Unit to Fall To.");
				endPos = new Vector3(hit.transform.position.x, hit.transform.position.y + 1, hit.transform.position.z);	
				float time = (Vector3.Distance(tf_ball.position, hit.transform.position)/12);
				tf_ball.DOMove(endPos, time, false).SetEase(Ease.InQuint).OnComplete(resetBall);
				levelMng.camCntrl.followBall(endPos, time);
			}
		}
		else{
			endPos = new Vector3(tf_ball.position.x, tf_ball.position.y - 12, tf_ball.position.z);
			tf_ball.DOMove(endPos, 1, false).SetEase(Ease.InQuint).OnComplete(resetBall_toLastPos);
		}
	



	}





	void runSplash(){
		//we landed in a water hazard....
		///sink ball anim
		sinkBallAnim();
		trgtUnit.runSplash();
	}
	void sinkBallAnim(){
		print("sink ball anim");
		float y = tf_ball.position.y - 1;
		tf_ball.DOLocalMoveY(y, 0.5f, false).OnComplete(resetBall_toLastPos);
	}



















	void run_killAnim(){
		//print("run_killAnim");
		tf_rndr.localScale = Vector3.zero;
		for(int i = 0; i < ballBit_units.Count; i++){
			ballBit_units[i].run();
		}

	
		//Invoke("resetBall_toLastPos", 0.5f);
	}

	void resetBall_toLastPos(){
		print("reset ball to last pos " + tf_ball.name);
		tf_ball.position = lastPos;
		levelMng.playerMng.hideClub();
		tf_ballRtn.localEulerAngles = Vector3.zero;

		for(int i = 0; i < ballBit_units.Count; i++){
			//ballBit_units[i].reset();
		}

		tf_ball.localScale = Vector3.zero;
		tf_ball.DOScale(Vector3.one, 0.25f).SetEase(Ease.OutBounce);
		rndr.enabled = true;
		levelMng.camCntrl.reset_forAirBall(tf_ball);

		resetBall();


	}

	///ENEMY COLLISIONS
	public void ballHitEnemy_spike(){
		///so, ball has landed on a spike... 

		killTweens();

		run_killAnim();
	}





	///LANDED ON SPRING

	public void ballHit_spring(){
		///so, ball has landed on a spike... 
		//levelMng.playerMng.checkForTarget();
		print("?? bal hit spring");
	}



	///LANDED ON SHIFTER UNIT
	public void ballHit_shifter(Transform unit_shifter){
		//print("BALL HIT SHIFTER");
		//must get the trajectory and magnitude of this unit shifter. 
		unit_shifter.SendMessage("getData");	
	
	}
	public void ping_shifter(int trj, float mgn){
		//return, unit shifter, carrying magnitude and trajectory of shift. 
		//print("PING BALL HIT SHIFTER");
		Vector3 newPos = Vector3.zero;

		if(trj == 1){
			newPos = new Vector3(tf_ball.position.x, tf_ball.position.y, tf_ball.position.z + mgn);
		}
		if(trj == 2){
			newPos = new Vector3(tf_ball.position.x + mgn, tf_ball.position.y, tf_ball.position.z);
		}
		if(trj == 3){
			newPos = new Vector3(tf_ball.position.x, tf_ball.position.y, tf_ball.position.z - mgn);
		}
		if(trj == 4){
			newPos = new Vector3(tf_ball.position.x - mgn, tf_ball.position.y, tf_ball.position.z);
		}


		//print("ball pos " + tf_ball.position + ", newPos " + newPos);

	
		tf_ball.DOMove(newPos, mgn/2, false).SetEase(Ease.OutBounce).OnComplete(resetBall).SetDelay(0.25f);
		levelMng.camCntrl.followBall(newPos, mgn/3);
	}








	void runSplash(int type_del){

		ballSplashMng.run(type_del, tf_ball.position);	
		
	}




}
