﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Coin_Mng : MonoBehaviour {

	public static Coin_Mng Instance;
	public static int coins;
	public Home_Mng homeMng;
	[HideInInspector]
	public TextMesh[] tm_coins;

	public TextMesh tm_coinsCollectedHud;

	[HideInInspector]
	public List<Coin_Unit> units;
	[HideInInspector]
	public List<Coin_Unit> units_active;

	public Transform tf_tm_coinsHUD;
	public GameObject gm_coinsHUD;


	public Transform tf_strokeParHud;
	public TextMesh tm_strokeParHud;
	Vector3 startPos_tm_strokeParHud;



	int strokeCount;//init at hole sunk, = to holemng.strokeCount, use to help animate into bank


	// Use this for initialization
	void Awake () {
		Instance = this;

		coins = PlayerPrefs.GetInt("coins");


		startPos_tm_strokeParHud = tf_strokeParHud.position;

		update_coinsInHUD();
	
	}


	void Start(){
		Vector3 rtn = new Vector3(0,180,0);
	}



	public void setCoins(Grounds_Mng groundsMng){
		print("?????????CoinMng _ setCoins() - for pick ups.");

		/*
		for(int i = 0; i < groundsMng.units.Count; i++){
			if(groundsMng.units[i].isCoinPos){
				units[0].set(groundsMng.units[i].nodeTarget.position);
			}
		}*/
	}



	public void set_ShotsCollected(){
		print("??? set shots collected");
		int shotsCollected = coins + homeMng.levelMng.holeMng.par;
		tm_coinsCollectedHud.text = shotsCollected.ToString();
	}



	public void subtract_coinsToCoins(int coinsSubtracted){
		print("!!!! subtract coins ");
		coins = coins - coinsSubtracted;
		update_coinsInHUD();
	}

	public void add_coinstoCoins(int coinsAdded){
		print("XXXadd coins to hud");
		coins = coins + coinsAdded;
		update_coinsInHUD();
	}


	public void Coin_pickedUp(){
		add_coinstoCoins(1);
		///a coin was obtained, on the grounds. As such, increase par.
		print("home mng " + homeMng.transform.name);
		print("levelMng " + homeMng.levelMng.transform.name);
		homeMng.levelMng.gracePar();
	}

	public void update_coinsInHUD(){
		print("??? update coins to hud?");
		tm_coinsCollectedHud.text = coins.ToString();
		print("coins = " + coins);
		Save();
		print("√ SAVING ");


		//print("??? adjust all coins owned texts??");
		for(int i = 0; i < tm_coins.Length; i++){
			tm_coins[i].text = ""; //coins.ToString();
		}
	}


	public void holeSunk(){
		print("coin mg, hole sunk hole sunk");
		print("COINS " + coins + " vs coins won = " + homeMng.levelMng.holeMng.strokeCount);
		coins = coins + homeMng.levelMng.holeMng.strokeCount;
		print("COINS UPDATED = " + coins);
		tm_coinsCollectedHud.text = coins.ToString();
		strokeCount = homeMng.levelMng.holeMng.strokeCount;
		tm_strokeParHud.text = "";

		Save();

	
	}

	void pass_strokesToBank(){
		print("?? pass strokes to bank?");
		coins++;
		strokeCount--;

		tm_strokeParHud.text = strokeCount.ToString() + "/" + homeMng.levelMng.holeMng.par.ToString();
		tm_coinsCollectedHud.text = coins.ToString();


		if(strokeCount == 0){
			CancelInvoke("pass_strokesToBank");
			//turnOff_HUD();
			Save();
		}					
	}



	public void turnOff_HUD(){
		print("???turn off hud");
		//gm_coinsHUD.SetActive(false);
	}

	public void turnOn_HUD(){
		print("???turn on hud");
		//gm_coinsHUD.SetActive(true);
	}




	public void reset(){
		///generally at clear this level, prep to play next.... reset units...
		print("??? coin mng, reset()");

		/*
		for(int i = 0; i < units_active.Count; i++){
			units_active[i].reset();
		}*/
	}







	public void Save(){
		//just storing coins
		PlayerPrefs.SetInt("coins", coins);
		PlayerPrefs.Save();
		print("√ SAVING");
	}



	//atQUIT
	public void atQuit(){
		Save();
	}






}
