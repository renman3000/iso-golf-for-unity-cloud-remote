﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Tree_Unit : MonoBehaviour {

	public Transform tf_main;
	public Transform tf_branch1;
	public Transform tf_branch2;
	public Transform tf_branch3;
	public Transform tf_branch4;


	public Transform tf_branch1A;
	public Transform tf_branch2A;
	public Transform tf_branch3A;
	public Transform tf_branch4A;


	public Transform tf_pivot;
	public Transform tf_coconut;
	bool isDropped_cocout;

	// Use this for initialization
	void Start () {



		//tf_main.DOLocalMoveY(0.18f, 1, false);

		float time = 4f;

		//tf_main.DOLocalMoveY(-0.25f, time, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.OutSine); 


		tf_branch1.DOLocalMoveY(0.25f, time, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);  
		tf_branch2.DOLocalMoveY(0.25f, time, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);  
		tf_branch3.DOLocalMoveY(0.25f, time, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);  				
		tf_branch4.DOLocalMoveY(0.25f, time, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);  


		tf_branch1A.DOLocalMoveY(0.25f, time, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);  
		tf_branch2A.DOLocalMoveY(0.25f, time, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);  
		tf_branch3A.DOLocalMoveY(0.25f, time, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);  				
		tf_branch4A.DOLocalMoveY(0.25f, time, false).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);  
			
							

	
	}


	void OnTriggerEnter(Collider col){
		if(col.CompareTag("BALL") || col.CompareTag("CLDR_BALL_VERT") ||  col.CompareTag("CLDR_BALL_HORZ")){
			print("BALL HIT TREE, " + col.tag);
			shakeTree(Home_Mng.Instance.levelMng.ballMng.trajectory);
			Home_Mng.Instance.levelMng.ballMng.hitTree(transform.position);
		}

	}



	public void shakeTree(int trj){

		Vector3 rtn = Vector3.zero;

		if(trj == 1){
			rtn = new Vector3(5, 0, 0);
		}
		if(trj == 2){
			rtn = new Vector3(0, 0, 5);
		}
		if(trj == 3){
			rtn = new Vector3(-5, 0, 0);
		}
		if(trj == 4){
			rtn = new Vector3(0, 0, -5);
		}




		tf_pivot.DOPunchRotation(rtn, 1, 5, 0.5f);
	
	}


	void dropCoconut(){
		if(tf_coconut && !isDropped_cocout){
			isDropped_cocout = true;

		}
	}



}
