﻿using UnityEngine;
using System.Collections;

public class Enemy_Mng : MonoBehaviour {

	public static Enemy_Mng Instance;

	public GameObject[] mngs;



	void Awake(){
		Instance = this;
	}

	void Start () {

		//InvokeRepeating("mng_enemy", 5, 5);
		//run();
	}

	int int_state;
	void mng_enemy(){

		int_state++;
		if(int_state == 1){
			run();
			return;
		}
		else{
			hide();
			int_state = 0;
			return;
		}
	}

	public void run(){
		mngs[0].SendMessage("run");
	}
	public void hide(){
		mngs[0].SendMessage("hide");
	}






}
