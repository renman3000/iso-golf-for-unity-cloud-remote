﻿using UnityEngine;
using System.Collections;

public class ScreenShot_Mng : MonoBehaviour {

	[HideInInspector]
	public Texture2D pic;
	public SpriteRenderer whiteFlash;
	public static ScreenShot_Mng Instance;
	public Renderer rndr;
	public Material ml_;

	// Use this for initialization
	void Awake() {
		Instance = this;
	}

	void Start(){

    }
	
	public void takePicture(){
		StartCoroutine(_takePic());
		///Invoke ("_runWhiteFlash", 0.1f);
	}

	IEnumerator _takePic(){
		Title_Mng.instance.tf_home.localScale = Vector3.one;
		yield return new WaitForEndOfFrame();
		Home_Mng.Instance.camMng.turnOn_titleCam();
		pic = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
		pic.ReadPixels(new Rect(0, 0, Screen.width, Screen.height),0,0);
		pic.Apply();
		ml_.mainTexture = pic;
		rndr.material = ml_;
		Home_Mng.Instance.camMng.turnOff_titleCam();
		Title_Mng.instance.tf_home.localScale = Vector3.zero;
		//reset the win of hole mng
		Home_Mng.Instance.levelMng.holeMng.tf_win.localScale = Vector3.one;
		yield return null;
	}
	void _runWhiteFlash(){
		StartCoroutine(runWhiteFlash());
	}
	IEnumerator runWhiteFlash(){
		print ("RUN WHITE FLASH");
		bool running = true;
		whiteFlash.enabled = true;
		whiteFlash.color = new Color(1, 1, 1, 1);
		Color clr = whiteFlash.color;
		while(running){
			clr.a = clr.a - Time.deltaTime;
			whiteFlash.color = clr;
			if(clr.a <= 0){
				running = false;
				whiteFlash.enabled = false;
			}
			yield return null;
		}
	}

	public void sharePic(){
		print("share pic");
		//at share pic, we must bring in the image, in its "holder", and remove the current ERBs'

	}





	public void bp_share_fb(){
		print("fb");
		SOCIAL_MNG.instance.post_pic_FB(pic);
	}
	public void bp_share_tw(){
		print("tw");
		SOCIAL_MNG.instance.post_pic_TW(pic);
	}
	public void bp_share_ig(){
		print("ig");
		SOCIAL_MNG.instance.post_pic_IG(pic);
	}
	public void bp_save_localDevice(){

	}

}
