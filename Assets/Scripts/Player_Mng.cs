﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Player_Mng : MonoBehaviour {

	[HideInInspector]
	public Level_Mng levelMng;


	public RectTransform rt_btn;
	Vector2 rt_size;


	Vector2 mp_start;
	Vector2 mp_crnt;
	Vector2 mp_end;
	Vector2 mp_delta;


	int trajectory;

	float dist_ballHit;

	[HideInInspector]
	public float f_hangTime;

	bool isAirBall;


	Vector3 endPos;
	Vector3[] tn_nodes = new Vector3[2];
	float height_full;

	[HideInInspector]
	public Transform tf_ball;


	public Transform tf_club;
	public Transform tf_clubHolder;
	float f_club_peakRtn;
	float f_swingTime;

	public Transform tf_hudShifter;
	Vector3 pos_hudShifter_reset;

	public LineRenderer lr_shotGauge;
	Camera ui_cam;
	public Transform tf_shotGaugeRunner;
	Vector3 v3_shotGauge_pos1;


	Grounds_Unit trgtUnit;


	bool isDead;




	public static float powerFactor = 5;/// <summary>
	/// normal is 1, in sand 0.5f etc......
	/// </summary>

	public TrailRenderer tr_ball;


	void Awake(){
		rt_btn.SetParent(Home_Mng.Instance.tf_Canvas);
		rt_btn.localPosition = Vector2.zero;
		rt_size = new Vector2(250, 250);
		//rt_btn.localScale = Vector3.one;
	}


	void Start () {
		
		stopInput();
		pos_hudShifter_reset = new Vector3(2.5f, 2.5f, 0);
		//tf_hudShifter.localPosition = pos_hudShifter_reset;
		//tf_hudShifter.localScale = Vector3.zero;
		powerFactor = 1;
		tr_ball.enabled = false;
	}


	////////////////
	/// SCENE COMMANDS. HANDLE GLOBAL SCENE ELEMENTS
	//////////////
	/////////////
	////////////
	////////////
	/////////////

	///init scene
	public void initScene(){
		ui_cam = Home_Mng.Instance.camMng.cam_UI;
		mp_start = ui_cam.WorldToScreenPoint(Vector3.zero);
	}
	/// Resets the scene. 
	public void resetScene(){
		
	}
	/// Stops the input.
	public void stopInput(){
		print("STOP PLAYER INPUT");
		rt_btn.sizeDelta = Vector2.zero;
	}
	/// Starts the input.
	public void startInput(){
		if(isDead){
			return;
		}
		print("START PLAYER INPUT");
		rt_btn.sizeDelta = rt_size;
		print(rt_btn.sizeDelta);
	}





	public void btnPressed(){
		mp_start = Input.mousePosition;

		lr_shotGauge.SetPosition(0, tf_ball.position);
		lr_shotGauge.SetPosition(1, tf_ball.position);
		/*
		if(levelMng.camCntrl.camGameScene.orthographicSize != levelMng.camCntrl.f_OS_ball){
			///means we are not zoomed on ball, as such, zoom in
			levelMng.camCntrl.flyTo_Ball(levelMng.ballMng.tf_ball.position);
		}*/
	}


	public void btnDragged(){
		monitorSwing(Input.mousePosition);
	}

	public void btnReleased(){
		//print("!!!@@!!!@@@!!!@@@!!@@!@@!@@@@@!!!RELEASED@!@!@@@@!!!!!!!@" + Time.time);
		mp_end = Input.mousePosition;
		getDelta_atRelease();
	}








	/////
	//////
	//////
	//////
	//////
	//////
	/// FOR THE ANIMATION OF THE CLUB....
	void monitorSwing(Vector2 mp_crnt){


		///WE MUST KNOW THE SWING AIM SO WE CAN CREATE THE RIGHT SWING ANIM

		tf_club.localScale = Vector3.one;
		int trj = 0;
		if(mp_start.x > mp_crnt.x){
			if(mp_start.y > mp_crnt.y){
				trj = 1;
			}
			if(mp_start.y < mp_crnt.y){
				trj = 2;
			}
		}
		if(mp_start.x < mp_crnt.x){
			if(mp_start.y < mp_crnt.y){
				trj = 3;
			}
			if(mp_start.y > mp_crnt.y){
				trj = 4;
			}
		}

		//set rotation of club holder based on trajectory....
		float rtnFctr = 0;
		if(trj == 1){
			tf_clubHolder.localEulerAngles = Vector3.zero;
			rtnFctr = (mp_crnt.x - mp_start.x) * 1f;
			tf_club.localEulerAngles = new Vector3(-rtnFctr,0,0);
			levelMng.ballMng.tf_rndr.localEulerAngles = new Vector3(0,270,0);
		}
		if(trj == 2){
			tf_clubHolder.localEulerAngles = new Vector3(0,90,0);
			rtnFctr = Mathf.Abs((mp_crnt.x - mp_start.x) * 0.65f);
			tf_club.localEulerAngles = new Vector3(rtnFctr,0,0);
			levelMng.ballMng.tf_rndr.localEulerAngles = new Vector3(0,0,0);
		}
		if(trj == 3){
			tf_clubHolder.localEulerAngles = new Vector3(0,0,0);
			rtnFctr = -Mathf.Abs((mp_crnt.x - mp_start.x) * 0.65f) + 360;
			tf_club.localEulerAngles = new Vector3(rtnFctr,0,0);
			levelMng.ballMng.tf_rndr.localEulerAngles = new Vector3(0,90,0);
		}
		if(trj == 4){
			tf_clubHolder.localEulerAngles = new Vector3(0,90,0);
			rtnFctr = -Mathf.Abs((mp_crnt.x - mp_start.x) * 0.65f) + 360;
			tf_club.localEulerAngles = new Vector3(rtnFctr,0,0);
			levelMng.ballMng.tf_rndr.localEulerAngles = new Vector3(0,180,0);
		}

	
		//set_hud(mp_crnt);

		set_lr_shotGauge(trj, rtnFctr);

	}

	void set_lr_shotGauge(int trj, float rtnFctr){
		////
		//first, get the rounded int value of the swing
		Vector2 mp_crnt = Input.mousePosition;
		float dist = (((Vector2.Distance(mp_start, mp_crnt)/25) * (powerFactor)));


		///now, we must the position of one end of the line renderer based on the dist and trj...
		if(trj == 1){
			Vector3 pos = new Vector3(tf_ball.position.x, tf_ball.position.y, tf_ball.position.z - dist);
			lr_shotGauge.SetPosition(1, pos);
			v3_shotGauge_pos1 = pos;
		}
		if(trj == 2){
			Vector3 pos = new Vector3(tf_ball.position.x - dist, tf_ball.position.y, tf_ball.position.z);
			lr_shotGauge.SetPosition(1, pos);
			v3_shotGauge_pos1 = pos;
		}
		if(trj == 3){
			Vector3 pos = new Vector3(tf_ball.position.x, tf_ball.position.y, tf_ball.position.z + dist);
			lr_shotGauge.SetPosition(1, pos);
			v3_shotGauge_pos1 = pos;
		}
		if(trj == 4){
			Vector3 pos = new Vector3(tf_ball.position.x + dist, tf_ball.position.y, tf_ball.position.z);
			lr_shotGauge.SetPosition(1, pos);
			v3_shotGauge_pos1 = pos;
		}



	}




	void set_hud(Vector2 mp_crnt){
		///
	
		Vector2 this_mp_delta = Vector2.zero;
		this_mp_delta.x = mp_crnt.x - mp_start.x;
		this_mp_delta.y = mp_crnt.y - mp_start.y;

		///
		float swingFactor = Mathf.Abs(Vector2.Distance(mp_start, mp_crnt)/35);
		///
		float crntPos_x = (pos_hudShifter_reset.x - swingFactor);
		//tf_hudShifter.localPosition = new Vector3(crntPos_x, pos_hudShifter_reset.y, pos_hudShifter_reset.z);
		//levelMng.camCntrl.set_zoom(swingFactor);


	}


	void reset_hud(float swingTime){
		swingTime = swingTime;
		//tf_hudShifter.DOLocalMoveX(pos_hudShifter_reset.x, swingTime, false).SetEase(Ease.InQuint);
	}



	/////////////////
	/// /ON RELEASE OF INPUT INIT ALL COMPONENTS TO HIT BALL
	//////////////////////////
	///////////////////////////////////////

	//GET DELTA 
	void getDelta_atRelease(){

		mp_delta.x = mp_end.x - mp_start.x;
		mp_delta.y = mp_end.y - mp_start.y;


		/////SORT OUR THE TRAJECTORY OF THE SWING
		if(mp_delta.x > 0){
			if(mp_delta.y > 0){
				trajectory = 3;
			}
			if(mp_delta.y < 0){
				trajectory = 4;
			}
		}
		if(mp_delta.x < 0){
			if(mp_delta.y < 0){
				trajectory = 1;
			}
			else if(mp_delta.y > 0){
				trajectory = 2;
			}
		}

		///CALCULATE SWING FCTR
		dist_ballHit = Mathf.RoundToInt(((Vector2.Distance(mp_start, mp_end)/25) * (powerFactor)));
		print("1. swing factor = " + dist_ballHit + ", trajectory = " + trajectory);
		if(dist_ballHit == 0)
		dist_ballHit = 1;

		//cast a ray, in trajectory dir. see if direction against a wall unit.
		seeIf_againstWall();

		//find_target();



		//checkForTarget();
		//levelMng.shotTaken();



	}


	void seeIf_againstWall(){
		Vector3 rayDir = Vector3.zero;
		if(trajectory == 1){
			rayDir = Vector3.forward;
		}
		if(trajectory == 2){
			rayDir = Vector3.right;
		}
		if(trajectory == 3){
			rayDir = Vector3.back;
		}
		if(trajectory == 4){
			rayDir = Vector3.left;
		}


		Ray ray = new Ray(tf_ball.position, rayDir);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit, 1)){ 
			//we have hit a wall
			print("have hit wall");
			hideClub();
			return;
		}


		find_target();
	}


	void find_target(){

		if(trajectory == 1){
			Vector3 targetPos = new Vector3(tf_ball.position.x, tf_ball.position.y, tf_ball.position.z + dist_ballHit);
			set_trajectory1(targetPos);
		}
		if(trajectory == 2){
			Vector3 targetPos = new Vector3(tf_ball.position.x + dist_ballHit, tf_ball.position.y, tf_ball.position.z);
			set_trajectory2(targetPos);
		}
		if(trajectory == 3){
			Vector3 targetPos = new Vector3(tf_ball.position.x, tf_ball.position.y, tf_ball.position.z - dist_ballHit);
			set_trajectory3(targetPos);
		}
		if(trajectory == 4){
			Vector3 targetPos = new Vector3(tf_ball.position.x - dist_ballHit, tf_ball.position.y, tf_ball.position.z);
			set_trajectory4(targetPos);
		}





	}







	////SET THE TN NODES FOR BALL TWEEN

	/// <summary>
	/// 1
	/// </summary>
	void set_trajectory1(Vector3 targetPos){
		///ball hit fwd...


		//account for varying hieghts..
		height_full = Mathf.Abs((dist_ballHit) + tf_ball.position.y);
		float zDiff = (targetPos.z - tf_ball.position.z);

		Vector3 nodePos = Vector3.zero;///init var here... used thruout.

	
		//BALL LAUNCHED


		//BALL AT FIRT PEAK 
		//first mid air pos
		nodePos.x = tf_ball.position.x;
		if(height_full > 3){
			height_full = height_full * 0.9f;
		}
		nodePos.y = height_full;
		nodePos.z = (zDiff/2) + tf_ball.position.z;
		tn_nodes[0] = nodePos;



		///BALL HIT GROUND			
		///set last node as nodePos
		tn_nodes[1] = targetPos;



		//swing club
		swingClub_ph1();


	}
	/// <summary>
	/// /2
	/// </summary>
	void set_trajectory2(Vector3 targetPos){
		///ball hit fwd...


		//account for varying hieghts..
		height_full = Mathf.Abs((dist_ballHit) + tf_ball.position.y);
		float xDiff = (targetPos.x - tf_ball.position.x);

		Vector3 nodePos = Vector3.zero;///init var here... used thruout.

	
		//BALL LAUNCHED


		//BALL AT FIRT PEAK 
		//first mid air pos
		nodePos.x = (xDiff/2)+ tf_ball.position.x;
		nodePos.y = height_full * 0.9f;
		nodePos.z = tf_ball.position.z;
		tn_nodes[0] = nodePos;



		///BALL HIT GROUND			
		///set last node as nodePos
		tn_nodes[1] = targetPos;



		//swing club
		swingClub_ph1();


	}
	/// <summary>
	/// /3
	/// </summary>
	void set_trajectory3(Vector3 targetPos){
		///ball hit fwd...


		//account for varying hieghts..
		height_full = Mathf.Abs((dist_ballHit) + tf_ball.position.y);
		float zDiff = -Mathf.Abs(targetPos.z - tf_ball.position.z);

		Vector3 nodePos = Vector3.zero;///init var here... used thruout.

	
		//BALL LAUNCHED


		//BALL AT FIRT PEAK 
		//first mid air pos
		nodePos.x = tf_ball.position.x;
		nodePos.y = height_full * 0.9f;
		nodePos.z = (zDiff/2) + tf_ball.position.z;
		tn_nodes[0] = nodePos;



		///BALL HIT GROUND			
		///set last node as nodePos
		tn_nodes[1] = targetPos;



		//swing club
		swingClub_ph1();


	}
	/// <summary>
	/// 4
	/// </summary>
	void set_trajectory4(Vector3 targetPos){
		///ball hit fwd...


		//account for varying hieghts..
		height_full = Mathf.Abs((dist_ballHit) + tf_ball.position.y);
		float xDiff = -Mathf.Abs(targetPos.x - tf_ball.position.x);

		Vector3 nodePos = Vector3.zero;///init var here... used thruout.

	
		//BALL LAUNCHED


		//BALL AT FIRT PEAK 
		//first mid air pos
		nodePos.x = (xDiff/2)+ tf_ball.position.x;
		nodePos.y = height_full * 0.9f;
		nodePos.z = tf_ball.position.z;
		tn_nodes[0] = nodePos;



		///BALL HIT GROUND			
		///set last node as nodePos
		tn_nodes[1] = targetPos;



		//swing club
		swingClub_ph1();


	}








	////////////
	/////////////
	/// SWING CLUB .. init hit ball
	//////////////
	//////////////
	//////////////
	///////////////
	void swingClub_ph1(){
		f_swingTime = dist_ballHit/20;
		f_club_peakRtn = tf_club.localEulerAngles.x;
		tf_club.DOLocalRotate(Vector3.zero, f_swingTime, 0).OnComplete(hitBall).OnComplete(hitBall).SetEase(Ease.InQuart); 
		reset_hud(f_swingTime);
		run_shotGaugeRunner(f_swingTime);	
	}
	void swingClub_ph2(){
		/// club is at 0 rtn... just made "contact" with ball...
		/// tween thru to mirror rtn
		tf_club.DOLocalRotate(new Vector3(-f_club_peakRtn, 0,0), f_swingTime, 0).OnComplete(hideClub).SetEase(Ease.OutQuart);
	}
	void run_shotGaugeRunner(float f_swingTime){
		///we have released club at this point, and it is initiated to swing.
		//this fucntion will place a runner 9empty transform at lr_shotGauge pos 1, and tween back to ball.
		///subsequently, the runner will dictate a new pos for the lr shotGauge....
		tf_shotGaugeRunner.position = v3_shotGauge_pos1;
		tf_shotGaugeRunner.DOMove(tf_ball.position, f_swingTime, false).SetEase(Ease.InQuart).OnUpdate(update_shotGauge);
	}
	void update_shotGauge(){
		lr_shotGauge.SetPosition(1, tf_shotGaugeRunner.position);
	}


	void hitBall(){
		//swing ph1 is complete, hit ball.....
		swingClub_ph2();

		f_hangTime = dist_ballHit/4;
		//print("CCCCCCC : f hang time: " + f_hangTime + ", isAirBall: " + isAirBall);
		levelMng.ballMng.hitBall(tn_nodes, f_hangTime, isAirBall, trgtUnit, dist_ballHit, trajectory);
		stopInput();
		isAirBall = false;

	}




	public void hideClub(){
		tf_ball = levelMng.ballMng.tf_ball;
		tf_clubHolder.position = new Vector3(tf_ball.position.x, tf_ball.position.y - 1, tf_ball.position.z);
		tf_club.localScale = Vector3.zero;
		f_swingTime = 0;
		f_club_peakRtn = 0;
		lr_shotGauge.SetPosition(0, tf_ball.position);
		lr_shotGauge.SetPosition(1, tf_ball.position);
	}



	void run_alert(){
		if(isAirBall){
			Alerts_Mng.Instance.run_airball();
		}
	}



	public void atDeath(){
		isDead = true;
		stopInput();
	}





}
