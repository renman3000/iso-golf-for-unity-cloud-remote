﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class Number_Delegate_Mng : MonoBehaviour {

	//holds multiple number delegates, acts as intermeiatary to the number manager and number delegate


	public Number_Delegate[] delegates;

	[HideInInspector]
	public List<int> individualValues;


	// Use this for initialization
	void Start () {
	
	}


	public void run_numbers(int value){
		///
		string valueString = value.ToString();
		int valueLength = valueString.Length;
		/////
		for(int i = 0; i < valueLength; i++){
			char e = valueString[i];
			delegates[i].set_number(e);
		}
	}


}
