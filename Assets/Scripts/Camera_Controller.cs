﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Camera_Controller : MonoBehaviour {

	public Level_Mng levelMng;

	public Camera gameCam;

	public Transform tf_camFollow;
	public Transform tf_camHolder;
	public Camera camGameScene;

	Vector3 v3_FT_hole;
	float f_OS_hole;
	[HideInInspector]
	public float f_OS_ball;


	Vector3 extent_left;
	Vector3 extent_right;


	//public GameObject gm_btn_FTH;
	//public GameObject gm_btn_FTB;


	float zoom;

	//public RectTransform rtf_btn;
	//Vector3 rtf_size;



	public Camera camLoading;


	void Awake(){
		gameCam.orthographicSize = 7.5f;

		//GameObject UI_Canvas = GameObject.Find("Root/UI/Canvas");
		//rtf_btn.transform.parent = UI_Canvas.transform;
		//gm_btn_FTB.transform.parent = UI_Canvas.transform;
		//gm_btn_FTH.transform.parent = UI_Canvas.transform;
		turnOff_gameScene();

	}
	public void turnOff_gameScene(){
		gameCam.gameObject.SetActive(false);
	}
	public void turnOn_gameScene(){
		//sent from level info mng, home mng, level mng, player mng. 
		gameCam.gameObject.SetActive(true);
	}


	void Start () {

		f_OS_ball = camGameScene.orthographicSize;

		//gm_btn_FTB.SetActive(false);
		//gm_btn_FTH.SetActive(true);

		//rtf_size = new Vector2(375, 555);
		//rtf_btn.sizeDelta = Vector2.zero;
		stopInput();

		Invoke("setCenter", 1);//used in fly to hole/ball.
	}


	void setCenter(){
		v3_FT_hole.x = (Home_Mng.Instance.levelMng.holeMng.tf_flag.position.x - Mathf.Abs(Home_Mng.Instance.levelMng.ballMng.tf_ball.position.x))/2; 
		v3_FT_hole.y = (Home_Mng.Instance.levelMng.holeMng.tf_flag.position.y); 
		v3_FT_hole.z = (Home_Mng.Instance.levelMng.holeMng.tf_flag.position.z - Mathf.Abs(Home_Mng.Instance.levelMng.ballMng.tf_ball.position.z))/2; 

		//tf_camHolder.position = v3_FT_hole;
		//tf_camFollow.parent = tf_camHolder;
	}


	public void setStart(Transform tf_ball){
		print("set start " + tf_ball.position);
		tf_camFollow.position = tf_ball.position;
		tf_camHolder.position = tf_camFollow.position;
		camGameScene.orthographicSize = 7.5f;
		tn_complete();

	}

	public void followBall(Vector3 endPos, float hang_time){
		tf_camFollow.DOMove(endPos, hang_time, false).SetEase(Ease.InOutSine).OnComplete(tn_complete);
	}


	public void moveToBall(Vector3 trgtPos, float time){
		tf_camFollow.DOMove(trgtPos, time);
	}


	public void cancelFollow(){
		tf_camFollow.DOPause();
		tf_camFollow.DOKill();
	}

	void tn_complete(){


		tf_camFollow.parent = transform;
		tf_camHolder.position = tf_camFollow.position;
		tf_camFollow.parent = tf_camHolder;


	}

	public void reset_forAirBall(Transform tf_ball){
		//last stroke was an air ball, accordingly.... 
		print("CAM reset for air ball");
		tf_camFollow.DOMove(tf_ball.position, 0.5f, false).OnComplete(tn_complete);
	
	}



	public void reset(Transform tf_ball){
		tf_camFollow.parent = transform;
		tf_camFollow.position = tf_ball.position;
		tf_camHolder.position = tf_camFollow.position;
		tf_camFollow.parent = tf_camHolder;
	}

	public void resetScene(){
		//gm_btn_FTB.SetActive(false);
		//gm_btn_FTH.SetActive(true);
	}



	public void ballLanded(){
		print("??? BALL LANDED " + tf_camFollow.name);
		//tf_camFollow.DOShakePosition(0.1f, 0.05f, 0, 0, false).SetLoops(2, LoopType.Yoyo);

		//
	}




	public void flyTo_Hole(){
		////
		//gm_btn_FTB.SetActive(true);
		//gm_btn_FTH.SetActive(false);
		print("A1");
		///
		v3_FT_hole.x = (Home_Mng.Instance.levelMng.holeMng.tf_flag.position.x - Mathf.Abs(Home_Mng.Instance.levelMng.ballMng.tf_ball.position.x))/2; 
		v3_FT_hole.y = (Home_Mng.Instance.levelMng.holeMng.tf_flag.position.y); 
		v3_FT_hole.z = (Home_Mng.Instance.levelMng.holeMng.tf_flag.position.z - Mathf.Abs(Home_Mng.Instance.levelMng.ballMng.tf_ball.position.z))/2; 


		float dist_ballVsHole = Vector3.Distance(Home_Mng.Instance.levelMng.ballMng.tf_ball.position, Home_Mng.Instance.levelMng.holeMng.tf_flag.position);
			
		f_OS_hole = dist_ballVsHole + 2;

		///
		tf_camFollow.DOMove(v3_FT_hole, 0.5f, false);
		camGameScene.DOOrthoSize(f_OS_hole, 0.5f);

		///
		levelMng.playerMng.stopInput();


		startInput();
	}

	public void flyTo_Ball(Vector3 ballPos){
		///
		//gm_btn_FTB.SetActive(false);
		//gm_btn_FTH.SetActive(true);
		print("A2");
		tf_camHolder.DOKill();
		tf_camFollow.DOMove(ballPos, 0.25f, false).OnComplete(reset_gamePlay).SetEase(Ease.InOutQuint);
		tf_camHolder.DOLocalRotate(Vector3.zero, 0.25f, 0);
		camGameScene.DOOrthoSize(f_OS_ball, 0.25f).SetEase(Ease.InOutQuint);

	}

	void reset_gamePlay(){
		levelMng.playerMng.startInput();
		tf_camFollow.parent = transform;
		tf_camHolder.position = tf_camFollow.position;
		tf_camFollow.parent = tf_camHolder;
	}


	public void ballSunk(){


		//gm_btn_FTB.SetActive(false);
		//gm_btn_FTH.SetActive(false);


	}







	/// Stops the input.
	public void stopInput(){
		//rtf_btn.sizeDelta = Vector2.zero;
	}
	/// Starts the input.
	public void startInput(){
		//rtf_btn.sizeDelta = rtf_size;
	}


	public void activeInput(){
		//potenial for zoom and spin.....
	}



	void Update(){
		if(Input.GetKeyUp("right")){
			print("spin right");
			spinRight_OneUnit();
		}
		if(Input.GetKey("left")){
			print("spin left");
			spinLeft_OneUnit();
		}
	}


	void spinLeft_OneUnit(){
		float targetRtn_y = tf_camHolder.localEulerAngles.y + 45;
		Vector3 v3_rtnTo = new Vector3(0, targetRtn_y, 0);
		tf_camHolder.DOLocalRotate(v3_rtnTo, 0.5f, 0).OnComplete(startInput);
		stopInput();
	}
	void spinRight_OneUnit(){
		float targetRtn_y = tf_camHolder.localEulerAngles.y - 45;
		Vector3 v3_rtnTo = new Vector3(0, targetRtn_y, 0);
		tf_camHolder.DOLocalRotate(v3_rtnTo, 0.5f, 0).OnComplete(startInput);
		stopInput();
	}



	////MONITOR DRAG CAM
	void spinCam(){
		
	}

	void dragCam(){
		
	}


	public void zoomOut_onBall(float time){
		print("A3");
		camGameScene.DOOrthoSize(7.5f, time).SetEase(Ease.InOutQuint);
	}




	public void set_Extent(Vector3 pos){
		extent_right = pos;
		//print("setExtent()::pos " + pos);
		//show_BallVsHole();
	}

	public void display_grounds(){
		Transform tf_ball = levelMng.ballMng.tf_ball;


		print("display grounds ");


		//insures cam shows full level at start of game play.
		Vector3 centerPos = Vector3.zero;
		if(tf_ball.position.x > extent_right.x){

			//centerPos.x = tf_ball.position.x - ext

		}

		centerPos.x = tf_ball.position.x - extent_right.x;
		centerPos.y = levelMng.holeMng.tf_flag.position.y;
		centerPos.z = tf_ball.position.z - extent_right.z;


		float dist = Vector3.Distance(Vector3.zero, centerPos);

		tf_camFollow.parent = transform;
		tf_camFollow.position = centerPos;
		tf_camHolder.position = centerPos;
		tf_camFollow.parent = tf_camHolder;
		zoom = dist * 2.5f;
		if(camGameScene.orthographicSize == zoom){
			zoomIn_onBall();
			return;
		}
		else{
			print("A5");
			camGameScene.DOOrthoSize(dist * 2.5f, 0.25f);
		}
		Vector3 rtn = new Vector3(0,360,0);
		//tf_camHolder.DORotate(rtn, 40, RotateMode.FastBeyond360).SetLoops(-1).SetEase(Ease.Linear);
	
	}


	void zoomIn_onBall(){	
		print("zoom in on ball");
		Vector3 endPos = Home_Mng.Instance.levelMng.ballMng.tf_ball.position;
		tf_camFollow.DOMove(endPos, 0.25f, false).SetEase(Ease.InOutSine).OnComplete(tn_complete);
		print("A6");
		camGameScene.DOOrthoSize(7.5f, 0.25f);
	}


	public void frame_ball(){
		return;
		Vector3 pos = Home_Mng.Instance.levelMng.ballMng.tf_ball.position;
		camGameScene.orthographicSize = 7.5f;
		tf_camFollow.position = pos;
	}



	float crntZoom;
	public void set_zoom(float swingFactor){
		//sent from player mng at monitor swing... adjust zoom based on swing.

		print("swingFactor " + swingFactor);
		crntZoom = camGameScene.orthographicSize;
		float adjustZoom = (swingFactor * Time.deltaTime) + crntZoom;
		camGameScene.orthographicSize = adjustZoom;
		//lastZoom = crntZoom;






	}



	public void show_BallVsHole(){

		Transform tf_ball = levelMng.ballMng.tf_ball;
		Vector3 holePos = levelMng.holeMng.tf_flag.position;

		print("show ball vs hole ");

		print("bpos = " + tf_ball.position + " VS hole pos = " + holePos); 

		float diff = 0;

		//insures cam shows full level at start of game play.
		Vector3 centerPos = Vector3.zero;
		if(tf_ball.position.x > holePos.x){
			diff = tf_ball.position.x - holePos.x;
			centerPos.x = tf_ball.position.x - (diff/2);
		}
		else if(tf_ball.position.x < holePos.x){
			diff = holePos.x - tf_ball.position.x;
			centerPos.x = tf_ball.position.x + (diff/2);
		}
		else if(tf_ball.position.x == holePos.x){
			centerPos.x = tf_ball.position.x;
		}


		if(tf_ball.position.z > holePos.z){
			diff = tf_ball.position.z - holePos.z;
			centerPos.z = tf_ball.position.z - (diff/2);
		}
		else if(tf_ball.position.z < holePos.z){
			diff = holePos.z - tf_ball.position.z;
			centerPos.z = tf_ball.position.z + (diff/2);
		}
		else if (tf_ball.position.z == holePos.z){
			centerPos.z = tf_ball.position.z;
		}
		centerPos.y = levelMng.holeMng.tf_flag.position.y;


		print("centerPos " + centerPos);



		float dist = Vector3.Distance(tf_ball.position, holePos);

		zoom = dist;
		print("zoom " + zoom + " VS cam size " + camGameScene.orthographicSize);
		if(camGameScene.orthographicSize == zoom){
			zoomIn_onBall();
			return;
		}
		else{
			print("A7");
			tf_camFollow.parent = transform;
			tf_camHolder.position = centerPos;
			tf_camFollow.parent = tf_camHolder;
			tf_camFollow.DOMove(centerPos, 0.25f);
			camGameScene.DOOrthoSize(dist, 0.25f);

		}
		Vector3 rtn = new Vector3(0,360,0);
		//tf_camHolder.DORotate(rtn, 40, RotateMode.FastBeyond360).SetLoops(-1).SetEase(Ease.Linear);
	
	}





	/*
	public void showLoading(){
		print("SHOW LOADING");
		camLoading.gameObject.SetActive(true); 
		camGameScene.gameObject.SetActive(false);
	}

	public void showLevelIntro(){
		levelMng.levelIntroMng.openScene();
		camLoading.gameObject.SetActive(false);
	}


	public void init_hideLoading(float delay){
		Invoke("hideLoading", delay);
	}
	public void hideLoading(){
		print("HIDE LOADING " + Time.time);
		camGameScene.gameObject.SetActive(true);
		camLoading.gameObject.SetActive(false);

	}*/






}
