﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;





public class BallSplash_Unit : MonoBehaviour {


	public BallSplash_Mng thisMng;

	public ParticleSystem ps_;



	// Use this for initialization
	void Start () {
		ps_.Stop();
	}

	public void run(Vector3 pos){
		transform.position = pos;
		ps_.Emit(12);
		Invoke("stop", 1);
	}

	void stop(){
		ps_.Stop();
	}


}
