﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Enemy_Spike_Unit : MonoBehaviour {




	public Enemy_Spike_Mng mng;
	public Transform tf_spike;
	public Collider cldr;
	float start_y;




	void Start () {
		start_y = tf_spike.localPosition.y;
		cldr.enabled = false;
		mng.units.Add(this);
	}



	public void run(){
		cldr.enabled = true;
		tf_spike.DOLocalMoveY(0, 1f, false).SetEase(Ease.OutBounce);
	}


	public void hide(){
		tf_spike.DOLocalMoveY(start_y, 0.25f, false).SetEase(Ease.InBounce);
	}


}
