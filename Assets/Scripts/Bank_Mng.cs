﻿using UnityEngine;
using System.Collections;

public class Bank_Mng : MonoBehaviour {




	public static Bank_Mng Instance;
	public static int shotsCollected_total;	
	public Number_Delegate[] delegates_ers;





	void Awake(){
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}

	public void show_shotsCollected_ERS(){
		//@ hole sunk
		//must display in the ers bank, shots collected....


		string valueString = shotsCollected_total.ToString();
		int valueLength = valueString.Length;

		print("st = " + shotsCollected_total + ", vL = " + valueLength + ", vS = " + valueString);



		if(valueLength == 1){
			int e = int.Parse(valueString[0].ToString());
			delegates_ers[0].set_number(e);
			//delegates_ers[0].shake();
		}
		if(valueLength == 2){
			int e = int.Parse(valueString[0].ToString());
			delegates_ers[1].set_number(e);
			e = int.Parse(valueString[1].ToString());
			delegates_ers[0].set_number(e);
			//delegates_ers[0].shake();
			//delegates_ers[1].shake();
		}
		if(valueLength == 3){
			int e = int.Parse(valueString[0].ToString());
			delegates_ers[2].set_number(e);
			e = int.Parse(valueString[1].ToString());
			delegates_ers[1].set_number(e);
			e = int.Parse(valueString[2].ToString());
			delegates_ers[0].set_number(e);
			//delegates_ers[0].shake();
			//delegates_ers[1].shake();
			//delegates_ers[2].shake();
		}
		if(valueLength == 4){
			int e = int.Parse(valueString[0].ToString());
			delegates_ers[3].set_number(e);
			e = int.Parse(valueString[1].ToString());
			delegates_ers[2].set_number(e);
			e = int.Parse(valueString[2].ToString());
			delegates_ers[1].set_number(e);
			e = int.Parse(valueString[3].ToString());
			delegates_ers[0].set_number(e);
			//delegates_ers[0].shake();
			//delegates_ers[1].shake();
			//delegates_ers[2].shake();
			//delegates_ers[3].shake();
		}

	


	}

	public void add_shotToBank(){

		shotsCollected_total++;
		show_shotsCollected_ERS();
	
	}

	public void shake(){
		for(int i = 0; i < delegates_ers.Length; i++){
			delegates_ers[i].shake();
		}
	}


	

}
