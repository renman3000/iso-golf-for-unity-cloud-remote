﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Cloud_Unit : MonoBehaviour {

	public Cloud_Mng thisMng;

	Vector3 resetPos;
	Vector3 endPos;
	Vector3 startPos;
	public int dir;
	public float endPos_axis;
	public float startPos_axis;
	public float resetPos_axis;
	public float speed;
	void Awake(){


		thisMng.units.Add(this);


		if(dir == 4){
			resetPos = new Vector3(resetPos_axis, transform.localPosition.y, transform.localPosition.z);
			endPos = new Vector3(endPos_axis, transform.localPosition.y, transform.localPosition.z);
			startPos = new Vector3(startPos_axis, transform.localPosition.y, transform.localPosition.z);
			transform.localPosition = new Vector3(startPos_axis, transform.localPosition.y, transform.localPosition.z);
		}
	
		if(dir == 2){
			resetPos = new Vector3(resetPos_axis, transform.localPosition.y, transform.localPosition.z);
			endPos = new Vector3(endPos_axis, transform.localPosition.y, transform.localPosition.z);
			startPos = new Vector3(startPos_axis, transform.localPosition.y, transform.localPosition.z);
			transform.localPosition = new Vector3(startPos_axis, transform.localPosition.y, transform.localPosition.z);
		}

		if(dir == 1){
			resetPos = new Vector3(transform.localPosition.x, transform.localPosition.y, resetPos_axis);
			endPos = new Vector3(transform.localPosition.x, transform.localPosition.y, endPos_axis);
			startPos = new Vector3(transform.localPosition.x, transform.localPosition.y, startPos_axis);
			transform.localPosition = new Vector3(transform.localPosition.z, transform.localPosition.y, startPos_axis);
		}

		if(dir == 3){
			resetPos = new Vector3(transform.localPosition.x, transform.localPosition.y, resetPos_axis);
			endPos = new Vector3(transform.localPosition.x, transform.localPosition.y, endPos_axis);
			startPos = new Vector3(transform.localPosition.x, transform.localPosition.y, startPos_axis);
			transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, resetPos_axis);
		}

		transform.localPosition = resetPos;

	}




	public  void run(){
		//print(transform.name + ", pos c = " + transform.localPosition);
		//transform.DOLocalMove(endPos, speed, false).OnComplete(reset); 
		//print(transform.name + ", pos d = " + transform.localPosition);
		StartCoroutine("move");
	}
	IEnumerator move(){
		bool moving = true;
		while(moving){
			float step = speed * Time.deltaTime;
			transform.localPosition = Vector3.MoveTowards(transform.localPosition, endPos, step);

			if(Mathf.Approximately(transform.localPosition.z, endPos_axis)){
				reset();
			}
			yield return null;
		}
	}

	void reset(){
		//print(transform.name + ", pos e = " + transform.localPosition);
		//transform.DOKill();
		transform.localPosition = startPos;
	}



	

}
