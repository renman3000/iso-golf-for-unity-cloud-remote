﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class NodeProject_Mng : MonoBehaviour {


	public CustomLevelLayout units_active;
	public CustomUnitType units_type;
	public CustomLevelLayout units_isHole;
	public CustomLevelLayout units_isStart;
	public CustomLevelLayout units_isExtent;





	public GameObject grassFlat;//1
	public GameObject grassRun;//2
	public GameObject water;//3
	public GameObject floor;//4
	public GameObject sand;//5
	public GameObject roseBed;//6
	public GameObject tree1;//7
	public GameObject tree2;//8
	public GameObject dirt;//9


	
	
	[HideInInspector]
	public List<NodeProject_Unit> units;




	// Use this for initialization
	void Start () {

		createUnits();
		


	}


	void createUnits(){

		for(int i = 0; i < units_active.rows.Length; i++){
			for(int j = 0; j < units_active.rows[i].row.Length; j++){
				if(units_active.rows[i].row[j] == true){
					createUnit(i, j);
				}
			}
		}
	}

	void createUnit(int x, int z){

		///now that we now we must create a unit at this x,z pos...
		Vector3 pos = new Vector3(transform.position.x + (float)x, transform.position.y, transform.position.z + (float)z);

		///we must get its type....(use x,z to sort.
		int unitType = units_type.rows[x].row[z];

		//we must get if isHole
		bool isHole = units_isHole.rows[x].row[z];

		//we must get if isStart
		bool isStart = units_isStart.rows[x].row[z];

		//we must get if isExtent
		bool isExtent = units_isExtent.rows[x].row[z];
		if(isExtent){
			Home_Mng.Instance.levelMng.camCntrl.set_Extent(pos);
		}

	

		///now we have a unit to create, and basic information we must pass to it. 
		GameObject unit = null;
		if(unitType == 1 || unitType == 0){
			//create grassy flat 
			unit = Instantiate(grassFlat, pos, Quaternion.identity) as GameObject;
		}
		if(unitType == 2){
			//create grassy run
			unit = Instantiate(grassRun, pos, Quaternion.identity) as GameObject;
		}
		if(unitType == 3){
			//create water 
			unit = Instantiate(water, pos, Quaternion.identity) as GameObject;
		}
		if(unitType == 4){
			//create floor
			//unit = Instantiate(floor, pos, Quaternion.identity) as GameObject;
			print("????? must set floor.");
			return;
		}
		if(unitType == 5){
			//create sand
			unit = Instantiate(sand, pos, Quaternion.identity) as GameObject;
		}
		if(unitType == 6){
			//create roseBed
			unit = Instantiate(roseBed, pos, Quaternion.identity) as GameObject;
		}
		if(unitType == 7){
			//create tree 1
			unit = Instantiate(tree1, pos, Quaternion.identity) as GameObject;
			unit.transform.parent = transform;
			return;
		}
		if(unitType == 8){
			//create tree 2
			//unit = Instantiate(tree2, pos, Quaternion.identity) as GameObject;
			return;
		}
		if(unitType == 9){
			//create dirt
			unit = Instantiate(dirt, pos, Quaternion.identity) as GameObject;
		}

		//now that we have spanwed and placed the unit, we must pass the basic essentials (isHole, isExtent, isStart)
		Grounds_Unit gu = unit.GetComponent<Grounds_Unit>();
		gu.isHole = isHole;
		gu.isStart = isStart;
		gu.isExtent = isExtent;
		gu.init_unit(unitType);
	
		unit.transform.parent = transform;
		//unit.transform.localEulerAngles = new Vector3(-90, 0,0);
	}




}
