﻿using UnityEngine;
using System.Collections;

public class NodeProject_Unit : MonoBehaviour {
/*
	public NodeProject_Mng thisMng;
	public int type;//0 = nothing, 1 = grass flat, 2 = grass run, 3 = dirt, 4 = sand, 5 = water, 6 = tree 1, 7 = tree 2, 8 = floor, 9 = rose bed 
	public int zPos;


	public bool isHole;
	public bool isStart;
	public bool isExtent;

	GameObject unit;
	Grounds_Unit unit_;

	// Use this for initialization
	void Start () {
		//add to mng
		thisMng.units.Add(this);
		///set pos
		transform.localPosition = new Vector3(0, 0, (float)zPos);
		set();

	}
	
	public void set(){
		if(type == 0){
			//do nothing
		}
		else if(type == 1){
			//grass, flat
			unit = Instantiate(thisMng.grassFlat) as GameObject;
			unit.transform.parent = transform;
			unit.transform.localPosition = Vector3.zero;
		}
	
		else if (type == 2){
			//grass, run
			unit = GameObject.Instantiate(thisMng.grassRun)as GameObject;
			unit.transform.parent = transform;
			unit.transform.localPosition = Vector3.zero;
		}
		else if (type == 3){
			//dirt
			unit = GameObject.Instantiate(thisMng.dirt) as GameObject;
			unit.transform.parent = transform;
			unit.transform.localPosition = Vector3.zero;
		}
		else if (type == 4){
			//sand
			unit = GameObject.Instantiate(thisMng.sand) as GameObject;
			unit.transform.parent = transform;
			unit.transform.localPosition = Vector3.zero;
		}
		else if (type == 5){
			//water
			unit = GameObject.Instantiate(thisMng.water) as GameObject;
			unit.transform.parent = transform;
			unit.transform.localPosition = Vector3.zero;
		}
		else if (type == 6){
			//tree 1
			unit = GameObject.Instantiate(thisMng.tree1) as GameObject;
			unit.transform.parent = transform;
			unit.transform.localPosition = Vector3.zero;
		}
		else if (type == 7){
			//tree 2
			unit = GameObject.Instantiate(thisMng.tree2) as GameObject;
			unit.transform.parent = transform;
			unit.transform.localPosition = Vector3.zero;
		}
		else if (type == 8){
			//floor
			unit = GameObject.Instantiate(thisMng.floor) as GameObject;
			unit.transform.parent = transform;
			unit.transform.localPosition = Vector3.zero;
		}
		else if (type == 9){
			//rose bed
			unit = GameObject.Instantiate(thisMng.roseBed) as GameObject;
			unit.transform.parent = transform;
			unit.transform.localPosition = Vector3.zero;
		}

	}

	public void set_asHole(){
		unit_ = unit.GetComponent<Grounds_Unit>();
		unit_.set_asHole();
	}
	public void set_asStart(){
		unit_ = unit.GetComponent<Grounds_Unit>();
		unit_.set_asStart(transform.position);
	}


	public void setFloor(Vector3 extentPos){
		if(type != 8){
			return;
		}
		print("setFloor");
		//pinged from the mng, this is the floor unit. 

		//here we have been passed he position of the extent. (furthest pos from 0,0,0)

		//we base our scale and position, on it.


		//position
		Vector3 setPos = new Vector3(extentPos.x/2, -0.5f,extentPos.z/2);
		transform.position = setPos;


		//scale 
		Vector3 scaleX = new Vector3(extentPos.x, 0, 0);
		Vector3 scaleZ = new Vector3(0, 0, extentPos.z);
		float distX = Vector3.Distance(Vector3.zero, scaleX);
		float distZ = Vector3.Distance(Vector3.zero, scaleZ);


		if(distX == 0){
			distX = 1.5f;
		}
		if(distZ == 0){
			distZ = 1.5f;
		}

		Vector3 newScale = new Vector3(distX + (distX * 0.5f), 1, distZ + (1));


		transform.localScale = newScale;

	}*/
}


