﻿using UnityEngine;
using System.Collections;

public class Title_Mng : MonoBehaviour {

	public static Title_Mng instance;
	public Transform tf_home;
	public Transform tf_store;
	public Transform tf_gift;
	public Transform tf_prize;




	void Awake(){
		instance = this;
	}

	// Use this for initialization
	void Start () {
		openHome();
	}
	public void openHome(){
		tf_home.localScale = Vector3.one;
		tf_store.localScale = Vector3.zero;
		tf_gift.localScale = Vector3.zero;
		tf_prize.localScale = Vector3.zero;
	}
	public void openStore(){
		tf_home.localScale = Vector3.zero;
		tf_store.localScale = Vector3.one;
		tf_gift.localScale = Vector3.zero;
		tf_prize.localScale = Vector3.zero;
	}
	public void openGift(){
		tf_home.localScale = Vector3.zero;
		tf_store.localScale = Vector3.zero;
		tf_gift.localScale = Vector3.one;
		tf_prize.localScale = Vector3.zero;
	}
	public void openPrize(){
		tf_home.localScale = Vector3.zero;
		tf_store.localScale = Vector3.zero;
		tf_gift.localScale = Vector3.zero;
		tf_prize.localScale = Vector3.one;
	}
	public void openGame(){
		tf_home.localScale = Vector3.zero;
		tf_store.localScale = Vector3.zero;
		tf_gift.localScale = Vector3.zero;
		tf_prize.localScale = Vector3.zero;
	}
	

}
