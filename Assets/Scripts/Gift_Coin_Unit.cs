﻿using UnityEngine;
using System.Collections;
using DG.Tweening;



public class Gift_Coin_Unit : MonoBehaviour {

	
	// Use this for initialization
	void Start () {
		Gift_Mng.Instance.coin_units.Add(this);
		transform.localEulerAngles = new Vector3(0, 35, 0);
		transform.localScale = Vector3.zero;
	}



	public void run(Vector3 hudPos, float frq){
		
		Gift_Mng.Instance.coin_units.Remove(this);

		transform.localScale = new Vector3(1,1,1);
		transform.position = Gift_Mng.Instance.tf_masterCoin.position;
		frq = frq + (frq * 0.1f);
		transform.DOMove(hudPos, frq, false).OnComplete(reset).SetEase(Ease.InSine);
		transform.DOScale(new Vector3(0.25f, 0.25f, 0.25f), 0.25f).SetEase(Ease.InSine).OnComplete(reset);
	}


	void reset(){
		transform.localPosition = Vector3.zero;
		transform.localScale = Vector3.zero;
		Gift_Mng.Instance.coin_units.Add(this);
		adjust_coinsInHud();
		Gift_Mng.Instance.coinAdded();
	}
	void adjust_coinsInHud(){
		Coin_Mng.Instance.add_coinstoCoins(1);
	}


}
