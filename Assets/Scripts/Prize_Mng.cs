﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;


public class Prize_Mng : MonoBehaviour {

	public static Prize_Mng Instance;
	public Transform tf_prizeMachine;
	public RectTransform rt_prizeMachine;
	Vector2 v2Size_prizeMachineBtn;
	public Transform tf_prizeBox;
	public Transform tf_prize;
	public RectTransform rt_prize;
	public RectTransform rt_returnHome;
	public Transform tf_openERS;
 
 	public TextMesh tm_ballName;

	public ParticleSystem[] ps_conffetti;


	public GameObject[] balls;//these balls are the potential prefabs for the prize won.

	GameObject gm_prizeBall;//preffabed unit, visual repreesentaion at prize reveal
	[HideInInspector]
	public string prizeBall_name;//passed by the prize ball unit on instantiation, used to compare name, in order to unlock and set as default. 
	int int_rdmBall;


	public Transform tf_prizeBallHolder;

	[HideInInspector]
	public List<Store_Ball_Unit> storeBallUnits;//use to compare prize ball won, to these to set its values after won.
	[HideInInspector]
	List<Store_Ball_Unit> potentialPrizes;




	void Awake(){
		Instance = this;
		v2Size_prizeMachineBtn = rt_prize.sizeDelta;
		reset();
		tm_ballName.text = "";
	}


	public void run(){
		print("PMG: run()");
		///scale up prize machine and turn on input
		tf_prizeMachine.localScale = Vector3.one;
		rt_prizeMachine.sizeDelta = v2Size_prizeMachineBtn;

		//set_randomBall_asPrize();

	}

	public void bp_selectPrize(){
		///prize machine activated...

		//hide prize machine scale up prize box, turn on prize box input
		print("PMG: bp select prize...()");
				
		///
		rt_prizeMachine.sizeDelta = Vector2.zero;

		///
		tf_prizeMachine.DOScale(Vector3.zero, 0.25f);

		///
		tf_prizeBox.DOScale(Vector3.one, 0.25f);			

		///
		rt_prize.sizeDelta = v2Size_prizeMachineBtn;

		set_randomBall_asPrize();

	}

	void set_randomBall_asPrize(){
		///spawn a rndr of the ball won

		potentialPrizes = new List<Store_Ball_Unit>();
		for(int i = 0; i < storeBallUnits.Count; i++){
			print("i " + i);
			if(storeBallUnits[i].isPrize){
				print("storeBallUnit.name = " + storeBallUnits[i].name);

				potentialPrizes.Add(storeBallUnits[i]);
			}
		}
		int_rdmBall = Random.Range(0,potentialPrizes.Count);
		gm_prizeBall = Instantiate(potentialPrizes[int_rdmBall].prefabbed_rep, Vector3.zero, transform.rotation) as GameObject;
		gm_prizeBall.transform.parent = tf_prize;
		gm_prizeBall.transform.localScale = new Vector3(5,5,5);
		gm_prizeBall.transform.localPosition = Vector3.zero;
		storeBallUnits[int_rdmBall].unlock();
		storeBallUnits[int_rdmBall].isPrize = false;
		potentialPrizes.Clear();


		//rotate ball for viual effect
		Vector3 rtn = new Vector3(0, 180, 0);
		tf_prize.DOLocalRotate(rtn, 20, 0).SetLoops(-1);

		//set layer to prize layer
		foreach(Transform child in gm_prizeBall.transform){
			child.gameObject.layer = LayerMask.NameToLayer("PRIZE");
		}

	}




	public void bp_revealPrize(){
		///btn pressed to collect prize.....
		print("PMG reveial prize");
		//hide prize box
		tf_prizeBox.localScale = Vector3.zero;
		//reveal prize
		tf_prize.localScale = Vector3.one;
		//rotate prize
		//Vector3 rtn = new Vector3(0, 180, 0);
		//tf_prize.DOLocalRotate(rtn, 20, 0).SetLoops(-1);
		//turn off prize btn
		rt_prize.sizeDelta = Vector2.zero;

		tm_ballName.text = storeBallUnits[int_rdmBall].name;

		//play conffetti
		for (int j = 0; j < ps_conffetti.Length; j++){
			ps_conffetti[j].Emit(25);
			ps_conffetti[j].transform.localScale = Vector3.one;
		}

		ERS_Mng.Instance.isPrizeCollected = true;
		tf_openERS.localScale = Vector3.one;
	
	}
	//
	public void bp_collectPrize(){
		print("bp collect prize...");
		//stop tween prize
		//tf_prize.DOKill();
		//pass prize to the ball mng to store as owned
		print("!!!!FILL IN !!!! - Pass new prize to ball mng as owned"); 
		//reset

		for(int i = 0; i< ps_conffetti.Length; i++){
			ps_conffetti[i].Emit(0);
		}
		///open ers static btns
		ERS_Mng.Instance.setERBS();
	
		reset();

	}





	public void reset(){
		rt_prize.sizeDelta = Vector2.zero;
		rt_prizeMachine.sizeDelta = Vector2.zero;
		rt_returnHome.sizeDelta = Vector2.zero;
		tf_prizeMachine.localScale = Vector3.zero;
		tf_prize.DOKill();
		tf_prize.localEulerAngles = Vector3.zero;
		tf_prize.localScale = Vector3.zero;
		tf_prizeBox.localScale = Vector3.zero;
		print("?? BELOW, confetti?? ");
		tf_openERS.localScale = Vector3.zero;

		tm_ballName.text = "";
		tf_prize.DOKill();

		if(gm_prizeBall){
			Home_Mng.Instance.set_defaultBall(gm_prizeBall);
			Home_Mng.Instance.camMng.turnOn_gameScene();
		}
		/*
		for (int j = 0; j < ps_conffetti.Length; j++){
			ps_conffetti[j].Stop();
			ps_conffetti[j].transform.localScale = Vector3.zero;
		}*/
	
	}


}
