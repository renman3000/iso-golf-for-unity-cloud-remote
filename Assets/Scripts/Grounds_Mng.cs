﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;



public class Grounds_Mng : MonoBehaviour {


	[HideInInspector]
	public Level_Mng levelMng;


	[HideInInspector]
	public List<Grounds_Unit> units;


	[HideInInspector]
	public List<Grounds_Unit> potential_target_units;


	public ParticleSystem ps_splash;






	// Use this for initialization
	void Start () {

	}


	////////
	////////
	//////
	//////
	/////
	/*
	void run_testBall(Vector3 endPos, int j, Transform tf_ball, int trajectory, float dist_ball){
		print(j + "RUN TEST BALLS " + endPos);
		///here we test in an invisable transform, trigger, with the potential destinaion......
		Vector3[] tn_nodes = new Vector3[2];
		///


		float y_offset = Mathf.Abs(endPos.y - tf_ball.position.y);
		float height_full = Mathf.Abs((dist_ball) + y_offset);





		if(trajectory == 1){
			float zDiff = Mathf.Abs(endPos.z + tf_ball.position.z);
			tn_nodes[0] = new Vector3(tf_ball.position.x, height_full, (zDiff/2) + tf_ball.position.z);
		}
		if(trajectory == 2){
			float xDiff = Mathf.Abs(endPos.z + tf_ball.position.x);
			tn_nodes[0] = new Vector3(xDiff + tf_ball.position.x, height_full, tf_ball.position.z);
		}
		if(trajectory == 3){
			float zDiff = Mathf.Abs(endPos.z - tf_ball.position.z);
			tn_nodes[0] = new Vector3(tf_ball.position.x, height_full, -(zDiff/2) + tf_ball.position.z);
		}
		if(trajectory == 4){
			float xDiff = Mathf.Abs(endPos.z + tf_ball.position.x);
			tn_nodes[0] = new Vector3(xDiff - tf_ball.position.x, height_full, tf_ball.position.z);
		}
		tn_nodes[1] = endPos;	
		tf_tests[j].position = tf_ball.position;
		tf_tests[j].DOPath(tn_nodes, 5, PathType.CatmullRom, PathMode.Full3D, 10, null);

	}
	*/
	/*
	public void testBallHit(Transform tf_testBall, Grounds_Unit hit_unit){
		print("- TEST BALL HIT " + tf_testBall.position);
		tf_testBall.DOKill();
		//tf_testBall.position = new Vector3(0,0,1000);
	}*/
	/// <summary>
	/// /
	/// </summary>
	/// <param name="tf_ball">Tf ball.</param>
	/// <param name="dist_ball">Swing fctr.</param>
	/// <param name="int_trajectory">Int trajectory.</param>





	public void runSplash(Grounds_Unit trgtUnit){
		print("mng, runSplash");
		Home_Mng.Instance.atDeath();
	}






	public void reset(){
		for(int i = 0; i < units.Count; i++){
			units[i].reset();
		}
	}
}